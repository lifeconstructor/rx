'use strict';

var React = require('react-native');

var {
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Platform,
    ListView
} = React;
var GLOBALS  = require('./Globals');




var ReminderAdd = require('./reminders-add-page');
var RemindersManage = require('./reminders-manage-page');
var ReminderEdit = require('./reminders-edit-page');



var Page = React.createClass({

  getInitialState: function() {

      var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;

      fetch(URL)
          .then((response) => response.json())
          .catch((error) => {

          })
          .then((responseData) => {

              this.setState({
                  dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                  dataRaw: responseData.result
              });

          })
          .done();





    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    return {
        www: 0,
        accTopPosition: 0,
        progress: [],
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        dataSourceRemindersAPI: ds.cloneWithRows(['row 1', 'row 2']),
        dataRaw: []
    };
  },


    getDataSource1: function(test: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(test);
    },


    componentWillReceiveProps: function() {

        var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;


        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                    dataRaw: responseData.result
                });

            })
            .done();

    },



    gotoEditReminder: function(reminder, product, id){
        this.props.navigator.push({
            title: GLOBALS.titleCut('Edit Reminder'),
            component: ReminderEdit,
            reminder: reminder,
            product: product,
            id: id
        });
    },

  gotoAddReminder: function(){
    this.props.navigator.push({
      title: GLOBALS.titleCut('Add Reminder'),
      component: ReminderAdd
    });
  },

  gotoManageReminders: function(){
    this.props.navigator.push({
      title: GLOBALS.titleCut('Manage Reminders'),
      component: RemindersManage
    });
  },

  // Rotate
  measureMainComponent: function() {
    
      this.refs.MainComponent.measure((ox, oy, width) => {
            this.setState({
                www: width,
            });
            var a = 0;
            var b = (Platform.OS === 'android')? 13 : 21;
            this.setState({
                accTopPosition: (this.state.www > 480)?a:b,
            });
        });
  },

  render: function(){

      var remindersList = this.state.dataRaw.length
          ?
          <ListView
              dataSource={this.state.dataSourceRemindersAPI}
              renderRow={
                    (rowData) => {

                        var colorActive = new Date(JSON.parse(rowData.reminder).endDate).setHours(23,59,59,999) >= new Date()? 'green':'red';

                         var name = JSON.parse(rowData.reminder).product_id;

                        return(
                            <TouchableHighlight
                                gotoEditReminder={this.gotoEditReminder}
                                onPress={function(){
                                            this.gotoEditReminder(JSON.parse(rowData.reminder), rowData.product, rowData.id);
                                        }
                                    }
                            >
                                <View style={{flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                                  <Image
                                      style={{height:40, width: 40, margin: 10}}
                                      source={GLOBALS.icons.pill}
                                  />
                                  <View style={{flex: 0.6, marginLeft: 10}}>
                                    <Text></Text>
                                    <Text style={{color: colorActive}}>{name}</Text>
                                    {/*<Text>{rowData.product.DOSAGEFORMNAME}</Text>*/}
                                  </View>
                                  <View style={{flex: 0.2, alignItems: 'center', marginTop: 10}}>
                                    <Text></Text>
                                    <Text></Text>
                                  </View>
                                </View>
                            </TouchableHighlight>
                            );
                }}
          />
          :
              <TouchableHighlight>
                  <View style={{flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>

                      <View style={{flex: 0.6, alignItems: 'center', marginTop: 20}}>
                          <Text>NO REMINDERS FOUND</Text>
                          <Text></Text>
                          <Text></Text>
                      </View>
                      <View style={{flex: 0.2, alignItems: 'center', marginTop: 10}}>
                          <Text></Text>
                          <Text></Text>
                      </View>
                  </View>
              </TouchableHighlight>
          ;


    return (

        <ScrollView style={{marginTop: this.state.accTopPosition}}>
        <View  
            onLayout={this.measureMainComponent.bind(null,this)} 
            ref="MainComponent">


            {remindersList}



          <TouchableHighlight
              onPress={this.gotoAddReminder}
              style={styles.loginButton}
          >
            <Text style={styles.loginText}>
              Add Reminder
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
              onPress={this.gotoManageReminders}
              style={styles.loginButton}
          >
            <Text style={styles.loginText}>
              Manage Reminders
            </Text>
          </TouchableHighlight>

        </View>
        </ScrollView>
    );
  }

});

// TODO: fix styles for the page if not removed
var styles = StyleSheet.create({
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'black',
    marginTop: 70,
  },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginTop: 20,
        marginHorizontal: 40,
        alignItems: 'center'
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }
});


module.exports = Page;










var Page = React.createClass({



  render(){
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
        <View style={styles.mainContainer}>
          <View style={styles.toolbar}>

            <Text style={styles.toolbarButton} onPress={this.runDemo}>
              Start Run
            </Text>

            <Text style={styles.toolbarButton} onPress={this.closeDatabase}>
              Close DB
            </Text>
            <Text style={styles.toolbarButton} onPress={this.deleteDatabase}>
              Delete DB
            </Text>
            <Text style={styles.toolbarButton} onPress={this.populateDatabase}>
              Query Data
            </Text>
          </View>
          <ListView
              dataSource={ds.cloneWithRows(this.state.progress)}
              renderRow={this.renderProgressEntry}
              style={listStyles.liContainer}/>
        </View>
    );
  }
});


