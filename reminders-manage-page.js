'use strict';

var React = require('react-native');

var {
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Platform,
    ListView
    } = React;
var GLOBALS  = require('./Globals');

var PushNotification = require('react-native-push-notification');

PushNotification.configure({

    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function(token) {
        console.log( 'TOKEN:', token );
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
    },

    // ANDROID ONLY: (optional) GCM Sender ID.
    senderID: "YOUR GCM SENDER ID",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * IOS ONLY: (optional) default: true
     * - Specified if permissions will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     */
    requestPermissions: true,
});

var Page = React.createClass({

    getInitialState: function() {

        var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                    dataRaw: responseData.result
                });

            })
            .done();



        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return {
            www: 0,
            accTopPosition: 0,
            progress: [],
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            dataSourceRemindersAPI: ds.cloneWithRows(['row 1', 'row 2']),
            dataRaw: [],
            reload: ''
        };
    },


    getDataSource1: function(test: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(test);
    },

    componentWillReceiveProps: function() {

        var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;



        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                    dataRaw: responseData.result
                });

            })
            .done();

    },

    updateLocalReminders: function(){

        PushNotification.cancelAllLocalNotifications();

        var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                /*this.setState({
                 dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                 dataRaw: responseData.result
                 });*/



                if (responseData && responseData.result && responseData.result.length){

                    responseData.result.map((data, index) =>{

                        var reminderInfo = JSON.parse(data.reminder);
                        console.log(reminderInfo);



                        switch(reminderInfo.recurrence) {
                            case 'ONCE':
                                PushNotification.localNotificationSchedule({
                                    message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                    date: (new Date(reminderInfo.startDate)).toISOString()
                                });
                                break;
                            case 'DAILY':
                                var dataD = new Date(reminderInfo.startDate);

                                var DAILY = 0;

                                while (dataD < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    DAILY++;
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: dataD.toISOString()
                                    });
                                    dataD.setDate(dataD.getDate() + 1);
                                }
                                if (!DAILY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'WEEKLY':
                                var dataW = new Date(reminderInfo.startDate);

                                var WEEKLY = 0;

                                while (dataW < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    WEEKLY++;
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: dataW.toISOString()
                                    });
                                    dataW.setDate(dataW.getDate() + 7);
                                }

                                if (!WEEKLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'MONTHLY':
                                var dataM = new Date(reminderInfo.startDate);

                                var MONTHLY = 0;

                                while (dataM < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    MONTHLY++;
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: dataM.toISOString()
                                    });
                                    dataM.setMonth(dataM.getMonth() + 1);
                                }

                                if (!MONTHLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'YEARLY':
                                var dataY = new Date(reminderInfo.startDate);

                                var YEARLY = 0;

                                while (dataY < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    YEARLY++;
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: dataY.toISOString()
                                    });
                                    dataY.setYear(dataY.getYear() + 1);
                                }

                                if (!YEARLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }
                                break;
                            case 'CUSTOM':

                                PushNotification.localNotificationSchedule({
                                    message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                    date: (new Date(reminderInfo.startDate)).toISOString()
                                });

                                if (reminderInfo.alarms.length){

                                    reminderInfo.alarms.map((dataCustom, index) => (
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataCustom
                                        })
                                    ))
                                }

                                break;
                            default:
                                console.log('DEFAULT');
                                break;
                        }
                    });

                }




            })
            .done();


    },



    removeReminder: function(){


        var URL = CONFIG.API_URL + 'api/get-reminders?access-token=' + GLOBALS.API_KEY;



        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {


                this.setState({
                    dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                    dataRaw: responseData.result
                });

                this.updateLocalReminders();

            })
            .done();



    },

    // Rotate
    measureMainComponent: function() {

        this.refs.MainComponent.measure((ox, oy, width) => {
            this.setState({
                www: width,
            });
            var a = 0;
            var b = (Platform.OS === 'android')? 13 : 21;
            this.setState({
                accTopPosition: (this.state.www > 480)?a:b,
            });
        });
    },



    render: function(){


        var remindersList = this.state.dataRaw.length
                ?
                <ListView
                    dataSource={this.state.dataSourceRemindersAPI}
                    renderRow={
                    (rowData) =>{

                    var name = JSON.parse(rowData.reminder).product_id;

                        return (
                        <TouchableHighlight>
                            <View style={{flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                              <Image
                                  style={{height:40, width: 40, margin: 10}}
                                  source={GLOBALS.icons.pill}
                              />
                              <View style={{flex: 0.3, marginLeft: 10}}>
                                <Text></Text>
                                <Text>{name}</Text>
                                {/*<Text>{rowData.product.DOSAGEFORMNAME}</Text>*/}
                              </View>
                              <View style={{flex: 0.5, alignItems: 'center', flexDirection: 'row', marginTop: 3}}>
                                <TouchableHighlight
                                    onPress={
                                        function(){

                                            var URL = 'http://v2-0-rxscout.pilgrimconsulting.com/backend/api/delete-reminder?id=' + rowData.id;


                                            fetch(URL, {
                                                method: 'GET',
                                                headers: {
                                                    'Accept': 'application/json',
                                                    'Content-Type': 'application/json',
                                                    'Authorization': 'Bearer ' + GLOBALS.API_KEY
                                                }
                                            })
                                                .then((response) => response.json())
                                                .catch((error) => {

                                                })
                                                .then((responseData) => {

                                                    this.removeReminder();
                                                })
                                                .done();
                                        }
                                    }
                                    removeReminder={this.removeReminder}
                                    style={styles.removeButton}
                                >
                                    <Text style={styles.loginText}>
                                        Remove
                                    </Text>
                                </TouchableHighlight>
                              </View>
                            </View>
                        </TouchableHighlight>)
                }}
                />
                :
                <TouchableHighlight>
                    <View style={{flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>

                        <View style={{flex: 0.6, alignItems: 'center', marginTop: 20}}>
                            <Text></Text>
                            <Text>NO REMINDERS FOUND</Text>
                            <Text></Text>
                        </View>
                        <View style={{flex: 0.2, alignItems: 'center', marginTop: 10}}>

                        </View>
                    </View>
                </TouchableHighlight>
            ;


        return (

            <ScrollView style={{marginTop: this.state.accTopPosition}}>
                <View
                    onLayout={this.measureMainComponent.bind(null,this)}
                    ref="MainComponent">
                    {remindersList}
                </View>
            </ScrollView>
        );
    }

});

var styles = StyleSheet.create({
    tabContent: {
        flex: 1,
        alignItems: 'center'
    },
    tabText: {
        color: 'black',
        marginTop: 70
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginTop: 20,
        marginHorizontal: 40,
        alignItems: 'center'
    },
    removeButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginTop: 5,
        marginHorizontal: 40,
        alignItems: 'center'
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }
});


module.exports = Page;


