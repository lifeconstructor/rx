'use strict';

var React = require('react-native');
var {

    AppRegistry,
    Image,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ListView,
    Platform

    } = React;

var GLOBALS = require('./Globals');
var SePage3 = require('./search-page-3NL');

var Menu = require('react-native-menu');
var { MenuContext, MenuOptions, MenuOption, MenuTrigger } = Menu;

var form        = [];
var strength    = [];
var quantity    = [];
var daysSupply  = [];
var radius      = ["5 miles", "15 miles", "25 miles"];
var supply      = ["Less than 3 months", '3 or more months'];

var DRUGS       = [];



var DrugSelectScreen = React.createClass({

    getInitialState: function() {

        if (navigator){

            navigator.geolocation.getCurrentPosition(
                (position) => {
                    var initialPosition = JSON.stringify(position);
                    this.setState({initialPosition});


                    var URL_for_polyline = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&language=en';


                    fetch(URL_for_polyline)
                        .then((response) => response.json())
                        .catch((error) => {


                        })
                        .then((responseData) => {

                            var zip = '';

                            if (responseData.results){

                                responseData.results.forEach(function logArrayElements(element, index, array) {

                                    if (element.types[0] == 'postal_code'){
                                        element.address_components.forEach(function(element1, index1, array1){

                                            if (element1.types[0] == 'postal_code'){

                                                if (element1.long_name){
                                                    zip = element1.long_name;
                                                }
                                                if (element1.short_name){
                                                    zip = element1.short_name;
                                                }

                                            }

                                        });
                                    }

                                });

                                this.setState({
                                    zip: zip,
                                    zipEntered: zip,
                                    zipLat: responseData.results[0].geometry.location.lat,
                                    zipLng: responseData.results[0].geometry.location.lng
                                });



                            }

                            console.log(responseData);

                        })
                        .done();
                });
        }


        DRUGS = this.props.route.drugs;

        form =  DRUGS
            .map(function (drug) {
                return drug.dosageForm;
            })
            .filter(function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            });

        strength =  DRUGS
            .map(function (drug) {
                return drug.strength.description;
            })
            .filter(function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            });

        return({

            dosageForm: form[0],
            strength: strength[0],
            radius: radius[0],
            zip : '',
            zipEntered: '',
            quantity: '',
            supply: supply[0],
            quantityError: '',
            zipError: '',
            dropdownSelection: '-- Choose --',
            zipLat: '',
            zipLng: ''

        });

    },



    componentDidMount() {

        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});

            },
            (error) => {alert(error.message)},
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );


        this.watchID = navigator.geolocation.watchPosition((position) => {
            var lastPosition = JSON.stringify(position);
            /*if (lastPosition){
             this.setState({lastPosition: lastPosition});
             }*/

        });

    },

    searchPharmacies: function(){

        if (!this.state.quantity){
            this.setState({
                quantityError: 'Quantity cannot be empty'
            });
            return;
        }

        if (this.state.zipEntered){

            var URL_pharm = 'https://dplu-preview.data-rx.com/api/v1/pharmacies?distance:from:zip='
                + this.state.zipEntered + '&distance:miles:max=' + parseInt(this.state.radius);



            fetch(URL_pharm, {
                method: 'GET',
                headers: {
                    'Host': 'dplu-preview.data-rx.com',
                    'Connection': 'keep-alive',
                    'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                    'Referer': 'https://dplu-preview.data-rx.com/',
                    'Accept-Encoding': 'gzip, deflate, sdch',
                    'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
                }
            }).then((response) => response.json())
                .catch((error) => {

                    console.log(error);

                })
                .then((responseData) => {

                    var test = this.state.zip == this.state.zipEntered;

                    GLOBALS.currentZip = (this.state.zip == this.state.zipEntered)? this.state.zip:this.state.zipEntered;
                    GLOBALS.pharmaciesData = responseData;

                    this.props.navigator.push({
                        title: GLOBALS.titleCut(this.props.route.drugTitle),
                        component: SePage3,
                        pharmaciesData: responseData,
                        drugTitle: this.props.route.drugTitle,
                        strength: this.state.strength,
                        dosageForm: this.state.dosageForm,
                        daysSupply: this.state.supply,
                        quantity: this.state.quantity,
                        DRUGS: DRUGS,
                        userCurentLocation: test,
                        zip: this.state.zipEntered

                    });

                })
                .done();

        } else {
            this.setState({
                zipError: 'Zip cannot be empty'
            });
        }
    },

    render: function() {

        return (
            <View style={{marginTop: ((Platform.OS === 'ios')?64:55), flex:1}}>
                <ScrollView style={{flex: 1}}>
                    <Text style={{fontSize: 24, alignSelf: 'center', marginTop: 15}}>{this.props.route.drugTitle}</Text>

                    <MenuContext style={{ flex: 1}} ref="MenuContext">

                        <View style={[styles.content, {backgroundColor: CONFIG.backgroundCl}]}>
                            <Text style={styles.contentText}>
                                Dosage Form
                            </Text>
                            <Menu style={styles.dropdown} onSelect={(value) => this.setState({ dosageForm: value })}>

                                <MenuTrigger>
                                    <Text style={{ fontSize: 20 }}>{this.state.dosageForm}</Text>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={styles.dropdownOptions}
                                             renderOptionsContainer={(options) => <ScrollView>{options}</ScrollView>}>

                                    {form.map((dosageForm, index) => (
                                        <MenuOption value={dosageForm}>
                                            <Text style={{ fontSize: 20 }}>{dosageForm}</Text>
                                        </MenuOption>
                                    ))}

                                </MenuOptions>

                            </Menu>
                        </View>

                        <View style={[styles.content, {alignItems: 'stretch', backgroundColor: CONFIG.backgroundCl}]}>
                            <Text style={styles.contentText}>Quantity</Text>
                            <View style={{flexDirection: 'row'}}>
                                <TextInput
                                    style={{height: 30, backgroundColor: 'white', borderRadius: 5, marginHorizontal: 50, flex: 0.5, fontSize: 20}}
                                    onChangeText={(quantity) => this.setState({quantity})}
                                    value={this.state.quantity}
                                />
                                <Text
                                    style={{flex: 0.5, marginTop: 5, color: 'white', fontSize: 20}}
                                >{this.state.dosageForm}(s)</Text>
                            </View>
                            {(this.state.quantityError)?
                                <Text style={{fontSize: 18, alignSelf: 'center', marginTop: 10, color: 'red'}}>{this.state.quantityError}</Text>
                                : <View style={{width:0, height: 0}}></View>}
                        </View>

                        <View style={[styles.content, {backgroundColor: CONFIG.backgroundCl}]}>

                            <Text style={styles.contentText}>Zip</Text>

                            <TextInput
                                style={{height: 30, backgroundColor: 'white', borderRadius: 5, marginHorizontal: 50, fontSize: 20}}
                                onChangeText={(zip) => this.setState({zipEntered: zip})}
                                value={this.state.zipEntered}
                            />

                            {(this.state.zipError)?
                                <Text style={{fontSize: 18, alignSelf: 'center', marginTop: 5, color: 'red'}}>{this.state.zipError}</Text>
                                : <View style={{width:0, height: 0}}></View>}

                        </View>


                        <View style={[styles.content, {backgroundColor: CONFIG.backgroundCl}]}>
                            <Text style={styles.contentText}>
                                Supply
                            </Text>
                            <Menu style={styles.dropdown} onSelect={(value) => this.setState({ supply: value })}>

                                <MenuTrigger>
                                    <Text style={{ fontSize: 20 }}>{this.state.supply}</Text>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={styles.dropdownOptions}
                                             renderOptionsContainer={(options) => <ScrollView>{options}</ScrollView>}>

                                    {supply.map((supply, index) => (
                                        <MenuOption value={supply}>
                                            <Text style={{ fontSize: 20 }}>{supply}</Text>
                                        </MenuOption>
                                    ))}

                                </MenuOptions>
                            </Menu>
                        </View>


                        <View style={[styles.content, {backgroundColor: CONFIG.backgroundCl, borderBottomWidth: 0}]}>
                            <Text style={[styles.contentText, {backgroundColor: CONFIG.backgroundCl}]}>
                                Strength
                            </Text>
                            <Menu style={styles.dropdown} onSelect={(value) => this.setState({ strength: value })}>

                                <MenuTrigger>
                                    <Text style={{ fontSize: 20 }}>{this.state.strength}</Text>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={styles.dropdownOptions}
                                             renderOptionsContainer={(options) => <ScrollView>{options}</ScrollView>}>

                                    {strength.map((strength, index) => (
                                        <MenuOption value={strength}>
                                            <Text style={{ fontSize: 20 }}>{strength}</Text>
                                        </MenuOption>
                                    ))}

                                </MenuOptions>

                            </Menu>
                        </View>


                        <TouchableHighlight
                            onPress={this.searchPharmacies}
                            style={{
                                    flex: 0.4,
                                    borderRadius: 10,
                                    marginTop: 5,
                                    marginHorizontal: 50,
                                    marginBottom: 80,
                                    alignItems: 'center',
                                    backgroundColor: '#357bb5',
                                    padding: 20
                                    }}
                        >
                            <Text style={{color: 'white', fontSize: 20}}>
                                Search
                            </Text>
                        </TouchableHighlight>
                    </MenuContext>

                </ScrollView>
            </View>
        );
    },
});

var styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingTop: 10,
        paddingBottom: 30,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        alignItems: 'center'
    },
    contentText: {
        fontSize: 20,
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 10,
        color: 'white'
    },
    dropdown: {
        width: 300,
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 5
    },
    dropdownOptions: {
        marginTop: 30,
        borderColor: '#ccc',
        borderWidth: 2,
        width: 300,
        height: 200
    }

});

module.exports = DrugSelectScreen;