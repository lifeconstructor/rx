'use strict';


var CONFIG = require('./config/config');

var ga = this.ga = null;

var AutoComplete = require('react-native-autocomplete');


var LandingPage = React.createClass({

    getInitialState: function() {

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return {
            isLoading: false,
            isLoadingTail: false,
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            searchResults: ds.cloneWithRows([]),
            filter: '',
            queryNumber: 0,
            www: 0,
            searchBarWidth: 0,
            showMore: 0,
            showMore2: 1,
            test: 30,
            data: [],
            drugsAutocomplete: [],
            selectedDrug: '',
            keyBoardHeight: 0
        };
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this));
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this));

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Landing Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);
    },

    keyboardWillShow (e) {
        console.log('show');
        this.setState({keyBoardHeight: e.endCoordinates.height})
    },

    keyboardWillHide (e) {
        console.log('hide');
        this.setState({keyBoardHeight: 0});

        if (this.refs && this.refs._scrollView){
            this.refs._scrollView.scrollTo({x:0, y:0, animated:true});
        }
    },

    componentWillReceiveProps (e) {
        /*this.setState({keyBoardHeight: 0})*/

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);

    },

    measureMainComponent: function() {
        this.refs.MainComponent.measure((ox, oy, width, height) => {
            this.setState({
                www: width,
                hhh: height
            });
        });
    },

    login: function(){


        this.props.route.login();
    },


    endEditing: function(){

        console.log('123');

    },

    render: function() {
        return (
            <View style={{flex: 1}}
                  onLayout={this.measureMainComponent.bind(null,this)}  ref="MainComponent">
                <ScrollView
                    style={{width: this.state.www, height: this.state.hhh, marginBottom: this.state.keyBoardHeight}}
                    ref='_scrollView'
                >
                    <Image
                        source={require('./img/home-background.png')}
                        style={{width: this.state.www, height: this.state.hhh}}
                    >
                        <View style={styles.container}>
                            <View style={{flexDirection: 'row', width: 249, height: 101, marginBottom:30}}>
                                <Image
                                    source={require('./img/rxlogo.png')}
                                    style={{
                                            flex: 1,
                                            alignSelf: 'stretch',
                                            width: 299,
                                            height: 87
                                            }}
                                />
                            </View>



                            <View style={styles.buttonWrapper}>
                                <TouchableHighlight
                                    onPress={this.login}
                                    style={styles.buttons}
                                >
                                    <Text
                                        style={[styles.buttonText, {marginLeft: 41, marginRight: 41}]}
                                    >
                                        Member Login
                                    </Text>
                                </TouchableHighlight>
                            </View>

                            {/*<View style={styles.buttonWrapper}>
                                <TouchableHighlight
                                    onPress={this.props.route.signUp}
                                    style={[styles.buttons, {backgroundColor: null}]}
                                >
                                    <Text
                                        style={{backgroundColor: null}}
                                    >
                                        * Sign up *
                                    </Text>
                                </TouchableHighlight>
                            </View>*/}
                        </View>
                    </Image>
                </ScrollView>
            </View>


        );
    }

});

var styles = StyleSheet.create({
    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 40,
        marginRight: 40,
        marginBottom: 20,
        borderRadius: 5
    },
    backgroundImage: {
        flex: 1,
        alignSelf: 'stretch'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',

    },
    buttonWrapper: {
        flexDirection: 'column'
    },
    buttons: {
        flex: 1,
        backgroundColor: '#357bb5',
        borderRadius: 5,
        margin: 5
    },
    buttonText :{
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }
});

module.exports = LandingPage;
