'use strict';

var React = require('react-native');
var {
    Image,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ListView,
    Platform
    } = React;

var GLOBALS = require('./Globals');
var SePage3 = require('./search-page-3FM');

var MovieScreen = React.createClass({

    getInitialState: function() {
        var Drug = this.props.route.drug;
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows(Drug),
            drugTitle: GLOBALS.QUERY
        };
    },

    renderRow: function(rowData){
        return(
            <TouchableHighlight
                selectProduct={this.selectProduct}
                productData = {rowData}
                onPress={
                    function(){
                        this.selectProduct(this.productData)
                    }
                }
                style={{borderWidth: 1, borderRadius: 5, borderColor: 'black'}}
            >
                <View style={{ flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <Image
                        style={{height:40, width: 40, margin: 10}}
                        source={require('./img/pill.png')}
                    />
                    <View style={{flex:1, marginLeft: 10}}>
                        <Text
                            style={{fontSize: 20, marginTop:10,alignItems: 'center'}}>
                            {rowData.PROPRIETARYNAME[0].toUpperCase() + rowData.PROPRIETARYNAME.slice(1)}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {rowData.NONPROPRIETARYNAME[0].toUpperCase() + rowData.NONPROPRIETARYNAME.slice(1)}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {rowData.LABELERNAME[0].toUpperCase() + rowData.LABELERNAME.slice(1)}
                        </Text>
                    </View>
                </View>

            </TouchableHighlight>
        );
    },

    selectProduct: function(productObject){

        this.props.navigator.push({
            title: GLOBALS.titleCut(productObject.PROPRIETARYNAME),
            component: SePage3,
            productObject: productObject
        });

    },

    render: function() {

        return (
            <View style={{marginTop: 22, flex:1}}>
                <ScrollView style={{flex: 1}}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow}
                    />

                </ScrollView>
            </View>
        );
    },
});

var styles = StyleSheet.create({
    centerText: {
        alignItems: 'center',
    },
    noMoviesText: {
        marginTop: 80,
        color: '#888888',
    },
    separator: {
        height: 1,
        backgroundColor: '#eeeeee',
    },
    scrollSpinner: {
        marginVertical: 20,
    },
    rowSeparator: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        height: 1,
        marginLeft: 4,
    },
    rowSeparatorHide: {
        opacity: 0.0,
    },
    tabContent: {
        marginTop: 45,
        flex: 1,
    },
    tabText: {
        color: 'black',
        marginTop: 30,
        alignItems: 'center',
    },
});

module.exports = MovieScreen;