'use strict';

var React = require('react-native');
var {
    StyleSheet,
    Text,
    ListView,
    } = React;

var GLOBALS = require('./Globals');



// Main Component
var SearchPage = React.createClass({

    getInitialState: function() {

        var TEMP = [];
        loop1:
        for (var i in GLOBALS.DATA){
            if (GLOBALS.DATA[i].packages){
                loop2:
                for (var j in GLOBALS.DATA[i].packages){
                    loop3:
                    for (var jj in TEMP) {
                        if (TEMP[jj].NDCPACKAGECODE == GLOBALS.DATA[i].packages) {
                            break loop2;
                        }
                    }
                    TEMP.push(GLOBALS.DATA[i].packages[j]);

                }
            }
        }



        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows(TEMP)
        };
    },

    showProductPackages: function(){

    },

    render: function() {
        return (
            <ScrollView style={{marginTop: 64}}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={function(rowData){
                        return(
                            <TouchableHighlight
                                style={{borderWidth: 1, borderRadius: 5, borderColor: 'black'}}
                            >
                                <View>
                                    <Text>
                                        PROPRIETARYNAME:
                                    </Text>
                                    <Text>
                                        {GLOBALS.QUERY}
                                    </Text>
                                    <Text style={{flex: 1}}>
                                        {rowData.PACKAGEDESCRIPTION}
                                    </Text>
                                    <Text style={{flex: 1}}>
                                        NDCPACKAGECODE
                                    </Text>
                                    <Text style={{flex: 1}}>
                                        {rowData.NDCPACKAGECODE}
                                    </Text>
                                </View>
                            </TouchableHighlight>

                        );
                    }}
                />
            </ScrollView>
        );
    },
});


var styles = StyleSheet.create({

    loginButton: {
        width: 200,
        backgroundColor: 'red',
        borderRadius: 10,
        margin: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        fontSize: 20,
        color: 'black'
    }
});

module.exports = SearchPage;