'use strict';
var GLOBALS = require('./Globals');
var AccountPage = React.createClass({

    getInitialState: function(){

        var URL = CONFIG.API_URL + CONFIG.info + 'about&access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result.meta_value){
                    var a = JSON.parse(responseData.result.meta_value);
                }

                if (a){
                    if (a.title){
                        this.setState({
                            title: a.title
                        });
                    }
                    if (a.text){
                        this.setState({
                            text: a.text
                        });
                    }
                }
            })
            .done();

        return({
            title        : '',
            text         : ''
        });

    },

    render: function() {

        return (
            <ScrollView style={styles.container}>
                <View>
                    <View style={styles.blackBorderView}>

                        <Text style={{fontWeight: 'bold', margin: 10}}>
                            {this.state.title}
                        </Text>
                        <Text style={{fontWeight: 'normal', margin: 10}}>
                            {this.state.text}
                        </Text>
                        <Text>App v.{CONFIG.ios_app_version}</Text>

                    </View>
                </View>
            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 12
    },
    blackBorderView: {
        borderWidth:1,
        borderColor: 'black',
        margin: 10,
        paddingHorizontal:10,
        paddingVertical:25,
        alignItems: 'center'
    }
});


module.exports = AccountPage;