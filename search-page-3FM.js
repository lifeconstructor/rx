'use strict';

var React = require('react-native');
var {

    Image,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ListView,
    Platform

    } = React;

var GLOBALS = require('./Globals');

var SePage4 = require('./search-page-4FM');


var MovieScreen = React.createClass({

    getInitialState: function() {

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows(this.props.route.productObject.packages),
            drugTitle: GLOBALS.QUERY
        };
    },

    renderRow: function(rowData){
        return(
            <TouchableHighlight
                selectPackage={this.selectPackage}
                packageData = {rowData}
                onPress={
                    function(){
                        this.selectPackage(this.packageData)
                    }
                }
                style={styles.packageButton}
            >
                <View style={{ flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <Image
                        style={{height:40, width: 40, margin: 10}}
                        source={require('./img/bottle.png')}
                    />
                    <View style={{flex:1, marginLeft: 10}}>
                        <Text
                            style={{fontSize: 20, marginTop:10,alignItems: 'center'}}>
                            {rowData.PRODUCTNDC}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {rowData.PACKAGEDESCRIPTION}
                        </Text>
                    </View>
                </View>

            </TouchableHighlight>
        );
    },

    selectPackage: function(packageObject){

        this.props.navigator.push({
            title: GLOBALS.titleCut(packageObject.PRODUCTNDC),
            component: SePage4,
            packageObject: packageObject,
            productObject: this.props.route.productObject
        });

    },

    render: function() {

        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.innerView}>
                        <Image
                            style={styles.image}
                            source={require('./img/pill.png')}
                        />
                        <View style={styles.productWrapper}>
                            <Text
                                style={styles.titleText}>
                                {this.props.route.productObject.PROPRIETARYNAME[0].toUpperCase() + this.props.route.productObject.PROPRIETARYNAME.slice(1)}
                            </Text>
                            <Text
                                style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                                {this.props.route.productObject.NONPROPRIETARYNAME[0].toUpperCase() + this.props.route.productObject.NONPROPRIETARYNAME.slice(1)}
                            </Text>
                            <Text
                                style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                                {this.props.route.productObject.LABELERNAME[0].toUpperCase() + this.props.route.productObject.LABELERNAME.slice(1)}
                            </Text>
                        </View>
                    </View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow}
                    />
                </ScrollView>
            </View>
        );
    },
});

var styles = StyleSheet.create({
    container: {
        marginTop: 22,
        flex:1
    },
    scrollView: {
        flex: 1
    },
    innerView: {
        flexDirection: 'row',
        backgroundColor: 'lightgray',
        borderBottomWidth: 1,
        borderBottomColor: 'gray'
    },
    image:{
        height:40,
        width: 40,
        margin: 10
    },
    productWrapper: {
        flex:1,
        marginLeft: 10
    },
    titleText: {
        fontSize: 20,
        marginTop:10,
        alignItems: 'center'
    },
    packageButton: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'black'
    }

});

module.exports = MovieScreen;