'use strict';

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    TouchableHighlight,
    DatePickerIOS,
    NativeAppEventEmitter
} from 'react-native';

var RNCalendarReminders = require('react-native-calendar-reminders');

var PushNotification = require('react-native-push-notification');


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

PushNotification.configure({

    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function(token) {
        console.log( 'TOKEN:', token );
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
    },

    // ANDROID ONLY: (optional) GCM Sender ID.
    senderID: "YOUR GCM SENDER ID",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * IOS ONLY: (optional) default: true
     * - Specified if permissions will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     */
    requestPermissions: true,
});

var CONFIG = require('./config/config');

var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var AutoComplete = require('react-native-autocomplete');



var LoginScreen = React.createClass({

    getInitialState: function() {

        RNCalendarReminders.authorizeEventStore((error, auth) => {
            console.log(error);
            console.log(auth);

        });

        RNCalendarReminders.fetchAllReminders(reminders => {
            console.log(reminders);
        });

        return({
            drugName            : '',
            selectedDrug        : '',
            data                : [],
            drugsAutocomplete   : [],
            date                : new Date(),
            endDate             : new Date(),
            productId           : '',
            lifePeriod          : 800,
            prodIdError         : '',
            reguliarity         : 'ONCE',
            mode                : 'datetime',
            custom              : false,
            customAlarms        : [],
            Reguliarity         : ['ONCE', 'DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY', 'CUSTOM']
        });
    },

    componentWillMount () {
        this.eventEmitter = NativeAppEventEmitter.addListener('EventReminder', (reminders) => {
            this.setState({
                reminders: reminders
            });
        });
    },

    componentWillUnmount () {
        this.eventEmitter.remove();
    },

    getDataSource1: function(test: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(test);
    },

    onTyping: function (text) {

        this.setState({drugName: text});

        var URL = 'https://dplu-preview.data-rx.com/api/v1/drugs?name:prefix=' + this.state.drugName;


        fetch(URL, {
            method: 'GET',
            headers: {
                'Host': 'dplu-preview.data-rx.com',
                'Connection': 'keep-alive',
                'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                'Referer': 'https://dplu-preview.data-rx.com/',
                'Accept-Encoding': 'gzip, deflate, sdch',
                'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
            }
        })
            .catch((error) => {

                console.log(error);

            })
            .then((responseData) => {

                var drugs = JSON.parse(responseData._bodyText);

                var drugsAutocomplete = [];

                if (!drugs.message){



                    drugsAutocomplete = drugs.map(function (drug) {
                        return drug.name;
                    }).filter(function onlyUnique(value, index, self) {
                        return self.indexOf(value) === index;
                    });


                    this.setState({
                        drugsAutocomplete: drugsAutocomplete,
                        data: drugs
                    });

                }

            })
            .done();


        /*this.setState({drugName: text});

        var URL = CONFIG.API_URL + 'api/autocomplete-product'+ '?drug_name=' + this.state.drugName;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                var drugs = responseData.filter(function (drug) {
                    return drug.PROPRIETARYNAME.toLowerCase().startsWith(text.toLowerCase())
                }).map(function (drug) {
                    var Tier = '';



                    switch(Math.floor((Math.random() * 3) + 1)) {
                        case 1:
                            Tier = ' (NF)';
                            break;
                        case 2:
                            Tier = ' (Tier 1)';
                            break;
                        default:
                            Tier = ' (Tier 2)';
                    }

                    return drug.PROPRIETARYNAME + Tier;
                });

                this.setState({
                    drugsAutocomplete: drugs,
                    data: responseData
                });
            })
            .done();*/

    },

    onDateChange: function(date) {
        this.setState({date: date});
    },

    onDateChangeCustom: function(date, index) {

        var temp = this.state.customAlarms;
        temp[index] = date;

        this.setState({
            customAlarms: temp
        });
    },

    onEndDateChange: function(date) {
        this.setState({endDate: date});
    },

    addCustomDate: function() {

        this.setState({
            customAlarms: this.state.customAlarms.concat([new Date()])
        });


    },

    removeCustomDate: function() {

        this.setState({
            customAlarms: this.state.customAlarms.slice(0, -1)
    });

    },

    onChange: function(reguliarity){

        this.setState({reguliarity: reguliarity});
        if (reguliarity == 'DAILY'){
            this.setState({mode: 'time'})
        } else {
            this.setState({mode: 'datetime'})
        }

        if (reguliarity == 'CUSTOM'){
            this.setState({custom: true})
        } else {
            this.setState({custom: false})
        }

    },

    updateLocalReminders: function(){

        PushNotification.cancelAllLocalNotifications();

        var URL = 'http://v2-0-rxscout.pilgrimconsulting.com/backend/api/get-reminders?access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                /*this.setState({
                    dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                    dataRaw: responseData.result
                });*/



                if (responseData && responseData.result && responseData.result.length){

                    responseData.result.map((data, index) =>{

                        var reminderInfo = JSON.parse(data.reminder);
                        console.log(reminderInfo);



                        switch(reminderInfo.recurrence) {
                            case 'ONCE':

                                var dateOnce = '';
                                dateOnce = new Date(reminderInfo.startDate);

                                if (dateOnce > new Date()) {
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: (dateOnce).toISOString()
                                    });
                                }

                                break;
                            case 'DAILY':
                                var dataD = new Date(reminderInfo.startDate);

                                var DAILY = 0;

                                while (dataD < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    DAILY++;

                                    if (dataD > new Date()) {
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataD.toISOString()
                                        });
                                    }
                                    dataD.setDate(dataD.getDate() + 1);
                                }
                                if (!DAILY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'WEEKLY':
                                var dataW = new Date(reminderInfo.startDate);

                                var WEEKLY = 0;

                                while (dataW < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    WEEKLY++;

                                    if (dataW > new Date()){
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataW.toISOString()
                                        });
                                    }
                                    dataW.setDate(dataW.getDate() + 7);
                                }

                                if (!WEEKLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'MONTHLY':
                                var dataM = new Date(reminderInfo.startDate);

                                var MONTHLY = 0;

                                while (dataM < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    MONTHLY++;

                                    if (dataM > new Date()){
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataM.toISOString()
                                        });
                                    }

                                    dataM.setMonth(dataM.getMonth() + 1);
                                }

                                if (!MONTHLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'YEARLY':
                                var dataY = new Date(reminderInfo.startDate);

                                var YEARLY = 0;

                                while (dataY < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    YEARLY++;

                                    if (dataY > new Date()) {
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataY.toISOString()
                                        });
                                    }

                                    dataY.setYear(dataY.getYear() + 1);
                                }

                                if (!YEARLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }
                                break;
                            case 'CUSTOM':

                                var dateCustomF = '';
                                dateCustomF = new Date(reminderInfo.startDate);

                                if (dateCustomF > new Date()) {
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: (dateCustomF).toISOString()
                                    });
                                }


                                if (reminderInfo.alarms.length){

                                    reminderInfo.alarms.map((dataCustom, index) => {
                                        if (dataCustom > new Date()) {
                                            PushNotification.localNotificationSchedule({
                                                message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                                date: dataCustom
                                            });
                                        }
                                })
                                }

                                break;
                            default:
                                console.log('DEFAULT');
                                break;
                        }
                    });

                }
            })
            .done();


    },

    addReminder: function(){

        if (!this.state.selectedDrug) {
            AlertIOS.alert('Drug Title cannot be empty');
            return;
        }

        if ((this.state.date < new Date && this.state.reguliarity != "DAILY")
            || (this.state.date < new Date && this.state.reguliarity != "WEEKLY")
            || (this.state.date < new Date && this.state.reguliarity != "MONTHLY")
            || (this.state.date < new Date && this.state.reguliarity != "YEARLY") ) {

            AlertIOS.alert('Reminder cannot be set.');
            return;
        }


        var drugTitle = this.state.selectedDrug;

        drugTitle = drugTitle.replace(" (NF)", "");
        drugTitle = drugTitle.replace(" (Tier 1)", "");
        drugTitle = drugTitle.replace(" (Tier 2)", "");



        /* AlertIOS.alert('This feature is temporary disabled');*/

        for (var i in this.state.data) {
            if (this.state.data[i].PROPRIETARYNAME == drugTitle){
                this.setState({
                    productId: this.state.data[i].id
                });
            }
        }
        var URL = 'http://v2-0-rxscout.pilgrimconsulting.com/backend/api/add-reminder';

        var type = this.state.reguliarity != "CUSTOM" ? "REGULAR": "CUSTOM";

        if (this.state.reguliarity == 'ONCE') {
            this.setState({
                endDate: this.state.date
            });
        }

        if (this.state.reguliarity == 'CUSTOM') {
            var dateMax = new Date(Math.max( ...this.state.customAlarms));

            dateMax = dateMax > this.state.date ? dateMax : this.state.date;

            this.setState({
                endDate: dateMax
            });
        }

        var test = JSON.stringify (
            {
                "product_id": this.state.selectedDrug,
                "id_local": this.state.date.getTime(),
                "startDate": this.state.date.getTime(),
                "location": "Not specified.",
                "notes": "Check RxScout app for details.",
                "alarms": this.state.customAlarms,
                "type": type,
                "recurrence": this.state.reguliarity,
                "every": "1",
                "months": [],
                "days_of_month": [],
                "days_of_week": [],
                "endDate": this.state.endDate.getTime()

            }
        );

        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            },
            body: test
        })
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.errors.product_id){
                    this.setState({
                        prodIdError: responseData.errors.product_id
                    });
                } else {
                    this.setState({
                        prodIdError: ''
                    });
                }

                if (responseData.success){

                    AlertIOS.alert('Reminder saved successfully');

                    /*RNCalendarReminders.saveReminder('Don\'t forget to buy drugs.' + this.state.selectedDrug, {
                        location: 'Location not specified.',
                        notes: 'Check RxScout app for details.',
                        startDate: this.state.date.toISOString(),
                        alarms: [{
                            date: this.state.date.toISOString() // or absolute date
                        }]
                    });*/

                    this.updateLocalReminders();


                    this.props.navigator.pop();
                }
            })
            .done();
    },

    render: function() {
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <View style={[styles.container, {backgroundColor: GLOBALS.backgroundCl}]}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 15, marginTop: 15}}>
                            Drug Title
                        </Text>


                        <Text style={{color: 'red', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            {this.state.prodIdError}
                        </Text>

                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            Reminder Type
                        </Text>

                        <PickerIOS
                            style={{width: 300}}
                            selectedValue={this.state.reguliarity}
                            onValueChange={(reguliarity) => {
                                this.onChange(reguliarity);
                            }}>

                            {this.state.Reguliarity.map((reguliarity) => (
                                <PickerItemIOS
                                    key={reguliarity}
                                    value={reguliarity}
                                    label={reguliarity}
                                />
                            ))}
                        </PickerIOS>


                        <View style={{width: width, position: 'absolute', top: 40, alignItems: 'center'}}>
                            <AutoComplete
                                onTyping={this.onTyping}
                                onSelect={(e) => this.setState({selectedDrug: e})}
                                onBlur={() => {}}
                                onFocus={() => {}}
                                onSubmitEditing={(e) => {}}
                                onEndEditing={(e) => {}}

                                suggestions={this.state.drugsAutocomplete}

                                placeholder='Drug title'
                                style={[styles.autocomplete, ]}
                                clearButtonMode='always'
                                returnKeyType='go'
                                textAlign='center'
                                clearTextOnFocus={true}

                                maximumNumberOfAutoCompleteRows={6}
                                applyBoldEffectToAutoCompleteSuggestions={true}
                                reverseAutoCompleteSuggestionsBoldEffect={true}
                                showTextFieldDropShadowWhenAutoCompleteTableIsOpen={false}
                                autoCompleteTableViewHidden={false}

                                autoCompleteTableBorderColor='lightblue'
                                autoCompleteTableBackgroundColor='azure'
                                autoCompleteTableCornerRadius={10}
                                autoCompleteTableBorderWidth={1}

                                autoCompleteRowHeight={35}

                                autoCompleteFontSize={15}
                                autoCompleteRegularFontName='Helvetica Neue'
                                autoCompleteBoldFontName='Helvetica Bold'
                                autoCompleteTableCellTextColor={'black'}
                            />
                        </View>

                        <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 3, marginTop: 3}}>
                        </Text>


                        <DatePickerIOS
                            date={this.state.date}
                            onDateChange={this.onDateChange}
                            mode={this.state.mode}
                        />


                        {this.state.reguliarity == 'ONCE' || this.state.reguliarity == 'CUSTOM'?
                            <Text></Text>
                            :
                            <View style={{alignItems:'center'}}>
                                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 3, marginTop: 3}}>
                                    End Date
                                </Text>

                                <DatePickerIOS
                                    date={this.state.endDate}
                                    minimumDate={this.state.date}
                                    onDateChange={this.onEndDateChange}
                                    mode='date'
                                />
                            </View>

                        }


                        {this.state.custom?
                            <View style={{alignItems: 'center'}}>

                                {this.state.customAlarms.map((alarm, index, test) => (
                                    <DatePickerIOS
                                        date={this.state.customAlarms[index]}
                                        minimumDate={new Date()}
                                        onDateChange={(date) =>this.onDateChangeCustom(date, index)}
                                        mode={this.state.mode}
                                    />
                                ))}

                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <TouchableHighlight
                                        onPress={this.addCustomDate}
                                        style={styles.addReminderButton}
                                    >
                                        <Text style={styles.loginText}>
                                            Add Another Date
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <TouchableHighlight
                                        onPress={this.removeCustomDate}
                                        style={styles.addReminderButton}
                                        >
                                        <Text style={styles.loginText}>
                                            Remove Date
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            :
                            <Text></Text>
                        }




                    </View>
                    <View style={{marginHorizontal: 40, alignItems: 'center'}}>
                        <TouchableHighlight
                            onPress={this.addReminder}
                            style={styles.addReminderButton}
                        >
                            <Text style={styles.loginText}>
                                Add Reminder
                            </Text>
                        </TouchableHighlight>
                    </View>

                </ScrollView>

            </View>
        );
    }
});

const styles = StyleSheet.create({

    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 5
    },
    container: {
        marginTop: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    input:{
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 5,
        padding: 5,
        flex: 1
    },
    addReminderButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginBottom: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white',
        fontSize: 20
    }

});

module.exports = LoginScreen;