'use strict';

var React = require('react-native');

var {
        View,
        Text,
        Navigator,
        Dimensions,
        Platform,
    
    } = React;

import TabNavigator from 'react-native-tab-navigator';

var window = Dimensions.get('window');
var invariant = require('invariant');

var GLOBALS  = require('./Globals');

var RxPage = require('./rx-page-1');
var RemPage = require('./reminders-list');
var SePage1 = require('./search-page-1');
var MemberCard = require('./member-card');

var AcPage = require('./account-page');

var Menu = require('react-native-menu');
var { MenuContext, MenuOptions, MenuOption, MenuTrigger } = Menu;

var NavigationBarRouteMapperRx = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity
                onPress={() => navigator.pop()}
                style={styles.navBarLeftButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    {previousRoute.title}
                </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};

var NavigationBarRouteMapperReminders = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity
                onPress={() => navigator.pop()}
                style={styles.navBarLeftButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    {previousRoute.title}
                </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};

var NavigationBarRouteMapperSearch = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity
                onPress={() => navigator.pop()}
                style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};


var NavigationBarRouteMapperPillId = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity
                onPress={() => navigator.pop()}
                style={styles.navBarLeftButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    {previousRoute.title}
                </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};

var NavigationBarRouteMapperAccount = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity
                onPress={() => navigator.pop()}
                style={styles.navBarLeftButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    {previousRoute.title}
                </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};

var MainTabs = React.createClass({

    _renderContentRx: function(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderSceneSearch}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                    <Navigator.NavigationBar
                      routeMapper={NavigationBarRouteMapperRx}
                      style={styles.navBar}
                    />
                }
                navigationBarHidden={true}
                initialRoute={{
                  title: 'My Rx',
                  component: RxPage
                }}
            />
        );
    },

    _renderContentRe: function(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperReminders}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={true}
                initialRoute={{
                      title: 'Reminders',
                      component: RemPage
                    }}
            />
        );
    },

    _renderContentSe: function(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderSceneSearch}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperSearch}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: 'Search',
                      component: SePage1
                    }}
            />
        );
    },

    _renderContentPi: function(color: string, pageHeader: string, pageText?: string, num?: number) {

        let discountName = 'Discount Card';
        if (this.props.loginObject.is_member) {
            discountName = 'Member Card';
        }

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperPillId}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: discountName,
                      component: MemberCard,
                      textForCard: discountName
                    }}
            />
        );
    },

    _renderContentAc: function(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperAccount}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: GLOBALS.titleCut('My RxPreferred'),
                      component: AcPage
                    }}
            />
        );
    },

    getInitialState: function() {
        return {
            selectedTab: 'account',
            notifCount: 0,
            presses: 0,
        };
    },

    measureMainComponent: function() {
        this._root.measure((ox, oy, width, height, px, py) => {
            this.setState({
                width: width,
                height: height,
                ox: ox,
                oy:oy,
                px: px,
                py: py

            });
        });
    },

    renderSceneSearch: function(route, navigator) {
        var Component = route.component;
        return (
            <View style={[styles.container, {backgroundColor: CONFIG.backgroundCl}]}>
                <MenuContext style={{ flex: 1}} ref="MenuContext">
                <Component
                    route={route}
                    navigator={navigator}
                    topNavigator={navigator} />
                </MenuContext>
            </View>
        )
    },

    renderScene: function(route, navigator) {
        var Component = route.component;

        var margTop = (this.state.width > 480)? 64 : 44;

        return (
            <View style={[styles.container, {marginTop: margTop, backgroundColor: CONFIG.backgroundCl}]}
                  ref={component => this._root = component}
                  onLayout={this.measureMainComponent.bind(null,this)}
            >
                <Component
                    route={route}
                    navigator={navigator}
                    topNavigator={this.props.topNavigator} />
            </View>
        )
    },


    render: function() {
        //https://github.com/exponentjs/react-native-tab-navigator

        let discountName = 'Discount Card';
        if (this.props.loginObject.is_member) {
            discountName = 'Member Card';
        }
        let tempTitle = GLOBALS.titleCut("My RxPreferred");


        return (
            <TabNavigator
                tintColor='white'
                barTintColor="#f99b53"
            >
                {/*<TabNavigator.Item
                    title="My Rx"
                    renderIcon={() =>
                        <Image source={GLOBALS.icons.tabRx} style={{height:33, width: 33}}/>
                    }
                    renderSelectedIcon={() =>
                        <Image source={GLOBALS.icons.tabRx2} style={{height:33, width: 33}}/>
                    }
                    selected={this.state.selectedTab === 'myrx'}
                    onPress={
                    () => {
                        if (this.state.selectedTab !== 'myrx'){
                            GLOBALS.menuMap.activeTab = 'myrx';

                            this.setState({
                                selectedTab: 'myrx'
                            });

                        }
                    }}>
                    {this._renderContentRx('#b14A8C', 'My Rx')}
                </TabNavigator.Item>*/}

                <TabNavigator.Item
                    title="Reminders"
                    renderIcon={() =>
                        <Image source={GLOBALS.icons.tabReminder} style={{height:33, width: 33}}/>
                    }
                    renderSelectedIcon={() =>
                        <Image source={GLOBALS.icons.tabReminder2} style={{height:33, width: 33}}/>
                    }
                    selected={this.state.selectedTab === 'reminders' }
                    onPress={
                    () => {
                      if (this.state.selectedTab !== 'reminders'){
                      GLOBALS.menuMap.activeTab = 'reminders';
                        this.setState({
                          selectedTab: 'reminders'
                        });
                      }
                    }}>
                    {this._renderContentRe('#b14A8C', 'Reminders')}
                </TabNavigator.Item>

                <TabNavigator.Item
                    title="Search"
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabSearch} style={{height:32, width: 32}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabSearch2} style={{height:32, width: 32}}/>
                    }
                    selected={this.state.selectedTab === 'search'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'search'){
                      GLOBALS.menuMap.activeTab = 'search';
                        this.setState({
                          selectedTab: 'search'
                        });
                      }
                  }}>
                    {this._renderContentSe('#c1fA8C', 'search')}
                </TabNavigator.Item>

                <TabNavigator.Item
                    title={discountName}
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabPills} style={{height:20, width: 30, margin: 4}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabPills2} style={{height:20, width: 30, margin: 4}}/>
                    }
                    selected={this.state.selectedTab === 'pills'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'pills'){
                      GLOBALS.menuMap.activeTab = 'pills';

                        this.setState({
                          selectedTab: 'pills'
                        });
                      }
                  }}>
                    {this._renderContentPi('#d83E33', 'Pills')}
                </TabNavigator.Item>

                <TabNavigator.Item
                    title= {tempTitle}
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabUser} style={{height:32, width: 32}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabUser2} style={{height:32, width: 32}}/>
                    }
                    selected={this.state.selectedTab === 'account'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'account'){
                      GLOBALS.menuMap.activeTab = 'account';

                        this.setState({
                          selectedTab: 'account'
                        });
                      }
                  }}>
                    {this._renderContentAc('#e1551C', 'My RxPreferred', this.state.presses)}
                </TabNavigator.Item>
            </TabNavigator>
        )
    }


});

var styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    },

    messageText: {
        fontSize: 17,
        fontWeight: '500',
        padding: 15,
        marginTop: 50,
        marginLeft: 15,
    },

    button: {
        backgroundColor: 'white',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#CDCDCD',
    },

    buttonText: {
        fontSize: 17,
        fontWeight: '500',
    },

    navBar: {
        backgroundColor: GLOBALS.COLORS.orange,
    },

    navBarText: {
        fontSize: 16,
        marginVertical: 10,
    },

    navBarTitleText: {
        color: 'black',
        fontWeight: '500',
        marginVertical: 9,
    },

    navBarLeftButton: {
        paddingLeft: 10,
    },

    navBarRightButton: {
        paddingRight: 10,
    },

    navBarButtonText: {
        color: 'gray',
    },

    scene: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#EAEAEA',
    },

});

module.exports = MainTabs;