'use strict';

import Spinner from 'react-native-loading-spinner-overlay';

var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var LoginScreen = React.createClass({

    getInitialState: function() {
        return({
            username        : '',
            fname           : '',
            lname           : '',
            password        : '',
            email           : '',
            groupNumber     : '',
            memberId        : '',
            phoneNumber     : '',
            usernameError   : '',
            fnameError      : '',
            lnameError      : '',
            passwordError   : '',
            emailError      : '',
            groupNumberError: '',
            memberIdError   : '',
            phoneNumberError: '',
            message         : '',
            keyBoardHeight  : 0,
            isLoading       : false
        });
    },

    onSignUp: function(){

        if(CONFIG.PRODUCTION_MODE == 'PBM' && this.state.groupNumber == '' && this.state.memberId == ''){

            AlertIOS.alert('Please enter Group and Member ID\'s');
            return;

        }

        this.setState({
            usernameError   : '',
            passwordError   : '',
            emailError      : '',
            fnameError      : '',
            lnameError      : '',
            groupNumberError: '',
            memberIdError   : '',
            isLoading       : true
        });

        var URL = CONFIG.API_URL + CONFIG.sign_up_url;


        var test = JSON.stringify({
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            first_name: this.state.fname,
            last_name: this.state.lname,
            group_number: this.state.groupNumber,
            member_id : this.state.memberId,
            phone_number: this.state.phoneNumber
        });


        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: test
        }).then((response) => response.json())
            .then((responseText) => {

                this.setState({
                    isLoading: false
                });

                if (responseText.errors){
                    if (responseText.errors.username){
                        this.setState({
                            usernameError: responseText.errors.username[0]
                        });
                    }
                    if (responseText.errors.password){
                        this.setState({
                            passwordError: responseText.errors.password[0]
                        });
                    }
                    if (responseText.errors.email){
                        this.setState({
                            emailError: responseText.errors.email[0]
                        });
                    }
                    if (responseText.errors.first_name){
                        this.setState({
                            fnameError: responseText.errors.first_name[0]
                        });
                    }
                    if (responseText.errors.last_name){
                        this.setState({
                            lnameError: responseText.errors.last_name[0]
                        });
                    }

                    if (responseText.errors.member_id){
                        this.setState({
                            memberIdError: responseText.errors.member_id[0]
                        });
                    }
                    if (responseText.errors.group_number){
                        this.setState({
                            groupNumberError: responseText.errors.group_number[0]
                        });
                    }
                    if (responseText.errors.phone_number){
                        this.setState({
                            groupNumberError: responseText.errors.phone_number[0]
                        });
                    }
                }

                if (responseText.auth_key){

                    /*this.props.route.hideNavBar();*/

                    GLOBALS.API_KEY = responseText.auth_key;

                    this.props.topNavigator.push({
                        title: '',
                        component: Tabs,
                        loginObject: responseText,
                        selectedTab: 'account'
                    });
                }

            })
            .catch((error) => {

                this.setState({
                    error: 'Error connecting to server'
                });
            });
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this))
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this))
    },

    keyboardWillShow (e) {

        this.setState({keyBoardHeight: e.endCoordinates.height})

    },

    keyboardWillHide (e) {

        this.setState({keyBoardHeight: 0});

        //this.refs._scrollView.scrollTo(200);
    },


    render: function() {

        var content = this.state.isLoading
            ?
            <View style={{ flex: 1 }}>
                <Spinner visible={true} />
            </View>
            :
            <View style={[styles.container, {marginBottom: this.state.keyBoardHeight, backgroundColor: CONFIG.backgroundCl}]}>

                <Text style={{color: 'red', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.props.route.message}
                </Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Username
                    </Text>
                    <Text style={{color:'red'}}>*</Text>
                </View>

                <TextInput
                    style={styles.input}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.usernameError}
                </Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Password
                    </Text>
                    <Text style={{color:'red'}}>*</Text>
                </View>
                <TextInput
                    style={styles.input}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    secureTextEntry={true}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.passwordError}
                </Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Email
                    </Text>
                    <Text style={{color:'red'}}>*</Text>
                </View>
                <TextInput
                    style={styles.input}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    keyboardType={'email-address'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.emailError}
                </Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        First Name
                    </Text>
                </View>

                <TextInput
                    style={styles.input}
                    onChangeText={(fname) => this.setState({fname})}
                    value={this.state.fname}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.fnameError}
                </Text>


                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Last Name
                    </Text>
                </View>

                <TextInput
                    style={styles.input}
                    onChangeText={(lname) => this.setState({lname})}
                    value={this.state.lname}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.lnameError}
                </Text>


                <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Member ID
                </Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(memberId) => this.setState({memberId})}
                    value={this.state.memberId}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.memberIdError}
                </Text>


                <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Group ID
                </Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(groupNumber) => this.setState({groupNumber})}
                    value={this.state.groupNumber}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.groupNumberError}
                </Text>

                <Text style={{color: 'white', fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Phone Number
                </Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                    value={this.state.phoneNumber}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                />
                <Text style={{color: GLOBALS.COLORS.red, fontSize: 24, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    {this.state.phoneNumberError}
                </Text>



                <TouchableHighlight
                    onPress={this.onSignUp}
                    style={styles.loginButton}
                >
                    <Text style={styles.loginText}>
                        Sign Up
                    </Text>
                </TouchableHighlight>

            </View>;

        return (
            <ScrollView ref='_scrollView' keyboardShouldPersistTaps={true} >

                {content}

            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 64
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'white',
        borderRadius: 5,
        fontSize: 20
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginBottom: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white',
        fontSize: 20
    }

});

module.exports = LoginScreen;