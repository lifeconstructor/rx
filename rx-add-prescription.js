'use strict';

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    TouchableHighlight
} from 'react-native';


var CONFIG = require('./config/config');


var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var LoginScreen = React.createClass({



    getInitialState: function() {
        return({
            username        : '',
            password        : '',
            usernameError   : '',
            passwordError   : ''
        });
    },

    onLogin: function(){



    },

    render: function() {

        return (
            <View style={styles.container}>

                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Drug Title
                </Text>

                <TextInput
                    style={styles.input}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                />

                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Dosage
                </Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                />

                <TouchableHighlight
                    onPress={this.onLogin}
                    style={styles.loginButton}
                >
                    <Text style={styles.loginText}>
                        Add Prescription
                    </Text>
                </TouchableHighlight>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }

});

module.exports = LoginScreen;