'use strict';

var React = require('react-native');
var {
      Image,
      PixelRatio,
      ScrollView,
      StyleSheet,
      Text,
      View,
      ListView,
      Platform
    } = React;

var GLOBALS2 = require('./Globals');
var MapScreen = require('./map-screen');


var MovieScreen = React.createClass({
  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      dataSource: ds.cloneWithRows(
          GLOBALS2.medicines.drugs.filter(
              function(obj){
                if (obj.id == GLOBALS2.menuMap.myrx.currentDrugId) {
                  return true;
                } else {
                  return false;
                }
              })[0].locations
      ),
    };
  },


  selectLocation: function(locationId: Number, locationTitle: String) {
    if (Platform.OS === 'ios') {
      GLOBALS2.menuMap.myrx.count++;
      GLOBALS2.menuMap.myrx.currentLocationId = locationId;
      GLOBALS2.menuMap.myrx.titles[GLOBALS2.menuMap.myrx.count] = 'Locations';
      this.props.navigator.push({
        title: GLOBALS.titleCut('Locations'),
        component: MapScreen,
        passProps: {},
      });
    } else {
      dismissKeyboard();
      this.props.navigator.push({
        title: GLOBALS.titleCut(drug.title),
        name: 'drug',
        drug: drug
      });
    }
  },

  render: function() {
    var Drug = GLOBALS2.medicines.drugs.filter(
        function(obj){
          if (obj.id == GLOBALS2.menuMap.myrx.currentDrugId) {
            return true;
          } else {
            return false;
          }
        })[0];



    return (
        <ScrollView>
          <View style={{marginTop: 45, flexDirection: 'row', backgroundColor: 'white'}}>
            <Image
                style={{height:40, width: 40, margin: 10}}
                source={require('./pill.png')}
            />
            <View style={{marginLeft: 10}}>
              <Text>{Drug.title && Drug.title[0].toUpperCase() + Drug.title.slice(1)}</Text>
              <Text>{Drug.quantity}</Text>
              <Text>{Drug.dose}</Text>
            </View>
          </View>

          <Text style={{padding:10}}>Franklin, TN</Text>

          <ListView
              dataSource={this.state.dataSource}
              renderRow={(rowData) =>
            <TouchableHighlight
              onPress={()=>this.selectLocation(rowData.id, rowData.title)}
            >
              <View style={{backgroundColor:"#fdd287", borderBottomWidth: 1, borderBottomColor: "red", flexDirection: 'row', marginBottom: 3}}>
                <View style={{flex:0.7}}>
                  <Text style={{marginLeft: 5, marginTop: 10, marginBottom:3}}>{rowData.title}</Text>
                  <Text style={{marginLeft: 5, marginTop: 3, marginBottom:10}}>{rowData.distance_rounded}</Text>
                </View>
                <View style={{flex:0.3, borderWidth: 1, borderColor: "white", margin: 10, alignItems: 'center', borderRadius: 5}}>
                  <Text style={{margin: 2}}>{rowData.discountType}</Text>
                  <Text style={{margin: 2}}>{rowData.price}</Text>
                </View>
              </View>
            </TouchableHighlight>}
          />
        </ScrollView>
    );
  },
});



var styles = StyleSheet.create({
  contentContainer: {
    padding: 10,
  },
  rightPane: {
    justifyContent: 'space-between',
    flex: 1,
  },
  movieTitle: {
    flex: 1,
    fontSize: 16,
    fontWeight: '500',
  },
  rating: {
    marginTop: 10,
  },
  ratingTitle: {
    fontSize: 14,
  },
  ratingValue: {
    fontSize: 28,
    fontWeight: '500',
  },
  mpaaWrapper: {
    alignSelf: 'flex-start',
    borderColor: 'black',
    borderWidth: 1,
    paddingHorizontal: 3,
    marginVertical: 5,
  },
  mpaaText: {
    fontFamily: 'Palatino',
    fontSize: 13,
    fontWeight: '500',
  },
  mainSection: {
    flexDirection: 'row',
  },
  detailsImage: {
    width: 134,
    height: 200,
    backgroundColor: '#eaeaea',
    marginRight: 10,
  },
  separator: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: 1 / PixelRatio.get(),
    marginVertical: 10,
  },
  castTitle: {
    fontWeight: '500',
    marginBottom: 3,
  },
  castActor: {
    marginLeft: 2,
  },
});

module.exports = MovieScreen;
