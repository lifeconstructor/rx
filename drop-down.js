const {
  Dimensions,
  StyleSheet,
  Component,
  View,
  ScrollView
} = React;

var window = Dimensions.get('window');


class Items extends Component {

  constructor(props) {
    super(props);

    this.state = {
      filter: '',
      height: this.props.test
    }
  }

  render() {
    var { SearchBarHeight, SearchBarWidth, filter, test } = this.props;

    var stl = filter === '' ? [] : [styles.container, {height: this.props.test}]

    var comp = filter === '' ?
    <View/> :
          <ScrollView style={{ flex: 1 }}>
            {this.props.children}
          </ScrollView>

    return (
        <View style={[stl, {width: SearchBarWidth-6}]}>{comp}</View>
    );
  }

}



const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 6,
    borderColor: '#BDBDC1',
    borderWidth: 2 / window.scale,
    borderTopColor: 'transparent',
    backgroundColor: 'white',
  },
})

Items.propTypes = {
  positionX: React.PropTypes.number,
  positionY: React.PropTypes.number,
  show: React.PropTypes.bool,
  onPress: React.PropTypes.func
};

Items.defaultProps = {
  width: 0,
  height: 0,
  positionX: 0,
  positionY: 0,
  show: false,
  onPress: () => {}
};

module.exports = Items;
