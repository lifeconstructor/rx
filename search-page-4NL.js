'use strict';

var React = require('react-native');
var {

    Image,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ListView,
    Platform

    } = React;

var GLOBALS = require('./Globals');

var SePage4 = (Platform.OS === 'ios')? require('./map-screen') : require('./pharmacy-storea');




var MovieScreen = React.createClass({


    getInitialState: function() {

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows([]),
            drugTitle: GLOBALS.QUERY
        };
    },

    componentWillReceiveProps: function(){

        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});

                var URL_for_pharmacies = CONFIG.API_URL + 'api/nearby-pharmacies?lat=' + position.coords.latitude + '&lng=' + position.coords.longitude + '&distance=10';


                fetch(URL_for_pharmacies)
                    .then((response) => response.json())
                    .catch((error) => {



                    })
                    .then((responseData) => {



                        this.setState({
                            dataSource: this.getDataSource1(responseData.result)
                        });

                    })
                    .done();
            });
    },


    componentDidMount(){

        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});


                var URL_for_pharmacies = CONFIG.API_URL + 'api/nearby-pharmacies?lat='+position.coords.latitude+'&lng='+position.coords.longitude+'&distance=10';



                fetch(URL_for_pharmacies)
                    .then((response) => response.json())
                    .catch((error) => {


                    })
                    .then((responseData) => {

                        this.setState({
                            dataSource: this.getDataSource1(responseData.result)
                        });

                    })
                    .done();


            },
            (error) => alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
        this.watchID = navigator.geolocation.watchPosition((position) => {
            var lastPosition = JSON.stringify(position);
            this.setState({lastPosition});
        });
    },

    getDataSource1: function(test: Array<any>): ListView.DataSource {


        return this.state.dataSource.cloneWithRows(test);
    },


    renderRow: function(rowData){


        var float = rowData.distance_text;
        float = parseFloat(float).toFixed(2);

        var randomPrice = '$' + (Math.floor(Math.random() * (100 - 10)) + 10) + '.' + (Math.floor(Math.random() * (100 - 10)) + 10);

        return(
            <TouchableHighlight
                selectLocation={this.selectLocation}
                locationData = {rowData}
                onPress={
                    function(){
                        this.selectLocation(this.locationData)
                    }
                }
                style={styles.packageButton}
            >
                <View style={{ flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <Image
                        style={{height:40, width: 40, margin: 10}}
                        source={require('./img/bottle.jpeg')}
                    />
                    <View style={{flex:0.6, marginLeft: 10}}>
                        <Text
                            style={{fontSize: 14, marginTop:10,alignItems: 'center'}}>
                            {this.props.route.packageObject.PACKAGEDESCRIPTION}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {rowData.name}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {float} miles
                        </Text>

                    </View>

                    {/*<View style={{flex:0.6, marginLeft: 10, alignItems: 'center', flexDirection: 'column'}}>
                        <View style={{
                        borderWidth: 1,
                        borderColor: 'green',
                        paddingHorizontal: 10,
                        paddingVertical: 10,
                        borderRadius: 10,
                        marginTop: 20}
                        }>
                            <Text style={{color: 'green', margin: 3}}>
                                DISCOUNT
                            </Text>
                            <Text style={{color: 'green', margin: 3}}>
                                {randomPrice}
                            </Text>
                        </View>
                    </View>*/}
                </View>

            </TouchableHighlight>
        );
    },

    selectLocation: function(locationObject){


        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});


                var URL_for_polyline = 'https://maps.googleapis.com/maps/api/directions/json?&origin=' + position.coords.latitude + ',' + position.coords.longitude + '&destination='+locationObject.lat+','+locationObject.lng+'&mode=driving&language=en';


                fetch(URL_for_polyline)
                    .then((response) => response.json())
                    .catch((error) => {


                    })
                    .then((responseData) => {


                        this.props.navigator.push({
                            title: 'Map',
                            component: SePage4,

                            locationObject: locationObject,
                            polyline: responseData.routes[0].overview_polyline.points
                        });

                       /* this.props.navigator.push({
                            title: GLOBALS.titleCut(this.props.route.packageObject.PRODUCTNDC),
                            component: SePage4,
                            packageObject: this.props.route.packageObject,
                            productObject: this.props.route.productObject,
                            locationObject: locationObject,
                            polyline: responseData.routes[0].overview_polyline.points
                        });*/

                    })
                    .done();
            });



    },

    render: function() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>

                    <View style={styles.innerView}>
                        <Image
                            style={styles.image}
                            source={require('./img/pill.png')}
                        />
                        <View style={styles.productWrapper}>
                            <Text
                                style={styles.titleText}>
                                {this.props.route.productObject.PROPRIETARYNAME[0].toUpperCase() + this.props.route.productObject.PROPRIETARYNAME.slice(1)}
                            </Text>
                            <Text
                                style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                                {this.props.route.productObject.NONPROPRIETARYNAME[0].toUpperCase() + this.props.route.productObject.NONPROPRIETARYNAME.slice(1)}
                            </Text>
                            <Text
                                style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                                {this.props.route.productObject.LABELERNAME[0].toUpperCase() + this.props.route.productObject.LABELERNAME.slice(1)}
                            </Text>
                        </View>
                    </View>

                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow}
                    />
                </ScrollView>
            </View>
        );
    },
});

var styles = StyleSheet.create({
    container: {
        marginTop: ((Platform.OS === 'ios')?64:55),
        flex:1
    },
    scrollView: {
        flex: 1
    },
    innerView: {
        flexDirection: 'row',
        backgroundColor: 'lightgray',
        borderBottomWidth: 1,
        borderBottomColor: 'gray'
    },
    image:{
        height:40,
        width: 40,
        margin: 10
    },
    productWrapper: {
        flex:1,
        marginLeft: 10
    },
    titleText: {
        fontSize: 20,
        marginTop:10,
        alignItems: 'center'
    },
    packageButton: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'black'
    }

});

module.exports = MovieScreen;