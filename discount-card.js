'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    TouchableHighlight
} from 'react-native';


var Benefits1 = React.createClass({

    render: function() {

        return (
            <View style={styles.container}>

                <Text style={{color: '#357bb5', fontSize: 32, textAlign: 'center', marginBottom: 3, marginTop: 5}}>
                    {CONFIG.OWNER_NAME} Benefits
                </Text>
                <Text style={{color: 'gray', fontSize: 14, textAlign: 'center', marginBottom: 5}}>
                    TRANSFORMING PHARMACY BENEFIT MANAGEMENT
                </Text>
                <View style={{backgroundColor: 'darkblue', alignSelf: 'stretch'}}>
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Pharmacy {this.props.route.textForCard}
                    </Text>
                </View>

                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Group: {CONFIG.OWNER_NAME}
                </Text>
                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    PCN: DRX
                </Text>
                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    BIN: 610269
                </Text>
                <Text style={{color: 'black', fontSize: 14, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    This is not insurance
                </Text>

            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        borderRadius: 20,
        borderColor: 'lightgray',
        borderWidth: 1,
        marginTop: 65,
        marginHorizontal: 1,
        marginBottom: 1
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }

});





module.exports = Benefits1;