'use strict';

var React = require('react-native');
var {
      ActivityIndicatorIOS,
      ListView,
      Platform,
      ProgressBarAndroid,
      StyleSheet,
      Text,
      View,
      Dimensions,
    } = React;

var window = Dimensions.get('window');

var TimerMixin = require('react-timer-mixin');


var invariant = require('invariant');
var dismissKeyboard = require('dismissKeyboard');


var resultsCache = {
  dataForQuery: {},
  nextPageNumberForQuery: {},
  totalForQuery: {}
};

var LOADING = {};
var API_URL = 'http://stage-rxcout.pilgrimconsulting.com/backend/web/api/drugs/';
var API_KEYS = [
  '7MNqWkQAss4Pn8Dw9WTYaYNbnVREEFj5QJV27SmALUwGENhvr9',
];


var SearchScreen = React.createClass({
  mixins: [TimerMixin],

  timeoutID: (null: any),

getInitialState() {
  return {
    isLoading: false,
    isLoadingTail: false,
    dataSource: new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    }),
    filter: '',
    queryNumber: 0,
    www: 0,
    showMore: 0,
    showMore2: 0,
    test: 30,
    searchBarWidth: 0, 
  };
},


componentDidMount: function() {
  this.searchDrugs('');
},

_urlForQueryAndPage: function(query: string, pageNumber: number): string {
  var apiKey = API_KEYS[this.state.queryNumber % API_KEYS.length];

  return (
      API_URL + '?query=' + encodeURIComponent(query) + '&page_limit=20&page='+ pageNumber + '&apikey=' + apiKey
  );

},

searchDrugs: function(query: string) {

  this.timeoutID = null;

  this.setState({filter: query});

  var cachedResultsForQuery = resultsCache.dataForQuery[query];
  if (cachedResultsForQuery) {
    if (!LOADING[query]) {
      this.setState({
        dataSource: this.getDataSource(cachedResultsForQuery),
        isLoading: false
      });
    } else {
      this.setState({isLoading: true});
    }
    return;
  }

  LOADING[query] = true;
  resultsCache.dataForQuery[query] = null;
  this.setState({
    isLoading: true,
    queryNumber: this.state.queryNumber + 1,
    isLoadingTail: false,
  });

  fetch(this._urlForQueryAndPage(query, 0))
      .then((response) => response.json())
      .catch((error) => {
        LOADING[query] = false;
        resultsCache.dataForQuery[query] = undefined;
        this.setState({
          dataSource: this.getDataSource([]),
          isLoading: false,
        });
      })
      .then((responseData) => {
        LOADING[query] = false;
        resultsCache.totalForQuery[query] = responseData.count;
        resultsCache.dataForQuery[query] = responseData.result;
        resultsCache.nextPageNumberForQuery[query] = 2;

        if (this.state.filter !== query) {
          // do not update state if the query is stale
          return;
        }

        this.setState({
          isLoading: false,
          dataSource: this.getDataSource(responseData.result),
        });
      })
      .done();
},

hasMore: function(): boolean {
  var query = this.state.filter;
  if (!resultsCache.dataForQuery[query]) {
    return true;
  }
  return (
      resultsCache.totalForQuery[query] !==
      resultsCache.dataForQuery[query].length
  );
},

onEndReached: function() {
  var query = this.state.filter;
  if (!this.hasMore() || this.state.isLoadingTail) {
    // We're already fetching or have all the elements so noop
    return;
  }

  if (LOADING[query]) {
    return;
  }

  LOADING[query] = true;
  this.setState({
    queryNumber: this.state.queryNumber + 1,
    isLoadingTail: false,
  });

  this.setState({
    test: resultsCache.dataForQuery[query].slice().length
  });

  var page = resultsCache.nextPageNumberForQuery[query];
  invariant(page != null, 'Next page number for "%s" is missing', query);


  LOADING[query] = false;

  if (this.state.filter !== query) {
    // do not update state if the query is stale
    return;
  }

},

getDataSource: function(drugs: Array<any>): ListView.DataSource {
  return this.state.dataSource.cloneWithRows(drugs);
},


measureMainComponent: function() {
  this.refs.MainComponent.measure((ox, oy, width) => {
    this.setState({
      www: width,
      searchBarWidth: width-6,
    });
  });
},

render: function() {

    return (

        <View onLayout={this.measureMainComponent.bind(null,this)} ref="MainComponent" style={[styles.tabContent]}>
          <ScrollView>
            <View style={{marginTop:80}}>

              <Text style={styles.tabText}>If you're in a hurry and need a discount coupon for your medication right away, go straight to Search</Text>


              <Text style={styles.tabText}>Otherwise, let's quickly get My Rx set up with your prescriptions, so RxPreferred can provide you with the lowest prices for all your prescriptions.</Text>

              <Text style={styles.tabText}>Your prescription data is private and can be deleted from this app at any time in Account</Text>
            </View>
          </ScrollView>

        </View>


    );
  }
});



var styles = StyleSheet.create({
  centerText: {
    alignItems: 'center',
  },
  noMoviesText: {
    marginTop: 80,
    color: '#888888',
  },
  separator: {
    height: 1,
    backgroundColor: '#eeeeee',
  },
  scrollSpinner: {
    marginVertical: 20,
  },
  rowSeparator: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: 1,
    marginLeft: 4,
  },
  rowSeparatorHide: {
    opacity: 0.0,
  },
  tabContent: {
    flex: 1,
  },
  tabText: {
    color: 'black',
    margin: 30,
    marginBottom: 0,
    marginTop: 25,
    alignItems: 'center',
  },
});

module.exports = SearchScreen;