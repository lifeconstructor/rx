'use strict';


var AcountUpdatePhone = require('./account-phone-number');

var GLOBALS = require('./Globals');

var Benefits1 = React.createClass({

    getInitialState: function(){

        var URL = CONFIG.API_URL + CONFIG.user_update_get + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData){

                    if (responseData.member_id){
                        this.setState({
                            phoneNumber: responseData.member_id
                        });
                    } else {
                        this.setState({
                            phoneNumber: '(empty)'
                        });
                    }


                    if (responseData.pcn){
                        this.setState({
                            pcn: responseData.pcn
                        });
                    }  else {
                        this.setState({
                            pcn: '(empty)'
                        });
                    }

                    if (responseData.bin){
                        this.setState({
                            bin: responseData.bin
                        });
                    }  else {
                        this.setState({
                            bin: '(empty)'
                        });
                    }

                    if (responseData.group_number){
                        this.setState({
                            group_number: responseData.group_number
                        });
                    }  else {
                        this.setState({
                            group_number: '(empty)'
                        });
                    }
                }
            })
            .done();

        return({
            phoneNumber: '(empty)',
            pcn: '(empty)',
            bin: '(empty)',
            group_number: '(empty)'
        });
    },


    componentWillReceiveProps: function(){

        var URL = CONFIG.API_URL + CONFIG.user_update_get + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData){

                    if (responseData.member_id){
                        this.setState({
                            phoneNumber: responseData.member_id
                        });
                    } else {
                        this.setState({
                            phoneNumber: '(empty)'
                        });
                    }


                    if (responseData.pcn){
                        this.setState({
                            pcn: responseData.pcn
                        });
                    }  else {
                        this.setState({
                            pcn: '(empty)'
                        });
                    }

                    if (responseData.bin){
                        this.setState({
                            bin: responseData.bin
                        });
                    }  else {
                        this.setState({
                            bin: '(empty)'
                        });
                    }

                    if (responseData.group_number){
                        this.setState({
                            group_number: responseData.group_number
                        });
                    }  else {
                        this.setState({
                            group_number: '(empty)'
                        });
                    }
                }
            })
            .done();
    },

    gotoUpdatePhone: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('Update Phone'),
            component: AcountUpdatePhone
        });

    },

    render: function() {

        return (
            <View style={styles.container}>

                <Text style={{color: '#357bb5', fontSize: 32, textAlign: 'center', marginBottom: 3, marginTop: 5}}>
                    {CONFIG.PRODUCTION_MODE=='PBM'?'RxPreferred':'RxScout'} Benefits
                </Text>
                <Text style={{color: 'gray', fontSize: 14, textAlign: 'center', marginBottom: 5}}>
                    TRANSFORMING PHARMACY BENEFIT MANAGEMENT
                </Text>
                <View style={{backgroundColor: 'darkblue', alignSelf: 'stretch'}}>
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Pharmacy {this.props.route.textForCard}
                    </Text>
                </View>

                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    Group: {this.state.group_number}
                </Text>
                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    PCN: {this.state.pcn}
                </Text>
                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    BIN: {this.state.bin}
                </Text>
                <View style={{flexDirection: 'column'}}>
                    <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Member ID: {this.state.phoneNumber}
                    </Text>
                </View>

                <Text style={{color: 'black', fontSize: 14, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                    This is not insurance
                </Text>

            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        borderRadius: 20,
        borderColor: 'lightgray',
        borderWidth: 1,
        marginTop: 21,
        marginHorizontal: 1,
        marginBottom: 1
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }

});

module.exports = Benefits1;