#import "SBGoogleMap.h"

#import "RCTConvert.h"
#import "RCTEventDispatcher.h"
#import "RCTLog.h"
#import "RCTUtils.h"

@implementation SBGoogleMap {
  NSMutableDictionary *markerImages;
  CLLocationManager *locationManager;
  GMSPolyline *polyline1;
  float zoom;
  
}



/**
 * Init the google map view class.
 *
 * @return id
 */
- (id)init
{
  if (self = [super init]) {
    markerImages = [[NSMutableDictionary alloc] init];
  }
  
  return self;
}

/**
 * Enables layout sub-views which are required to render a non-blank map.
 *
 * @return void
 */
- (void)layoutSubviews
{
  [super layoutSubviews];
  
  CGRect mapFrame = self.frame;
  
  self.frame = CGRectZero;
  self.frame = mapFrame;
}

#pragma mark Accessors


/*- (void)viewDidLoad {
  
  NSURLSessionConfiguration *config =
  [NSURLSessionConfiguration defaultSessionConfiguration];
  config.URLCache = [[NSURLCache alloc] initWithMemoryCapacity:2 * 1024 * 1024
                                                  diskCapacity:10 * 1024 * 1024
                                                      diskPath:@"MarkerData"];
  self.markerSession = [NSURLSession sessionWithConfiguration:config];
}
*/

/**
 * Sets the map camera position.
 *
 * @return void
 */
- (void)setCameraPosition:(NSDictionary *)cameraPosition
{
  zoom = ((NSNumber*)cameraPosition[@"zoom"]).doubleValue;
  
  if (!cameraPosition[@"latitude"]) {
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
  } else {
    
    CLLocationDegrees latitude = ((NSNumber*)cameraPosition[@"latitude"]).doubleValue;
    CLLocationDegrees longitude = ((NSNumber*)cameraPosition[@"longitude"]).doubleValue;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:zoom];
    
    [self setCamera: camera];
  }
}

/**
 * The delegate for the did update location event - fired when the user's location becomes available and then centers the
 * map about the their location. The location manager is then stopped so that the map position doesn't continue to be updated.
 *
 * @return void
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
  
  
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                          longitude:newLocation.coordinate.longitude
                                                               zoom:zoom];
  
  
  
  [self setCamera: camera];
  
  // current user possition marker
  //GMSMarker *marker = [[GMSMarker alloc] init];
  //marker.position = camera.target;
  //marker.snippet = @"Your Current Location";
  //marker.appearAnimation = kGMSMarkerAnimationPop;
  //marker.icon = [UIImage imageNamed:@"current_location.png"];
  //marker.map = self;
  
  // temporary set 4 markers test
  
//  CLLocationDegrees latitude1 = (newLocation.coordinate.latitude + 0.005);
//  CLLocationDegrees longitude1 = (newLocation.coordinate.longitude + 0.005);
//  
//  GMSMarker* mapMarker1 = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude1, longitude1)];
//  
//  mapMarker1.title = @"Walmart";
//  mapMarker1.snippet = @"Pharmacy";
//  mapMarker1.map = self;
//  
//  CLLocationDegrees latitude2 = (newLocation.coordinate.latitude - 0.007);
//  CLLocationDegrees longitude2 = (newLocation.coordinate.longitude - 0.007);
//  
//  GMSMarker* mapMarker2 = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude2, longitude2)];
//  
//  mapMarker2.title = @"Walgreens Drug Store";
//  mapMarker2.snippet = @"Pharmacy";
//  mapMarker2.map = self;
//  
//  CLLocationDegrees latitude3 = (newLocation.coordinate.latitude + 0.006);
//  CLLocationDegrees longitude3 = (newLocation.coordinate.longitude - 0.007);
//  
//  GMSMarker* mapMarker3 = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude3, longitude3)];
//  
//  mapMarker3.title = @"Kroger";
//  mapMarker3.snippet = @"Pharmacy";
//  mapMarker3.map = self;
//  
//  CLLocationDegrees latitude4 = (newLocation.coordinate.latitude - 0.007);
//  CLLocationDegrees longitude4 = (newLocation.coordinate.longitude + 0.007);
//  
//  GMSMarker* mapMarker4 = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude4, longitude4)];
//  
//  mapMarker4.title = @"Rite Aid";
//  mapMarker4.snippet = @"Pharmacy";
//  mapMarker4.map = self;
  
  [locationManager stopUpdatingLocation];
}

/**
 * Adds marker icons to the map.
 *
 * @return void
 */

- (void)setMarkers:(NSArray *)markers
{
  
  
  for (NSDictionary* marker in markers) {
    NSString *publicId = marker[@"name"];
    CLLocationDegrees latitude = ((NSNumber*)marker[@"geometry"][@"location"][@"lat"]).doubleValue;
    CLLocationDegrees longitude = ((NSNumber*)marker[@"geometry"][@"location"][@"lng"]).doubleValue;
    
        
    GMSMarker* mapMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude, longitude)];
    
    //mapMarker.icon = [self getMarkerImage:marker];
    mapMarker.userData = publicId;
    mapMarker.title = marker[@"name"];
    mapMarker.snippet = @"Pharmacy";
    
    
    for (NSDictionary* line in self.polyline) {
    
    GMSPath *encodedPath = [GMSPath pathFromEncodedPath:(NSString*)line[@"line"]];
    self.polyline2 = [GMSPolyline polylineWithPath:encodedPath];
    self.polyline2.strokeWidth = 7;
    self.polyline2.strokeColor = [UIColor greenColor];
    self.polyline2.map = self;
    
    }
    
    mapMarker.map = self;
    
    
    
  }
}

/**
 * Load the marker image or use one that's already been loaded.
 *
 * @return NSImage
 */
- (UIImage *)getMarkerImage:(NSDictionary *)marker
{
  NSString *markerPath = marker[@"icon"][@"uri"];
  CGFloat markerScale = ((NSNumber*)marker[@"icon"][@"scale"]).doubleValue;
  
  if (!markerImages[markerPath]) {
    UIImage *markerImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:markerPath]]];
    
    UIImage *markerScaled = [UIImage imageWithCGImage:[markerImage CGImage]
                                                scale:(markerScale)
                                          orientation:(markerImage.imageOrientation)];
    
    [markerImages setObject:markerScaled forKey:markerPath];
  }
  
  
  return markerImages[markerPath];
}

/**
 * Sets the user's location marker, if it has been enabled. Don't be alarmed if the marker looks funny when testing the app in
 * the simulator, there's a known bug: https://code.google.com/p/gmaps-api-issues/issues/detail?id=5472
 *
 * @return void
 */
- (void)setShowsUserLocation:(BOOL *)showsUserLocation
{
  if (showsUserLocation) {
    self.myLocationEnabled = YES;
  } else {
    self.myLocationEnabled = NO;
  }
}

/**
 * Controls whether scroll gestures are enabled (default) or disabled.
 *
 * @return void
 */
- (void)setScrollGestures:(BOOL *)scrollGestures
{
  if (scrollGestures) {
    self.settings.scrollGestures = YES;
  } else {
    self.settings.scrollGestures = NO;
  }
}

/**
 * Controls whether zoom gestures are enabled (default) or disabled.
 *
 * @return void
 */
- (void)setZoomGestures:(BOOL *)zoomGestures
{
  if (zoomGestures) {
    self.settings.zoomGestures = YES;
  } else {
    self.settings.zoomGestures = NO;
  }
}

/**
 * Controls whether tilt gestures are enabled (default) or disabled.
 *
 * @return void
 */
- (void)setTiltGestures:(BOOL *)tiltGestures
{
  if (tiltGestures) {
    self.settings.tiltGestures = YES;
  } else {
    self.settings.tiltGestures = NO;
  }
}

/**
 * Controls whether rotate gestures are enabled (default) or disabled.
 *
 * @return void
 */
- (void)setRotateGestures:(BOOL *)rotateGestures
{
  if (rotateGestures) {
    self.settings.rotateGestures = YES;
  } else {
    self.settings.rotateGestures = NO;
  }
}

/**
 * Controls whether gestures by users are completely consumed by the GMSMapView when gestures are enabled (default YES).
 *
 * @return void
 */
- (void)setConsumesGesturesInView:(BOOL *)consumesGesturesInView
{
  if (consumesGesturesInView) {
    self.settings.consumesGesturesInView = YES;
  } else {
    self.settings.consumesGesturesInView = NO;
  }
}

/**
 * Enables or disables the compass.
 *
 * @return void
 */
- (void)setCompassButton:(BOOL *)compassButton
{
  if (compassButton) {
    self.settings.compassButton = YES;
  } else {
    self.settings.compassButton = NO;
  }
}

/**
 * Enables or disables the My Location button.
 *
 * @return void
 */
- (void)setMyLocationButton:(BOOL *)myLocationButton
{
  if (myLocationButton) {
    self.settings.myLocationButton = YES;
  } else {
    self.settings.myLocationButton = NO;
  }
}

/**
 * Enables (default) or disables the indoor floor picker.
 *
 * @return void
 */
- (void)setIndoorPicker:(BOOL *)indoorPicker
{
  if (indoorPicker) {
    self.settings.indoorPicker = YES;
  } else {
    self.settings.indoorPicker = NO;
  }
}

/**
 * Controls whether rotate and zoom gestures can be performed off-center and scrolled around (default YES).
 *
 * @return void
 */
- (void)setAllowScrollGesturesDuringRotateOrZoom:(BOOL *)allowScrollGesturesDuringRotateOrZoom
{
  if (allowScrollGesturesDuringRotateOrZoom) {
    self.settings.allowScrollGesturesDuringRotateOrZoom = YES;
  } else {
    self.settings.allowScrollGesturesDuringRotateOrZoom = NO;
  }
}

// --------------------------------------

- (void)drawRoute
{
  /*[self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
   {
     if(polyline)
       polyline.map = self.myMap;
   }];*/
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
  NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
  NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
  NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
  NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
  NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
  
  
  NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                               ^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                 
                                                 
                                                 NSArray *routesArray = [json objectForKey:@"routes"];
                                                 
                                                 GMSPolyline *polyline = nil;
                                                 if ([routesArray count] > 0)
                                                 {
                                                   NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                   NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                   NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                   GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                   polyline = [GMSPolyline polylineWithPath:path];
                                                 }
                                                 
                                                 if(completionHandler)
                                                   completionHandler(polyline);
                                               }];
  [fetchDirectionsTask resume];
}

// --------------------------------------------

@end