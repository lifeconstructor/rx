#import "RCTViewManager.h"
#import <GoogleMaps/GoogleMaps.h>

@interface SBGoogleMapManager : RCTViewManager<GMSMapViewDelegate>

@end
