'use strict';

var GLOBALS = require('./Globals');

var AcountUpdateInfo = require('./account-update-info');
var AcountUpdatePassword = require('./account-update-pass');
var AcountHelpFaq = require('./account-help-faq');
var AcountAboutRx = require('./account-about-rx');

var AcountUpdatePhone = require('./account-phone-number');

var Autocomplete = require('./auto').default;


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

var AccountPage = React.createClass({

    getInitialState: function() {


        var URL = CONFIG.API_URL + CONFIG.user_update_get + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData){

                    if (responseData.member_id){
                        this.setState({
                            phoneNumber: responseData.member_id
                        });
                    } else {
                        this.setState({
                            phoneNumber: '(empty)'
                        });
                    }


                    if (responseData.pcn){
                        this.setState({
                            pcn: responseData.pcn
                        });
                    }  else {
                        this.setState({
                            pcn: '(empty)'
                        });
                    }

                    if (responseData.bin){
                        this.setState({
                            bin: responseData.bin
                        });
                    }  else {
                        this.setState({
                            bin: '(empty)'
                        });
                    }

                    if (responseData.group_number){
                        this.setState({
                            group_number: responseData.group_number
                        });
                    }  else {
                        this.setState({
                            group_number: '(empty)'
                        });
                    }


                }
            })
            .done();

        var URL = CONFIG.API_URL + 'api/formulary-search?page_limit=20&page_number=0&query=a';

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result){
                    this.setState({
                        searchResults: this.getDataSource(responseData.result)
                    });
                }
            })
            .done();



        var URL1 = CONFIG.API_URL + 'api/get-plan-info?access-token='+ GLOBALS.API_KEY;

        fetch(URL1)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result){
                    this.setState({
                        planInfo: responseData.result.formulary
                    });
                }

            })
            .done();


        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


        return({
            searchResults: ds.cloneWithRows([]),
            activePage: 4,
            searchPhrase: '',
            drugName: '',
            planInfo: '',
            drugsAutocomplete: [],
            selectedDrug: '',
            films: [{title: 'test'}],
            query: '',
            phoneNumber: '(empty)',
            pcn: '(empty)',
            bin: '(empty)',
            group_number: '(empty)'
        });
    },

    clearAutocom: function(name){
        this.setState({
            drugsAutocomplete: [],
            query: name
        })
    },

    getDataSource: function(drugs: Array<any>): ListView.DataSource {
        return this.state.searchResults.cloneWithRows(drugs);
    },

    showPlanInfo: function(){
        if (this.state.activePage === 1){
            this.setState({
                activePage: 4
            });
        } else {
            this.setState({
                activePage: 1
            });
        }
    },

    showFormSearch: function(){
        if (this.state.activePage === 2){
            this.setState({
                activePage: 4
            });
        } else {
            this.setState({
                activePage: 2
            });
        }
    },

    showDiscountCard: function(){
        if (this.state.activePage === 3){
            this.setState({
                activePage: 4
            });
        } else {
            this.setState({
                activePage: 3
            });
        }
    },

    formSearch: function(){


        var URL = CONFIG.API_URL + 'api/formulary-search?page_limit=20&page_number=0&query='+this.state.searchPhrase;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    searchResults: this.getDataSource(responseData.result)
                });

            })
            .done();

    },

    gotoUpdateInfo: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('Update Info'),
            component: AcountUpdateInfo
        });

    },

    gotoUpdatePassword: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('Update Password'),
            component: AcountUpdatePassword
        });

    },

    gotoHelpFaq: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('Help & FAQ'),
            component: AcountHelpFaq
        });

    },

    gotoUpdatePhone: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('Update Phone'),
            component: AcountUpdatePhone
        });

    },

    gotoAboutRx: function(){

        this.props.navigator.push({
            title: GLOBALS.titleCut('About RxPreferred'),
            component: AcountAboutRx
        });

    },

    appLogout: function(){
        this.props.topNavigator.popToTop();
    },


    onTyping: function (text) {

        this.setState({drugName: text});

        var URL = CONFIG.API_URL + 'api/formulary-search-autocomplete?query=' + this.state.drugName;

        fetch(URL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        }).then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                var drugs = responseData;

                this.setState({
                    drugsAutocomplete: drugs,
                    data: responseData
                });

                return drugs;
            })
            .done();
    },

    componentWillReceiveProps: function(){

        var URL = CONFIG.API_URL + CONFIG.user_update_get + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData){

                    if (responseData.member_id){
                        this.setState({
                            phoneNumber: responseData.member_id
                        });
                    } else {
                        this.setState({
                            phoneNumber: '(empty)'
                        });
                    }


                    if (responseData.pcn){
                        this.setState({
                            pcn: responseData.pcn
                        });
                    }  else {
                        this.setState({
                            pcn: '(empty)'
                        });
                    }

                    if (responseData.bin){
                        this.setState({
                            bin: responseData.bin
                        });
                    }  else {
                        this.setState({
                            bin: '(empty)'
                        });
                    }

                    if (responseData.group_number){
                        this.setState({
                            group_number: responseData.group_number
                        });
                    }  else {
                        this.setState({
                            group_number: '(empty)'
                        });
                    }


                }
            })
            .done();

    },

    selectDrug: function (drugTitle) {

        return false;

    },


    _findFilm(query) {
        if (query === '') {
            return [];
        }

        var  films = this.state.drugsAutocomplete;
        var regex = new RegExp(`${query.trim()}`, 'i');
        return films.filter(film => film.name.search(regex) >= 0);
    },


    render: function() {

        let tabsHeader = '';

        let content = '';
        let items = '';


        var  query = this.state.query;
        var  films = this._findFilm(query);

        if (this.state.planInfo){
            tabsHeader =
                <View style={[{borderRadius:20, paddingBottom: 15, marginTop: 23}]}>
                    <View style={styles.container}>
                        <TouchableHighlight
                            onPress={this.showPlanInfo}
                            style={this.state.activePage == 1? styles.loginButtonActive:styles.loginButton}

                        >
                            <View style={{alignItems: 'center', paddingHorizontal:10}}>
                                <Image
                                    style={{height:20, width: 20, margin: 8, marginBottom: 5}}
                                    source={require('./img/plan-info.png')}
                                />
                                <Text style={styles.loginText}>
                                    Plan Info
                                </Text>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.showFormSearch}
                            style={this.state.activePage == 2? styles.loginButtonActive:styles.loginButton}
                        >
                            <View style={{alignItems: 'center'}}>
                                <Image
                                    style={{height:20, width: 20, margin: 6}}
                                    source={require('./img/formulary-search.png')}
                                />
                                <Text style={styles.loginText}>
                                    Formulary Search
                                </Text>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.showDiscountCard}
                            style={this.state.activePage == 3? styles.loginButtonActive:styles.loginButton}

                        >
                            <View style={{alignItems: 'center'}}>
                                <Image
                                    style={{height:15, width: 23, margin: 10, marginBottom: 6}}
                                    source={require('./img/discount-card.png')}
                                />
                                <Text style={styles.loginText}>
                                    Member Card
                                </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
        } else {
            tabsHeader = <View style={{width:0, height:0}}></View>

        }


        switch(this.state.activePage) {
            case 1:
                if (typeof this.state.planInfo != 'string') {


                    var innerText = this.state.planInfo.map(function(text){

                        var paragr = {};

                        switch (text.type) {
                            case 'h3':
                                paragr = <Text style={{fontSize: 18, fontWeight: 'bold', marginVertical: 10}}>{text.text}</Text>;
                                break;
                            case 'b':
                                paragr = <Text style={{fontSize: 14, fontWeight: 'bold', marginVertical: 5}}>{text.text}</Text>;
                                break;
                            case 'span':

                                if (text.deep == 1){
                                    paragr = <Text style={{fontSize: 14}}>{text.text}</Text>;
                                } else {
                                    paragr = <Text style={{fontSize: 14, marginLeft: 43}}>{text.text}</Text>;
                                }

                                break;
                        }

                       return paragr;
                    });




                    content =
                    <View>
                        <View
                            style={{borderWidth:1, borderColor: 'black', margin: 10, paddingHorizontal:10, paddingVertical:25}}>

                            {innerText}


                        </View>

                        <View style={[{backgroundColor: 'lightgray', height: 15, marginTop: 5}]}></View>

                        <View style={{margin: 10, paddingHorizontal:10, paddingVertical:25}}>

                            <TouchableHighlight
                                onPress={this.gotoUpdateInfo}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/update-personal-info.png')}
                                        />
                                    </View>

                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Update Personal Information</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            {/*<TouchableHighlight
                                onPress={this.gotoUpdatePassword}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <Image
                                        style={{height:25, width: 25, margin: 8, flex: 0.1}}
                                        source={require('./img/update-password.png')}
                                    />
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97}}>Update Password</Text>
                                            <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>*/}

                            <TouchableHighlight
                                onPress={this.gotoHelpFaq}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/help-faq.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Help & FAQ</Text>
                                            <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={this.gotoAboutRx}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/about.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>About RxPreferred</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={this.appLogout}
                                style={{
                                    flex: 0.3,
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    borderColor: 'black',
                                    backgroundColor: '#d83b34',
                                    alignItems: 'center',
                                    marginTop: 30,
                                    marginHorizontal: 10
                                    }}
                            >
                                <Text style={{color: 'white', margin: 15}}>Log Out</Text>
                            </TouchableHighlight>

                        </View>

                    </View>;
                } else {



                    content =
                        <View
                            style={{borderWidth:1, borderColor: 'black', margin: 10, paddingHorizontal:10, paddingVertical:25}}>


                            <Text style={{fontWeight: 'bold', marginBottom: 10, alignSelf: 'center'}}>
                                {this.state.planInfo}
                            </Text>

                        </View>;
                }
                break;
            case 2:
                content =
                    <View>

                        <Text style={{color: '#357bb5', margin: 10, marginBottom: 60, textAlign: 'center', fontSize: 10}}>Please enter your prescription medication below:</Text>



                        <View style={[{backgroundColor: 'lightgray', height: 15, marginTop: 5}]}></View>
                        <View style={{margin: 10, paddingHorizontal:10, paddingVertical:25}}>

                            <TouchableHighlight
                                onPress={this.gotoUpdateInfo}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/update-personal-info.png')}
                                        />
                                    </View>

                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Update Personal Information</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            {/*<TouchableHighlight
                             onPress={this.gotoUpdatePassword}
                             style={{}}
                             >
                             <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                             <Image
                             style={{height:25, width: 25, margin: 8, flex: 0.1}}
                             source={require('./img/update-password.png')}
                             />
                             <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                             <Text style={{marginTop: 12, flex: 0.97}}>Update Password</Text>
                             <Image
                             style={{height:10, width: 10, margin: 13, flex: 0.03}}
                             source={require('./img/arrow-right.png')}
                             />
                             </View>
                             </View>
                             </TouchableHighlight>*/}

                            <TouchableHighlight
                                onPress={this.gotoHelpFaq}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/help-faq.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Help & FAQ</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={this.gotoAboutRx}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/about.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>About RxPreferred</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>


                            <TouchableHighlight
                                onPress={this.appLogout}
                                style={{
                                            flex: 0.3,
                                            borderWidth: 1,
                                            borderRadius: 5,
                                            borderColor: 'black',
                                            backgroundColor: '#d83b34',
                                            alignItems: 'center',
                                            marginTop: 30,
                                            marginHorizontal: 10}}
                            >
                                <Text style={{color: 'white', margin: 15}}>Log Out</Text>
                            </TouchableHighlight>

                        </View>

                        <View style={{height: 500, position: 'absolute', top: 10}}>
                            <Autocomplete
                                autoCapitalize="none"
                                autoCorrect={false}
                                containerStyle={styles.autocompleteContainer}
                                data={this.state.drugsAutocomplete}
                                defaultValue={query}
                                onChangeText={text => this.onTyping(text)}
                                placeholder="Enter Drug Title"
                                clearAutocom = {this.clearAutocom}
                                renderItem={({name, release_date}) => (
                                    <TouchableOpacity onPress={()=>

                                        this.clearAutocom(name)

                                    }
                                    style={{backgroundColor: 'white', paddingLeft: 7}}
                                    >
                                      <Text style={styles.itemText}>
                                        {name}
                                      </Text>
                                    </TouchableOpacity>
                                  )}
                            />


                        </View>

                    </View>;
                break;
            case 3:
                content =
                    <View>
                    <View style={{alignItems: 'center', margin: 3, borderWidth: 1, borderColor: 'lightgray', borderRadius: 15, backgroundColor: 'white'}}>

                            <Text style={{color: '#357bb5', fontSize: 32, textAlign: 'center', marginBottom: 3, marginTop: 5}}>
                                {CONFIG.OWNER_NAME} Benefits
                            </Text>
                            <Text style={{color: 'gray', fontSize: 14, textAlign: 'center', marginBottom: 5}}>
                                TRANSFORMING PHARMACY BENEFIT MANAGEMENT
                            </Text>
                            <View style={{backgroundColor: 'darkblue', alignSelf: 'stretch'}}>
                                <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                    Pharmacy Member Card
                                </Text>
                            </View>

                            <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                Group: {this.state.group_number}
                            </Text>
                            <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                PCN: {this.state.pcn}
                            </Text>
                            <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                BIN: {this.state.bin}
                            </Text>
                            <View style={{flexDirection: 'column'}}>
                                <Text style={{color: '#357bb5', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                    Member ID: {this.state.phoneNumber}
                                </Text>
                                {/*<TouchableHighlight
                                    onPress={this.gotoUpdatePhone}
                                    style={{alignSelf:'center'}}
                                >
                                    <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                        <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray'}}>
                                            <Text style={{marginTop: 12, textAlign: 'center'}}>Edit Phone Number</Text>
                                        </View>
                                    </View>

                                </TouchableHighlight>*/}
                            </View>
                            <Text style={{color: 'black', fontSize: 14, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                                This is not insurance
                            </Text>
                    </View>
                        <View>
                            <View style={[{backgroundColor: 'lightgray', height: 15, marginTop: 5}]}></View>
                            <View style={{margin: 10, paddingHorizontal:10, paddingVertical:25}}>

                                <TouchableHighlight
                                    onPress={this.gotoUpdateInfo}
                                    style={{}}
                                >
                                    <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                        <View  style={{flex: 0.1, marginRight: 10}}>
                                            <Image
                                                style={{height:25, width: 25, margin: 8}}
                                                source={require('./img/update-personal-info.png')}
                                            />
                                        </View>

                                        <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                            <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Update Personal Information</Text>
                                            <Image
                                                style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                                source={require('./img/arrow-right.png')}
                                            />
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                {/*<TouchableHighlight
                                 onPress={this.gotoUpdatePassword}
                                 style={{}}
                                 >
                                 <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                 <Image
                                 style={{height:25, width: 25, margin: 8, flex: 0.1}}
                                 source={require('./img/update-password.png')}
                                 />
                                 <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                 <Text style={{marginTop: 12, flex: 0.97}}>Update Password</Text>
                                 <Image
                                 style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                 source={require('./img/arrow-right.png')}
                                 />
                                 </View>
                                 </View>
                                 </TouchableHighlight>*/}

                                <TouchableHighlight
                                    onPress={this.gotoHelpFaq}
                                    style={{}}
                                >
                                    <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                        <View  style={{flex: 0.1, marginRight: 10}}>
                                            <Image
                                                style={{height:25, width: 25, margin: 8}}
                                                source={require('./img/help-faq.png')}
                                            />
                                        </View>
                                        <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                            <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Help & FAQ</Text>
                                            <Image
                                                style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                                source={require('./img/arrow-right.png')}
                                            />
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    onPress={this.gotoAboutRx}
                                    style={{}}
                                >
                                    <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                        <View  style={{flex: 0.1, marginRight: 10}}>
                                            <Image
                                                style={{height:25, width: 25, margin: 8}}
                                                source={require('./img/about.png')}
                                            />
                                        </View>
                                        <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                            <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>About RxPreferred</Text>
                                            <Image
                                                style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                                source={require('./img/arrow-right.png')}
                                            />
                                        </View>
                                    </View>
                                </TouchableHighlight>


                                <TouchableHighlight
                                    onPress={this.appLogout}
                                    style={{
                                        flex: 0.3,
                                        borderWidth: 1,
                                        borderRadius: 5,
                                        borderColor: 'black',
                                        backgroundColor: '#d83b34',
                                        alignItems: 'center',
                                        marginTop: 30,
                                        marginHorizontal: 10}}
                                >
                                    <Text style={{color: 'white', margin: 15}}>Log Out</Text>
                                </TouchableHighlight>

                            </View>
                        </View>

                    </View>;
                break;
            default:
                content =
                    <View>
                        <View style={[{backgroundColor: 'lightgray', height: 15, marginTop: 5}]}></View>
                        <View style={{margin: 10, paddingHorizontal:10, paddingVertical:25}}>

                            <TouchableHighlight
                                onPress={this.gotoUpdateInfo}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/update-personal-info.png')}
                                        />
                                    </View>

                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Update Personal Information</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            {/*<TouchableHighlight
                             onPress={this.gotoUpdatePassword}
                             style={{}}
                             >
                             <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                             <Image
                             style={{height:25, width: 25, margin: 8, flex: 0.1}}
                             source={require('./img/update-password.png')}
                             />
                             <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                             <Text style={{marginTop: 12, flex: 0.97}}>Update Password</Text>
                             <Image
                             style={{height:10, width: 10, margin: 13, flex: 0.03}}
                             source={require('./img/arrow-right.png')}
                             />
                             </View>
                             </View>
                             </TouchableHighlight>*/}

                            <TouchableHighlight
                                onPress={this.gotoHelpFaq}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/help-faq.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>Help & FAQ</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={this.gotoAboutRx}
                                style={{}}
                            >
                                <View style={{flexDirection: 'row', paddingHorizontal:10}}>
                                    <View  style={{flex: 0.1, marginRight: 10}}>
                                        <Image
                                            style={{height:25, width: 25, margin: 8}}
                                            source={require('./img/about.png')}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', borderRadius: 5, borderBottomWidth: 1, borderBottomColor: 'lightgray', flex: 0.9}}>
                                        <Text style={{marginTop: 12, flex: 0.97, marginLeft: 10}}>About RxPreferred</Text>
                                        <Image
                                            style={{height:10, width: 10, margin: 13, flex: 0.03}}
                                            source={require('./img/arrow-right.png')}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>


                            <TouchableHighlight
                                onPress={this.appLogout}
                                style={{
                                        flex: 0.3,
                                        borderWidth: 1,
                                        borderRadius: 5,
                                        borderColor: 'black',
                                        backgroundColor: '#d83b34',
                                        alignItems: 'center',
                                        marginTop: 30,
                                        marginHorizontal: 10}}
                            >

                                <Text style={{color: 'white', margin: 15}}>Log Out</Text>

                            </TouchableHighlight>

                        </View>
                    </View>;
                break;
        }

        return (


            <ScrollView keyboardShouldPersistTaps={this.state.activePage == 2}>

                {tabsHeader}




                <View>

                    {content}
                </View>

            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 5,
        marginBottom: 15,
        backgroundColor: 'white',
        fontSize: 20
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: GLOBALS.backgroundCl
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 30,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        margin: 5
    },
    loginButtonActive: {
        backgroundColor: '#28628f',
        borderRadius: 10,
        margin: 5
    },
    loginText :{
        marginTop: 5,
        marginBottom: 5,
        fontSize: 10,
        marginLeft: 10,
        marginRight: 10,
        color: 'white'
    },
    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 20,
        width: width-60,
        marginHorizontal: 30
    },
    itemText: {
        fontSize: 15,
        margin: 2
    }
});


module.exports = AccountPage;