'use strict';
var GLOBALS = require('./Globals');

var AccountPage = React.createClass({

    getInitialState: function() {

        var URL = CONFIG.API_URL + CONFIG.info + 'help&access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result.meta_value) {
                    var a = JSON.parse(responseData.result.meta_value);
                }

                if (a) {
                    if (a.title) {
                        this.setState({
                            titleHelp: a.title
                        });
                    }
                    if (a.text) {
                        this.setState({
                            textHelp: a.text
                        });
                    }
                }
            })
            .done();

        var URL = CONFIG.API_URL + CONFIG.info + 'faq&access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result.meta_value) {
                    var a = JSON.parse(responseData.result.meta_value);
                }

                if (a) {
                    if (a.title) {
                        this.setState({
                            titleFaq: a.title
                        });
                    }
                    if (a.text) {
                        this.setState({
                            textFaq: a.text
                        });
                    }
                    if (a.items) {
                        this.setState({
                            questions: this.getDataSource1(a.items)
                        });
                    }
                }
            })
            .done();

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return ({
            titleHelp: '',
            textHelp: '',
            titleFaq: '',
            textFaq: '',
            questions: ds.cloneWithRows([]),
        });
    },

    getDataSource1: function (test:Array<any>):ListView.DataSource {
        return this.state.questions.cloneWithRows(test);
    },

    showHelp: function(){
        if (this.state.activePage !== 1){
            this.setState({
                activePage: 1
            });
        }
    },

    showFaq: function(){
        if (this.state.activePage !== 2){
            this.setState({
                activePage: 2
            });
        }
    },




    render: function() {

        let tabsHeader = '';

        let content = '';


        tabsHeader =
            <View style={[{borderRadius:20, paddingBottom: 15, marginTop: 23}]}>
                <View style={[styles.container, {backgroundColor: GLOBALS.backgroundCl}]}>
                    <TouchableHighlight
                        onPress={this.showHelp}
                        style={this.state.activePage == 1? styles.loginButtonActive:styles.loginButton}

                    >
                        <View style={{alignItems: 'center', paddingHorizontal:10}}>
                            <Image
                                style={{height:20, width: 20, margin: 8, marginBottom: 5}}
                                source={require('./img/plan-info.png')}
                            />
                            <Text style={styles.loginText}>
                                Help
                            </Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={this.showFaq}
                        style={this.state.activePage == 2? styles.loginButtonActive:styles.loginButton}
                    >
                        <View style={{alignItems: 'center'}}>
                            <Image
                                style={{height:20, width: 20, margin: 8, marginBottom: 5}}
                                source={require('./img/formulary-search.png')}
                            />
                            <Text style={styles.loginText}>
                                FAQ
                            </Text>
                        </View>
                    </TouchableHighlight>

                </View>
            </View>;



        switch(this.state.activePage) {
            case 2:
                content =
                    <View style={styles.blackBorderView}>
                        <Text style={{fontWeight: 'bold', margin: 10}}>
                            {this.state.titleFaq}
                        </Text>
                        <Text style={{margin: 10}}>
                            {this.state.textFaq}
                        </Text>
                        <ListView
                            dataSource={this.state.questions}
                            renderRow={(rowData) =>

                                    <View style={{borderWidth: 1, borderColor: 'lightgray', backgroundColor: 'white', margin: 2, borderRadius:5, backgroundColor: GLOBALS.backgroundCl}}>
                                        <Text style={{margin: 8, fontSize: 16}}>{rowData.question}</Text>
                                        <Text style={{margin: 8, fontSize: 16}}>{rowData.answer}</Text>
                                    </View>

                                }
                        />
                    </View>;
                break;

            default:
                content =
                    <View style={styles.blackBorderView}>

                        <Text style={{fontWeight: 'bold', margin: 10}}>
                            {this.state.titleHelp}
                        </Text>
                        <Text style={{margin: 10}}>
                            {this.state.textHelp}
                        </Text>

                    </View>;
                break;
        }

        return (
            <ScrollView>
                {tabsHeader}
                <View>
                    {content}
                </View>
            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flexDirection: 'row'
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        margin: 5
    },
    loginButtonActive: {
        backgroundColor: '#28628f',
        borderRadius: 10,
        margin: 5
    },
    loginText :{
        marginTop: 5,
        marginBottom: 5,
        fontSize: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    },
    blackBorderView: {
        borderWidth:1,
        borderColor: 'black',
        margin: 10,
        paddingHorizontal:10,
        paddingVertical:25,
        alignItems: 'center'
    }
});


module.exports = AccountPage;