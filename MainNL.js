'use strict';

var React = require('react-native');

var {
        View,
        Text,
        Navigator,
        Dimensions,
        Platform,
    } = React;

import TabNavigator from 'react-native-tab-navigator';
var window = Dimensions.get('window');
var invariant = require('invariant');

var GLOBALS  = require('./Globals');

var RxPage = require('./login-screen');
var RemPage = require('./sign-up');


var SePage1 = require('./search-page-1NL');
var SePage2 = require('./search-page-2NL');
var SePage3 = require('./search-page-3NL');

var MemberCard = require('./discount-card');

var AcPage = require('./account-page');

var Menu = require('react-native-menu');
var { MenuContext, MenuOptions, MenuOption, MenuTrigger } = Menu;


class MainTabs extends React.Component {


    constructor(props){

        super(props);

        this.renderScene = this.renderScene.bind(this);

        this.renderSceneSearch = this.renderSceneSearch.bind(this);

        this.state = {
            selectedTab: this.props.selectedTab,
            presses: 0,
        };
    }

    static loginRouteMapper = (props) => ({

        LeftButton(route, navigator, index, navState) {

            if (index === 0) {
                return (
                    <TouchableOpacity
                        onPress={function(){
                                props.topNavigator.popToTop();
                        }}
                        style={styles.navBarLeftButton}>
                        <Text style={[styles.navBarText, styles.navBarButtonText]}>
                            Landing Page
                        </Text>
                    </TouchableOpacity>
                );
            }
            var previousRoute = navState.routeStack[index - 1];
            return (
                <TouchableOpacity
                    onPress={() => navigator.pop()}
                    style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
                </TouchableOpacity>
            );
        },


        RightButton: function(route, navigator, index, navState) {
            return (
                <TouchableOpacity
                    onPress={() => navigator.push(newRandomRoute())}
                    style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
                </TouchableOpacity>
            );
        },

        Title(route, navigator, index, navState) {
            return (
                <Text style={[styles.navBarText, styles.navBarTitleText]}>
                    {route.title}
                </Text>
            );
        }
    });
    static signUpRouteMapper = (props) => ({

        LeftButton(route, navigator, index, navState) {

            if (index === 0) {
                return (
                    <TouchableOpacity
                        onPress={function(){
                                props.topNavigator.popToTop();
                        }}
                        style={styles.navBarLeftButton}>
                        <Text style={[styles.navBarText, styles.navBarButtonText]}>
                            Landing Page
                        </Text>
                    </TouchableOpacity>
                );
            }
            var previousRoute = navState.routeStack[index - 1];
            return (
                <TouchableOpacity
                    onPress={() => navigator.pop()}
                    style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
                </TouchableOpacity>
            );
        },


        RightButton: function(route, navigator, index, navState) {
            return (
                <TouchableOpacity
                    onPress={() => navigator.push(newRandomRoute())}
                    style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
                </TouchableOpacity>
            );
        },

        Title(route, navigator, index, navState) {
            return (
                <Text style={[styles.navBarText, styles.navBarTitleText]}>
                    {route.title}
                </Text>
            );
        }
    });

    static searchRouteMapper = (props) => ({

        LeftButton(route, navigator, index, navState) {

            if (index === 0) {
                return (
                    <TouchableOpacity
                        onPress={function(){
                                props.topNavigator.popToTop();
                        }}
                        style={styles.navBarLeftButton}>
                        <Text style={[styles.navBarText, styles.navBarButtonText]}>
                            Landing Page
                        </Text>
                    </TouchableOpacity>
                );
            }
            var previousRoute = navState.routeStack[index - 1];
            return (
                <TouchableOpacity
                    onPress={() => navigator.pop()}
                    style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
                </TouchableOpacity>
            );
        },


        RightButton: function(route, navigator, index, navState) {
            return (
                <TouchableOpacity
                    onPress={() => navigator.push(newRandomRoute())}
                    style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
                </TouchableOpacity>
            );
        },

        Title(route, navigator, index, navState) {
            return (
                <Text style={[styles.navBarText, styles.navBarTitleText]}>
                    {route.title}
                </Text>
            );
        }
    });

    static accountRouteMapper = (props) => ({

        LeftButton(route, navigator, index, navState) {

            if (index === 0) {
                return (
                    <TouchableOpacity
                        onPress={function(){
                                props.topNavigator.popToTop();
                        }}
                        style={styles.navBarLeftButton}>
                        <Text style={[styles.navBarText, styles.navBarButtonText]}>
                            Landing Page
                        </Text>
                    </TouchableOpacity>
                );
            }
            var previousRoute = navState.routeStack[index - 1];
            return (
                <TouchableOpacity
                    onPress={() => navigator.pop()}
                    style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
                </TouchableOpacity>
            );
        },


        RightButton: function(route, navigator, index, navState) {
            return (
                <TouchableOpacity
                    onPress={() => navigator.push(newRandomRoute())}
                    style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
                </TouchableOpacity>
            );
        },

        Title(route, navigator, index, navState) {
            return (
                <Text style={[styles.navBarText, styles.navBarTitleText]}>
                    {route.title}
                </Text>
            );
        }
    });

    _renderContentRx() {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderSceneSearch}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                    <Navigator.NavigationBar
                      routeMapper={NavigationBarRouteMapper(this.props)}
                      style={styles.navBar}
                    />
                }
                navigationBarHidden={true}
                initialRoute={{
                  title: 'Login',
                  component: RxPage
                }}
            />
        );
    }

    static NavigationBarRouteMapper = props => ({

        LeftButton(route, navigator, index, navState) {

            if (index === 0) {
                return (
                    <TouchableOpacity
                        onPress={function(){
                                props.topNavigator.popToTop();
                        }}
                        style={styles.navBarLeftButton}>
                        <Text style={[styles.navBarText, styles.navBarButtonText]}>
                            Landing Page
                        </Text>
                    </TouchableOpacity>
                );
            }

            var previousRoute = navState.routeStack[index - 1];
            return (
                <TouchableOpacity
                    onPress={() => navigator.pop()}
                    style={styles.navBarLeftButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                        {previousRoute.title}
                    </Text>
                </TouchableOpacity>
            );
        },

        RightButton: function(route, navigator, index, navState) {
            return (
                <TouchableOpacity
                    onPress={() => navigator.push(newRandomRoute())}
                    style={styles.navBarRightButton}>
                    <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    </Text>
                </TouchableOpacity>
            );
        },

        Title(route, navigator, index, navState) {
            return (
                <Text style={[styles.navBarText, styles.navBarTitleText]}>
                    {route.title}
                </Text>
            );
        }
    });





    _renderContentRe(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={MainTabs.signUpRouteMapper(this.props)}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={true}
                initialRoute={{
                      title: 'Sign Up',
                      component: RemPage
                    }}
            />
        );
    }

    _renderContentSe(color: string, pageHeader: string, pageText?: string, num?: number) {

        var SearchPage

        if (this.props.DATA) {
            if (this.props.DATA.length == 1){
                SearchPage = SePage3;
            } else {
                SearchPage = SePage2;
            }
        } else {
            SearchPage = SePage1;
        }

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderSceneSearch}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={MainTabs.searchRouteMapper(this.props)}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: 'Search',
                      component: SearchPage
                    }}
            />
        );
    }

    _renderContentPi(color: string, pageHeader: string, pageText?: string, num?: number) {

        let discountName = 'Discount Card';

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator;}}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={MainTabs.accountRouteMapper(this.props)}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: discountName,
                      component: MemberCard,
                      textForCard: discountName
                    }}
            />
        );
    }

    _renderContentAc(color: string, pageHeader: string, pageText?: string, num?: number) {

        return(
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='red'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperAccount}
                          style={styles.navBar}
                        />
                    }
                navigationBarHidden={false}
                initialRoute={{
                      title: 'Account',
                      component: AcPage
                    }}
            />
        );
    }



    measureMainComponent(){
        this._root.measure((ox, oy, width, height, px, py) => {
            this.setState({
                width: width,
                height: height,
                ox: ox,
                oy:oy,
                px: px,
                py: py

            });
        });
    }

    renderSceneSearch(route, navigator) {
        var Component = route.component;
        return (
            <View style={[styles.container, {backgroundColor: CONFIG.backgroundCl}]}>
                <MenuContext style={{ flex: 1}} ref="MenuContext">
                    <Component
                        route={route}
                        navigator={navigator}
                        topNavigator={this.props.topNavigator}
                        DATA={this.props.DATA}
                    />
                </MenuContext>
            </View>
        )
    }

    renderScene(route, navigator) {

        var Component = route.component;


        return (
            <View style={[styles.container, {backgroundColor: CONFIG.backgroundCl}]}
                  ref={component => this._root = component}
            >
                <Component
                    route={route}
                    navigator={navigator}
                    topNavigator={this.props.topNavigator} />
            </View>
        )
    }

    render(){


        let discountName = 'Discount Card';


        return (
            <TabNavigator
                tintColor='white'
                barTintColor="#f99b53"
            >
                <TabNavigator.Item
                    title="Login"
                    renderIcon={() =>
                        <Image source={GLOBALS.icons.tabRx} style={{height:33, width: 33}}/>
                    }
                    renderSelectedIcon={() =>
                        <Image source={GLOBALS.icons.tabRx2} style={{height:33, width: 33}}/>
                    }
                    selected={this.state.selectedTab === 'myrx'}
                    onPress={
                    () => {
                        if (this.state.selectedTab !== 'myrx'){
                            GLOBALS.menuMap.activeTab = 'myrx';

                            this.setState({
                                selectedTab: 'myrx'
                            });

                        }
                    }}>
                    <Navigator
                        ref={(navigator) => { this.navigator = navigator; }}
                        renderScene={this.renderSceneSearch}
                        tintColor='red'
                        barTintColor='#FFFFFD'
                        titleTextColor='#D6573D'
                        navigationBar={
                            <Navigator.NavigationBar
                              routeMapper={MainTabs.loginRouteMapper(this.props)}
                              style={styles.navBar}
                            />
                        }
                        navigationBarHidden={true}
                        initialRoute={{
                          title: 'Login',
                          component: RxPage
                        }}
                    />
                </TabNavigator.Item>

                <TabNavigator.Item
                    title="Sign Up"
                    renderIcon={() =>
                        <Image source={GLOBALS.icons.tabReminder} style={{height:33, width: 33}}/>
                    }
                    renderSelectedIcon={() =>
                        <Image source={GLOBALS.icons.tabReminder2} style={{height:33, width: 33}}/>
                    }
                    selected={this.state.selectedTab === 'reminders' }
                    onPress={
                    () => {
                      if (this.state.selectedTab !== 'reminders'){
                      GLOBALS.menuMap.activeTab = 'reminders';
                        this.setState({
                          selectedTab: 'reminders'
                        });
                      }
                    }}>
                    {this._renderContentRe('#b14A8C', 'Reminders')}
                </TabNavigator.Item>

                <TabNavigator.Item
                    title="Search"
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabSearch} style={{height:32, width: 32}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabSearch2} style={{height:32, width: 32}}/>
                    }
                    selected={this.state.selectedTab === 'search'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'search'){
                      GLOBALS.menuMap.activeTab = 'search';
                        this.setState({
                          selectedTab: 'search'
                        });
                      }
                  }}>
                    {this._renderContentSe('#c1fA8C', 'search')}
                </TabNavigator.Item>

                <TabNavigator.Item
                    title={discountName}
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabPills} style={{height:20, width: 30, margin: 4}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabPills2} style={{height:20, width: 30, margin: 4}}/>
                    }
                    selected={this.state.selectedTab === 'pills'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'pills'){
                      GLOBALS.menuMap.activeTab = 'pills';

                        this.setState({
                          selectedTab: 'pills'
                        });
                      }
                  }}>
                    {this._renderContentPi('#d83E33', 'Pills')}
                </TabNavigator.Item>

                {/*<TabNavigator.Item
                    title="My Account"
                    renderIcon={() => 
                        <Image source={GLOBALS.icons.tabUser} style={{height:32, width: 32}}/>
                    }
                    renderSelectedIcon={() => 
                        <Image source={GLOBALS.icons.tabUser2} style={{height:32, width: 32}}/>
                    }
                    selected={this.state.selectedTab === 'account'}
                    onPress={() => {
                    if (this.state.selectedTab !== 'account'){
                      GLOBALS.menuMap.activeTab = 'account';

                        this.setState({
                          selectedTab: 'account'
                        });
                      }
                  }}>
                    {this._renderContentAc('#e1551C', 'My Account', this.state.presses)}
                </TabNavigator.Item>*/}
            </TabNavigator>
        )
    }


};

var styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    },

    messageText: {
        fontSize: 17,
        fontWeight: '500',
        padding: 15,
        marginTop: 50,
        marginLeft: 15,
    },

    button: {
        backgroundColor: 'white',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#CDCDCD',
    },

    buttonText: {
        fontSize: 17,
        fontWeight: '500',
    },

    navBar: {
        backgroundColor: GLOBALS.COLORS.orange,
    },

    navBarText: {
        fontSize: 16,
        marginVertical: 10,
    },

    navBarTitleText: {
        color: 'black',
        fontWeight: '500',
        marginVertical: 9,
    },

    navBarLeftButton: {
        paddingLeft: 10,
    },

    navBarRightButton: {
        paddingRight: 10,
    },

    navBarButtonText: {
        color: 'gray',
    },

    scene: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#EAEAEA',
    },

});

module.exports = MainTabs;