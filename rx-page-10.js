'use strict';

var React = require('react-native');

var {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
} = React;


var GLOBALS  = require('./Globals');



var Page = React.createClass({

  render: function(){
    return (


        <View style={[styles.tabContent]}>
          <ScrollView>
            <View style={{alignItems: 'center'}}>
            <Text style={styles.tabText}>If you're in a hurry and need a discount</Text>
            <Text>coupon for your medication right away,</Text>
            <Text>go straight to Search</Text>


            <Text style={styles.tabText}>Otherwise, let's quickly get My Rx set</Text>
            <Text>up with your prescriptions, so RxPreferred</Text>
            <Text>can provide you with the lowest prices</Text>
            <Text>for all your prescriptions.</Text>


            <View style={{
            backgroundColor:'red',
            padding:10,
            borderRadius: 10,
            marginTop: 30
          }}>
              <TouchableHighlight
                  onPress={() => {
                this.props.ButtonClick();
              }
            }>
                <Text style={{}}>Add prescriptions to My Rx</Text>

              </TouchableHighlight>

            </View>

            <Text style={styles.tabText}>Your prescription data is private and can be</Text>
            <Text>deleted from this app at any time in Account</Text>

          </View>
          </ScrollView>
        </View>

    );
  }

});


var styles = StyleSheet.create({
  tabContent: {
    marginTop: 45,
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'black',
    marginTop: 30,
    alignItems: 'center',
  },
});


module.exports = Page;