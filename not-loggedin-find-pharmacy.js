'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';

var GoogleMap = require('./SBGoogleMap');
var GLOBALS = require('./Globals');

class mapTestView5 extends Component {

    constructor(){
        super();
    }

    render() {
        return (
            <View style={styles.container}>
                <GoogleMap
                    style={{flex:1}}
                    cameraPosition={{auto: true, zoom: 10}}
                    showsUserLocation={true}
                    scrollGestures={true}
                    zoomGestures={true}
                    tiltGestures={true}
                    rotateGestures={true}
                    consumesGesturesInView={true}
                    compassButton={true}
                    myLocationButton={true}
                    indoorPicker={true}
                    allowScrollGesturesDuringRotateOrZoom={true}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        flexDirection:'row'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
});

module.exports = mapTestView5;
