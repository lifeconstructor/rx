var ga = this.ga = null;

var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var LoginScreen = React.createClass({

    getInitialState: function() {

        var URL = CONFIG.API_URL + CONFIG.update_phone;

        fetch(URL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        }).then((response) => response.json())
            .then((responseText) => {

                console.log(responseText);


                if (responseText.phone_number){

                    this.setState({
                        phoneNumber: responseText.phone_number
                    });

                }


            })
            .catch((error) => {

                this.setState({
                    error: 'Error connecting to server'
                });
            });


        return({
            phoneNumber: '',
            phoneNumberError : '',
            keyBoardHeight  : 0
        });
    },

    componentWillReceiveProps: function(){
        var URL = CONFIG.API_URL + CONFIG.update_phone;

        fetch(URL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        }).then((response) => response.json())
            .then((responseText) => {

                console.log(responseText);


                if (responseText.phone_number){

                    this.setState({
                        phoneNumber: responseText.phone_number
                    });

                }


            })
            .catch((error) => {

                this.setState({
                    error: 'Error connecting to server'
                });
            });
    },

    changePassword: function(){

        this.setState({
            phoneNumberError : ''
        });

        var URL = CONFIG.API_URL + CONFIG.update_phone;


        var test = JSON.stringify({
            phone_number: this.state.phoneNumber
        });

        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            },
            body: test
        }).then((response) => response.json())
            .then((responseText) => {

                console.log(responseText);

                if (responseText.errors){
                    if (responseText.errors.password){
                        this.setState({
                            phoneNumberError: responseText.errors.phone_number[0]
                        });
                    }

                }

                if (responseText.phone_number){


                    AlertIOS.alert('Data saved successfully');

                    this.props.navigator.pop({});
                }


            })
            .catch((error) => {

                this.setState({
                    error: 'Error connecting to server'
                });
            });

    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this));
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this));

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);
    },

    componentWillReceiveProps() {

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);

    },

    keyboardWillShow (e) {

        this.setState({keyBoardHeight: e.endCoordinates.height})
    },

    keyboardWillHide (e) {
        this.setState({keyBoardHeight: 0});

        //this.refs._scrollView.scrollTo(200);
    },

    render: function() {

        return (
            <ScrollView ref='_scrollView' >

                <View style={[styles.container, {marginBottom: this.state.keyBoardHeight, backgroundColor: GLOBALS.backgroundCl}]}>



                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Phone Number
                    </Text>

                    <TextInput
                        style={styles.input}
                        onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                        value={this.state.phoneNumber}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.phoneNumberError}
                    </Text>

                    <TouchableHighlight
                        onPress={this.changePassword}
                        style={styles.loginButton}
                    >
                        <Text style={styles.loginText}>
                            Change Phone Number
                        </Text>
                    </TouchableHighlight>


                </View>
            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 20
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        borderRadius: 5,
        backgroundColor: 'white',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white',
        fontSize: 20
    }

});

module.exports = LoginScreen;