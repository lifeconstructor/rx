'use strict';

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TextInput,
    TouchableHighlight,
    DatePickerIOS,
    NativeAppEventEmitter
} from 'react-native';

var RNCalendarReminders = require('react-native-calendar-reminders');

var PushNotification = require('react-native-push-notification');

PushNotification.configure({

    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function(token) {
        console.log( 'TOKEN:', token );
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
    },

    // ANDROID ONLY: (optional) GCM Sender ID.
    senderID: "YOUR GCM SENDER ID",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * IOS ONLY: (optional) default: true
     * - Specified if permissions will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     */
    requestPermissions: true,
});

var CONFIG = require('./config/config');

var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var AutoComplete = require('react-native-autocomplete');



var LoginScreen = React.createClass({

    getInitialState: function() {

        RNCalendarReminders.authorizeEventStore((error, auth) => {
            console.log(error);
            console.log(auth);

        });

        RNCalendarReminders.fetchAllReminders(reminders => {
            console.log(reminders);
        });

        var rec = this.props.route.reminder.recurrence == "CUSTOM"? true:false;

        return({
            drugName            : '',
            selectedDrug        : this.props.route.reminder.product_id,  //name
            data                : [],
            drugsAutocomplete   : [],
            date                : new Date(this.props.route.reminder.startDate),
            endDate             : new Date(this.props.route.reminder.endDate),
            productId           : this.props.route.reminder.id,
            lifePeriod          : 800,
            prodIdError         : '',
            reguliarity         : this.props.route.reminder.recurrence,
            mode                : 'datetime',
            custom              : rec,
            customAlarms        : this.props.route.reminder.alarms,
            Reguliarity         : ['ONCE', 'DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY', 'CUSTOM'],
            reminderId          : this.props.route.id
        });
    },

    componentWillMount () {
        this.eventEmitter = NativeAppEventEmitter.addListener('EventReminder', (reminders) => {
            this.setState({
                reminders: reminders
            });
        });
    },

    componentWillUnmount () {
        this.eventEmitter.remove();
    },

    getDataSource1: function(test: Array<any>): ListView.DataSource {
        return this.state.dataSource.cloneWithRows(test);
    },

    onTyping: function (text) {


        this.setState({drugName: text});

        var URL = CONFIG.API_URL + 'api/autocomplete-product'+ '?drug_name=' + this.state.drugName;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                var drugs = responseData.filter(function (drug) {
                    return drug.PROPRIETARYNAME.toLowerCase().startsWith(text.toLowerCase())
                }).map(function (drug) {
                    var Tier = '';



                    switch(Math.floor((Math.random() * 3) + 1)) {
                        case 1:
                            Tier = ' (NF)';
                            break;
                        case 2:
                            Tier = ' (Tier 1)';
                            break;
                        default:
                            Tier = ' (Tier 2)';
                    }

                    return drug.PROPRIETARYNAME + Tier;
                });

                this.setState({
                    drugsAutocomplete: drugs,
                    data: responseData
                });
            })
            .done();

    },

    onDateChange: function(date) {
        this.setState({date: date});
    },

    onDateChangeCustom: function(date, index) {

        var temp = this.state.customAlarms;
        temp[index] = date;

        this.setState({
            customAlarms: temp
        });
    },

    onEndDateChange: function(date) {
        this.setState({endDate: date});
    },

    addCustomDate: function() {

        this.setState({
            customAlarms: this.state.customAlarms.concat([new Date()])
        });


    },

    removeCustomDate: function() {

        this.setState({
            customAlarms: this.state.customAlarms.slice(0, -1)
    });

    },

    onChange: function(reguliarity){

        this.setState({reguliarity: reguliarity});

        if (reguliarity == 'DAILY'){
            this.setState({mode: 'time'})
        } else {
            this.setState({mode: 'datetime'})
        }

        if (reguliarity == 'CUSTOM'){
            this.setState({custom: true})
        } else {
            this.setState({custom: false})
        }

    },

    updateLocalReminders: function(){

        PushNotification.cancelAllLocalNotifications();

        var URL = 'http://v2-0-rxscout.pilgrimconsulting.com/backend/api/get-reminders?access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                /*this.setState({
                 dataSourceRemindersAPI: this.getDataSource1(responseData.result),
                 dataRaw: responseData.result
                 });*/



                if (responseData && responseData.result && responseData.result.length){

                    responseData.result.map((data, index) =>{

                        var reminderInfo = JSON.parse(data.reminder);
                        console.log(reminderInfo);



                        switch(reminderInfo.recurrence) {
                            case 'ONCE':

                                var dateOnce = '';
                                dateOnce = new Date(reminderInfo.startDate);

                                if (dateOnce > new Date()) {
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: (dateOnce).toISOString()
                                    });
                                }

                                break;
                            case 'DAILY':
                                var dataD = new Date(reminderInfo.startDate);

                                var DAILY = 0;

                                while (dataD < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    DAILY++;

                                    if (dataD > new Date()) {
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataD.toISOString()
                                        });
                                    }
                                    dataD.setDate(dataD.getDate() + 1);
                                }
                                if (!DAILY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'WEEKLY':
                                var dataW = new Date(reminderInfo.startDate);

                                var WEEKLY = 0;

                                while (dataW < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    WEEKLY++;

                                    if (dataW > new Date()){
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataW.toISOString()
                                        });
                                    }
                                    dataW.setDate(dataW.getDate() + 7);
                                }

                                if (!WEEKLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'MONTHLY':
                                var dataM = new Date(reminderInfo.startDate);

                                var MONTHLY = 0;

                                while (dataM < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    MONTHLY++;

                                    if (dataM > new Date()){
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataM.toISOString()
                                        });
                                    }

                                    dataM.setMonth(dataM.getMonth() + 1);
                                }

                                if (!MONTHLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }

                                break;
                            case 'YEARLY':
                                var dataY = new Date(reminderInfo.startDate);

                                var YEARLY = 0;

                                while (dataY < (new Date(reminderInfo.endDate)).setHours(23,59,59,999)) {
                                    YEARLY++;

                                    if (dataY > new Date()) {
                                        PushNotification.localNotificationSchedule({
                                            message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                            date: dataY.toISOString()
                                        });
                                    }

                                    dataY.setYear(dataY.getYear() + 1);
                                }

                                if (!YEARLY) {
                                    AlertIOS.alert('No reminders set for given time period');
                                }
                                break;
                            case 'CUSTOM':

                                var dateCustomF = '';
                                dateCustomF = new Date(reminderInfo.startDate);

                                if (dateCustom > new Date()) {
                                    PushNotification.localNotificationSchedule({
                                        message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                        date: (dateCustom).toISOString()
                                    });
                                }


                                if (reminderInfo.alarms.length){

                                    reminderInfo.alarms.map((dataCustom, index) => {
                                        if (dataCustom > new Date()) {
                                            PushNotification.localNotificationSchedule({
                                                message: "Do not forget to buy (" + reminderInfo.product_id + ")", // (required)
                                                date: dataCustom
                                            });
                                        }
                                    })
                                }

                                break;
                            default:
                                console.log('DEFAULT');
                                break;
                        }
                    });

                }
            })
            .done();
    },

    addReminder: function(){

        if (this.state.date < new Date) {
            AlertIOS.alert('Reminder cannot be set as the time has already passed');
            return;
        }

        var drugTitle = this.state.selectedDrug;


        var URL = 'http://v2-0-rxscout.pilgrimconsulting.com/backend/api/update-reminder?id='+this.state.reminderId;

        var type = this.state.reguliarity != "CUSTOM" ? "REGULAR": "CUSTOM";

        if (this.state.reguliarity == 'CUSTOM') {
            var dateMax = new Date(Math.max( ...this.state.customAlarms));

            dateMax = dateMax > this.state.date ? dateMax : this.state.date;

            this.setState({
                endDate: dateMax
            });
        }

        var test = JSON.stringify (
            {
                "id"            : this.state.reminderId,
                "product_id"    : this.state.selectedDrug,
                "id_local"      : this.state.date.getTime(),
                "startDate"     : this.state.date.getTime(),
                "location"      : "Not specified.",
                "notes"         : "Check RxScout app for details.",
                "alarms"        : this.state.customAlarms,
                "type"          : type,
                "recurrence"    : this.state.reguliarity,
                "every"         : "1",
                "months"        : [],
                "days_of_month" : [],
                "days_of_week"  : [],
                "endDate"       : this.state.endDate.getTime()

            }
        );

        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            },
            body: test
        })
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.errors.product_id){
                    this.setState({
                        prodIdError: responseData.errors.product_id
                    });
                } else {
                    this.setState({
                        prodIdError: ''
                    });
                }

                if (responseData.success){

                    AlertIOS.alert('Reminder saved successfully');

                    this.updateLocalReminders();


                    this.props.navigator.pop();
                }
            })
            .done();
    },

    render: function() {




        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <View style={[styles.container, {backgroundColor: GLOBALS.backgroundCl}]}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 10}}>
                            Drug Title
                        </Text>
                        <Text style={{fontSize: 24, color: '#357bb5'}}>
                            {this.state.selectedDrug}
                        </Text>

                        <Text style={{color: 'red', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            {this.state.prodIdError}
                        </Text>

                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            Reminder Type
                        </Text>

                        <PickerIOS
                            style={{width: 300}}
                            selectedValue={this.state.reguliarity}
                            onValueChange={(reguliarity) => {
                                this.onChange(reguliarity);
                            }}>

                            {this.state.Reguliarity.map((reguliarity) => (
                                <PickerItemIOS
                                    key={reguliarity}
                                    value={reguliarity}
                                    label={reguliarity}
                                />
                            ))}
                        </PickerIOS>

                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 3, marginTop: 3}}>
                        </Text>


                        <DatePickerIOS
                            date={this.state.date}
                            onDateChange={this.onDateChange}
                            mode={this.state.mode}
                        />


                        {this.state.reguliarity == 'ONCE' || this.state.reguliarity == 'CUSTOM'?
                            <Text></Text>
                            :
                            <View style={{alignItems:'center'}}>
                                <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 3, marginTop: 3}}>
                                    End Date
                                </Text>

                                <DatePickerIOS
                                    date={this.state.endDate}
                                    onDateChange={this.onEndDateChange}
                                    mode='date'
                                />
                            </View>

                        }


                        {this.state.custom?
                            <View style={{alignItems: 'center'}}>

                                {this.state.customAlarms.map((alarm, index, test) => (
                                    <View style={{margin: 20}}>
                                        <DatePickerIOS
                                            date={new Date(this.state.customAlarms[index])}
                                            onDateChange={(date) =>this.onDateChangeCustom(date, index)}
                                            mode={this.state.mode}
                                        />
                                    </View>
                                ))}

                                <View style={{alignItems: 'center'}}>
                                    <TouchableHighlight
                                        onPress={this.addCustomDate}
                                        style={styles.addReminderButton}
                                    >
                                        <Text style={styles.loginText}>
                                            Add Another Date
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    <TouchableHighlight
                                        onPress={this.removeCustomDate}
                                        style={styles.addReminderButton}
                                        >
                                        <Text style={styles.loginText}>
                                            Remove Date
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            :
                            <Text></Text>
                        }

                        <View>
                            <TouchableHighlight
                                onPress={this.addReminder}
                                style={[styles.addReminderButton, {marginHorizontal: 50, alignItems: 'center'}]}
                            >
                                <Text style={styles.loginText}>
                                    Save Reminder
                                </Text>
                            </TouchableHighlight>
                        </View>
                    </View>

                </ScrollView>

            </View>
        );
    }
});

const styles = StyleSheet.create({

    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 5
    },
    container: {
        marginTop: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    input:{
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 5,
        padding: 5,
        flex: 1
    },
    addReminderButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginBottom: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white',
        fontSize: 20
    }

});

module.exports = LoginScreen;