'use strict';

var Globals = require('./app/globals').load();

var LandingPage = require('./landing-page-navigator');

var ga = this.ga = null;

var RxScout = React.createClass({


  handleFirstConnectivityChange(reach) {
    
    if(reach != 'none'){
      this.lockScreen(false);
    } else {
      Alert.alert(
          'No Internet Connection',
          'Please connect to the Internet',
          [
            {text: 'OK', onPress: () => this.lockScreen(true)}
          ]
      )
    }
  },

  lockScreen(param){
    this.setState({
        screenLocked: param
    });

  },

  getInitialState(){
    return({
      screenLocked: false
    });
  },

  componentWillMount() {

    NetInfo.addEventListener(
        'change',
        this.handleFirstConnectivityChange
    );

    let clientId = DeviceInfo.getUniqueID();
    let userAgent = DeviceInfo.getUserAgent();

    ga = new Analytics('UA-75829701-1',clientId, 1, DeviceInfo.getUserAgent());
    var screenView = new GAHits.ScreenView('RxScout', 'Index Component', CONFIG.ios_app_version, 'com.snb.rxscout');
    ga.send(screenView);

  },

  componentWillUnmount(){

    NetInfo.removeEventListener(
        'change',
        handleFirstConnectivityChange
    );

  },

  componentWillReceiveProps() {

    let clientId = DeviceInfo.getUniqueID();
    ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
    var screenView = new GAHits.ScreenView('RxScout', 'Index Component', CONFIG.ios_app_version, 'com.snb.rxscout');
    ga.send(screenView);

  },

  _onChoice(testName, variantName) {

    var experiment = new GAExperiment(testName, variantName);

    var state = this.state;
    state.experiments[testName] = experiment;
    this.setState(state);
  },

  _resetExperiment() {
    this.refs.welcomeMessageTest.reset();
  },

  _sendEvent() {
    var experiment = this.state.experiments['welcome-message'];
    var gaEvent = new GAHits.Event('Demos', 'send', 'React Native', 100, experiment);
    ga.send(gaEvent);
  },

  render: function() {

    var DisplayedData = <LandingPage/>;

    if (this.state.screenLocked) {

      DisplayedData = <View style={{flex:1}}><Text style={{alignSelf: 'center', marginTop: 40}}>Not Connected</Text></View>;

    }
    return (

      DisplayedData

    );
  }

});

AppRegistry.registerComponent('RxScout', () => RxScout);