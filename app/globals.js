var React = require('react-native')
Globals = {

    vars: {
        _: require('lodash'),
        DB: require('./db'),
        DBEvents: require('react-native-db-models').DBEvents,
        Router: require('react-native-router'),
        React: require('react-native'),
        AppRegistry: React.AppRegistry,
        AppStateIOS: React.AppStateIOS,
        AlertIOS: React.AlertIOS,
        Alert: React.Alert,
        NativeModules: React.NativeModules,
        Text: React.Text,
        View: React.View,
        ListView: React.ListView,
        PushNotificationIOS: React.PushNotificationIOS,
        StyleSheet: React.StyleSheet,
        TouchableHighlight: React.TouchableHighlight,
        TouchableOpacity: React.TouchableOpacity,
        TextInput: React.TextInput,
        Image: React.Image,
        Navigator: React.Navigator,
        NavigatorIOS: React.NavigatorIOS,
        ScrollView: React.ScrollView,
        Analytics: require('react-native-google-analytics').Analytics,
        GAHits: require('react-native-google-analytics').Hits,
        GAExperiment: require('react-native-google-analytics').Experiment,
        DeviceEventEmitter: React.DeviceEventEmitter,
        DeviceInfo: require('react-native-device-info'),
        CONFIG: require('../config/config').CONFIG,
        Dimensions: React.Dimensions,
        PickerIOS: React.PickerIOS,
        PickerItemIOS : React.PickerIOS.Item,
        NetInfo: React.NetInfo
    },

    load: function () {
        var me = this;
        var _ = me.vars._

        _.map(me.vars, function (v, k) {
            window[k] = v
        })
        return window
    }

}

module.exports = Globals
