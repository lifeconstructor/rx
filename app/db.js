var RNDBModel = require('react-native-db-models')

var DB = {
    "search_query": new RNDBModel.create_db("search_query"),
    "recent_searches2": new RNDBModel.create_db("recent_searches2"),
}

module.exports = DB