'use strict';

var React = require('react-native');
var {
    StyleSheet,
    Text,
    Platform,
    View,
    ListView,
    Dimensions,
    } = React;

var GLOBALS = require('./Globals');

var AutoComplete = require('react-native-autocomplete');

var window = Dimensions.get('window');
var TimerMixin = require('react-timer-mixin');
var invariant = require('invariant');


var MainContainer = require('./main-container');

var SePage2 = require('./search-page-2');


// Main Component
var SearchPage = React.createClass({

    mixins: [TimerMixin],

    timeoutID: (null: any),


getInitialState: function() {

    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    return {
        isLoading: false,
        isLoadingTail: false,
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        searchResults: ds.cloneWithRows([]),
        filter: '',
        queryNumber: 0,
        www: 0,
        searchBarWidth: 0,
        showMore: 0,
        showMore2: 1,
        test: 30,
        data: [],
        drugsAutocomplete: [],
        selectedDrug: ''

    };
},


selectDrug: function(drugTitle) {


    drugTitle = drugTitle.replace(" (NF)", "");
    drugTitle = drugTitle.replace(" (Tier 1)", "");
    drugTitle = drugTitle.replace(" (Tier 2)", "");

    this.setState({selectedDrug: drugTitle});


    for (var i in this.state.data) {
        if (this.state.data[i].PROPRIETARYNAME == this.state.selectedDrug){
            this.setState({
                productId: this.state.data[i].id
            });
        }
    }

    var URL = CONFIG.API_URL + 'api/products' + '?query=' + this.state.selectedDrug + '&page_limit=100&page_number=0&apikey=' + GLOBALS.API_KEY;


    fetch(URL)
        .then((response) => response.json())
        .catch((error) => {

        })
        .then((responseData) => {

            if (responseData.result)
                GLOBALS.DATA = responseData.result;
            if (drugTitle != "No Recent Searches"){

                GLOBALS.QUERY = drugTitle;

                this.props.navigator.push({
                    title: GLOBALS.titleCut(drugTitle),
                    component: SePage2,
                    drug: responseData.result[0]
                });
            }
        })
        .done();
},


onTyping: function (text) {

    this.setState({drugName: text});

    var URL = CONFIG.API_URL + 'api/autocomplete-product'+ '?drug_name=' + this.state.drugName;

    fetch(URL)
        .then((response) => response.json())
        .catch((error) => {

        })
        .then((responseData) => {


            var drugs = responseData.filter(function (drug) {

                return drug.PROPRIETARYNAME.toLowerCase().startsWith(text.toLowerCase())
            }).map(function (drug) {
                var Tier = '';



                switch(Math.floor((Math.random() * 3) + 1)) {
                    case 1:
                        Tier = ' (NF)';
                        break;
                    case 2:
                        Tier = ' (Tier 1)';
                        break;
                    default:
                        Tier = ' (Tier 2)';
                }

                return drug.PROPRIETARYNAME + Tier;
            });

            this.setState({
                drugsAutocomplete: drugs,
                data: responseData
            });
        })
        .done();

},



measureMainComponent: function() {
    this.refs.MainComponent.measure((ox, oy, width) => {
        this.setState({
            www: width,
            searchBarWidth: width-6,
        });
    });
},



render: function() {

    var leftPosition = 0;
    var searchTopPosition = (Platform.OS === 'ios')? 7 : 0;
    var scrollerForLandscape = (this.state.www > 480)? 250 : 530;



    return (

        <MainContainer>
            <View onLayout={this.measureMainComponent.bind(null,this)} ref="MainComponent">


                <ScrollView style={{marginTop:10, height: scrollerForLandscape}}>

                    <AutoComplete
                        onTyping={this.onTyping}
                        onSelect={(e) => this.selectDrug(e)}
                        onBlur={() => {}}
                        onFocus={() => {}}
                        onSubmitEditing={(e) => {}}
                        onEndEditing={(e) => {}}

                        suggestions={this.state.drugsAutocomplete}

                        placeholder='Drug title'
                        style={styles.autocomplete}
                        clearButtonMode='always'
                        returnKeyType='go'
                        textAlign='center'
                        clearTextOnFocus={true}

                        maximumNumberOfAutoCompleteRows={6}
                        applyBoldEffectToAutoCompleteSuggestions={true}
                        reverseAutoCompleteSuggestionsBoldEffect={true}
                        showTextFieldDropShadowWhenAutoCompleteTableIsOpen={false}
                        autoCompleteTableViewHidden={false}

                        autoCompleteTableBorderColor='lightblue'
                        autoCompleteTableBackgroundColor='azure'
                        autoCompleteTableCornerRadius={10}
                        autoCompleteTableBorderWidth={1}

                        autoCompleteRowHeight={35}

                        autoCompleteFontSize={15}
                        autoCompleteRegularFontName='Helvetica Neue'
                        autoCompleteBoldFontName='Helvetica Bold'
                        autoCompleteTableCellTextColor={'black'}
                    />


                </ScrollView>

            </View>

        </MainContainer>
    );
}
});

var styles = StyleSheet.create({
    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
        borderRadius: 5
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 50,

    },

    innerContainer: {
        borderRadius: 10,
        alignItems: 'center',
    },
    row: {
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingTop: 5,
    },
    cellBorder: {
        backgroundColor: '#d1d1d1',
        height: 1,
        marginLeft: 4,
    },
    profpic: {
        width: 50,
        height: 50,
    },
    title: {
        fontSize: 16,
        marginBottom: 5,
    },
    subtitle: {
        fontSize: 16,
        marginBottom: 8,
    },
    textcontainer: {
        flex: 0.88,
        paddingLeft: 19,
    },
    blanktext: {
        padding: 10,
        fontSize: 20,
    },
    showContent: {height: 0},
    showContentHidden: {padding: 10},
});

module.exports = SearchPage;