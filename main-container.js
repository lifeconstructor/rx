
var React = require('react-native');
var {

    View,

    } = React;

// Main Container component
var MainContainer = React.createClass({
    render: function() {
        return (
            <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#eee'}}>
                <View style={{height:60}} />
                {this.props.children}
            </View>

        );
    }
});

module.exports = MainContainer;