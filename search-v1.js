'use strict';

var React = require('react-native');
var {
    StyleSheet,
    Text,
    ListView,
    } = React;

var GLOBALS = require('./Globals');



// Main Component
var SearchPage = React.createClass({

    getInitialState: function() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows(GLOBALS.DATA)
        };
    },

    showProductPackages: function(){

    },

    render: function() {
        return (
            <ScrollView style={{marginTop: 64}}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={function(rowData){
                        return(
                            <TouchableHighlight

                                style={{borderWidth: 1, borderRadius: 5, borderColor: 'black'}}
                            >
                                <View>
                                    <Text>
                                        PROPRIETARYNAME:
                                    </Text>
                                    <Text>
                                        {rowData.product.PROPRIETARYNAME}
                                    </Text>
                                    <Text>
                                        NONPROPRIETARYNAME:
                                    </Text>
                                    <Text>
                                        {rowData.product.NONPROPRIETARYNAME}
                                    </Text>
                                    <Text>
                                        {rowData.product.DOSAGEFORMNAME}
                                    </Text>
                                     <Text>
                                        {rowData.product.LABELERNAME}
                                    </Text>
                                    <Text>
                                        {rowData.product.PRODUCTNDC}
                                    </Text>
                                </View>
                            </TouchableHighlight>

                        );
                    }}
                />
            </ScrollView>
        );
    },
});


var styles = StyleSheet.create({

    loginButton: {
        width: 200,
        backgroundColor: 'red',
        borderRadius: 10,
        margin: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        fontSize: 20,
        color: 'black'
    }
});

module.exports = SearchPage;