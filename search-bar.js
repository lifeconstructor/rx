var React = require('react-native');

var {
    ActivityIndicatorIOS,
    TextInput,
    StyleSheet,
    View,
    Platform,
    } = React;

var GLOBALS = require('./Globals');

var SearchBar = React.createClass({


  setNativeProps: function(nativeProps){
      this._textInput.setNativeProps(nativeProps);
  },

  render: function() {
    return (
        <View style={[styles.searchBar,{width: this.props.SearchBarWidth}]}>
          <TextInput 
              ref={component => this._textInput = component}
              {...this.props}
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              onChange={this.props.onSearchChange}
              placeholder={this.props.placeholder}
              onFocus={this.props.onFocus}
              style={(Platform.OS === 'ios')?styles.searchBarInput:styles.searchBarInputA}
          />
        </View>
    );
  }
});

var styles = StyleSheet.create({
  searchBar: {
    borderWidth:1,
    borderColor: '#cccccc',
    borderRadius: 8,
    alignItems: 'center',
    height: 30,
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginLeft: 3,
    marginRight: 3,
  },
  searchBarInputA: {
    fontSize: 18,
    flex: 1,

    
    height:46,
    
    margin:3,
    marginLeft:4,
    marginTop: 5,
paddingTop: 7,

    flexDirection: 'row',
    color: 'black',
    backgroundColor: 'transparent',    
  },
  searchBarInput: {
    fontSize: 18,
    flex: 1,
    height:20,
    margin:3,
    marginLeft:4,
    flexDirection: 'row',
    color: 'black',
    backgroundColor: 'transparent',
    //borderColor: '#cccccc',
    //borderRadius: 3,
    //borderWidth: 1,
    //paddingLeft: 8
  },
  spinner: {
    width: 30,
  },
});

module.exports = SearchBar;