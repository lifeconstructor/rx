'use strict';



var GLOBALS = require('./Globals');

var AutoComplete = require('react-native-autocomplete');



var window = Dimensions.get('window');
var TimerMixin = require('react-timer-mixin');
var invariant = require('invariant');



var SePage2 = require('./search-page-2');

var SePage3 = require('./search-page-3');

var SearchHistory = require('./search-history');

var {height, width} = Dimensions.get('window');



// Main Component
var SearchPage = React.createClass({

    mixins: [TimerMixin],

    timeoutID: (null:any),


    getInitialState: function () {

        var URL = CONFIG.API_URL + 'api/get-from-history?access-token=' + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    searchResults: this.getDataSource1(responseData)
                });

            })
            .done();

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return {
            isLoading: false,
            isLoadingTail: false,
            searchResults: ds.cloneWithRows([]),
            filter: '',
            queryNumber: 0,
            www: 0,
            searchBarWidth: 0,
            showMore: 0,
            showMore2: 1,
            test: 30,
            data: [],
            drugsAutocomplete: [],
            selectedDrug: ''

        };
    },

    componentWillReceiveProps: function () {

        var URL = CONFIG.API_URL + 'api/get-from-history?access-token=' + GLOBALS.API_KEY;

        fetch(URL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        })
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                this.setState({
                    searchResults: this.getDataSource1(responseData)
                });

            })
            .done();

    },

    getDataSource1: function (test:Array<any>):ListView.DataSource {
        return this.state.searchResults.cloneWithRows(test);
    },

    selectDrug: function (drugTitle) {


        var DRUGS = this.state.data
            .filter(function onlySelected(value, index, self) {
                return value.name === drugTitle;
            });

        var URL_add_to_history = CONFIG.API_URL + 'api/add-to-history?product_ndc=' + this.state.productId + '&product_name=' + drugTitle;


        fetch(URL_add_to_history, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        })
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                console.log(responseData);

            })
            .done();

        this.props.navigator.push({
            title: GLOBALS.titleCut(drugTitle),
            component: SePage2,
            drugs: DRUGS,
            drugTitle: drugTitle
        });


    },

    selectDrugFromHistory: function (drugTitle) {

        this.setState({drugName: drugTitle});

        var URL = 'https://dplu-preview.data-rx.com/api/v1/drugs?name:prefix=' + drugTitle;


        fetch(URL, {
            method: 'GET',
            headers: {
                'Host': 'dplu-preview.data-rx.com',
                'Connection': 'keep-alive',
                'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                'Referer': 'https://dplu-preview.data-rx.com/',
                'Accept-Encoding': 'gzip, deflate, sdch',
                'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
            }
        })
            .catch((error) => {

                console.log(error);

            })
            .then((responseData) => {

                var drugs = JSON.parse(responseData._bodyText);

                var drugsAutocomplete = [];

                if (!drugs.message){

                    var DRUGS = drugs
                        .filter(function onlySelected(value, index, self) {
                            return value.name === drugTitle;
                        });


                    this.props.navigator.push({
                        title: GLOBALS.titleCut(drugTitle),
                        component: SePage2,
                        drugs: DRUGS,
                        drugTitle: drugTitle
                    });
                }
            })
            .done();

        var URL_add_to_history = CONFIG.API_URL + 'api/add-to-history?product_ndc=' + this.state.productId + '&product_name=' + drugTitle;


        fetch(URL_add_to_history, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            }
        })
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                console.log(responseData);

            })
            .done();




    },


gotoRecentSearches: function () {

        this.props.navigator.push({
            title: GLOBALS.titleCut('Recent Searches'),
            component: SearchHistory
        });

    },


    onTyping: function (text) {

        this.setState({drugName: text});

        var URL = 'https://dplu-preview.data-rx.com/api/v1/drugs?name:prefix=' + this.state.drugName;


        fetch(URL, {
            method: 'GET',
            headers: {
                'Host': 'dplu-preview.data-rx.com',
                'Connection': 'keep-alive',
                'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                'Referer': 'https://dplu-preview.data-rx.com/',
                'Accept-Encoding': 'gzip, deflate, sdch',
                'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
            }
        })
            .catch((error) => {

                console.log(error);

            })
            .then((responseData) => {

                var drugs = JSON.parse(responseData._bodyText);

                var drugsAutocomplete = [];

                if (!drugs.message){



                    drugsAutocomplete = drugs.map(function (drug) {
                        return drug.name;
                    }).filter(function onlyUnique(value, index, self) {
                        return self.indexOf(value) === index;
                    });


                    this.setState({
                        drugsAutocomplete: drugsAutocomplete,
                        data: drugs
                    });

                }

            })
            .done();

    },


    measureMainComponent: function () {
        this.refs.MainComponent.measure((ox, oy, width, height) => {
            this.setState({
                www: width,
                hhh: height
            });
        });
    },


    render: function () {

        let listOfDrugs = '';

        var test = (this.state.searchResults._cachedRowCount);
        if (test) {
            listOfDrugs =
                <ListView
                    dataSource={this.state.searchResults}
                    renderRow={(rowData) =>

                                <TouchableHighlight
                                    selectDrug={this.selectDrug}
                                    productData = {rowData}
                                    onPress={
                                        function(){
                                            this.selectDrug(rowData.product_name)
                                        }
                                    }
                                    style={{borderWidth: 1, borderRadius: 5, borderColor: 'black'}}
                                >
                                    <View style={{borderWidth: 1, borderColor: 'lightgray', backgroundColor: 'white', margin: 2, borderRadius:5}}>
                                        <Text style={{margin: 8, fontSize: 16}}>{rowData.product_name}</Text>
                                    </View>

                                </TouchableHighlight>
                                }
                />
        } else {
            listOfDrugs =
                <View style={{borderWidth: 1, borderColor: 'gray', backgroundColor: 'white'}}>
                    <View style={{flex: 1, marginTop: 10, marginBottom: 10}}>
                        <Text
                            style={{height: 20, marginLeft: 20, fontSize: 18}}
                        >
                            No Recent Searches
                        </Text>
                    </View>
                </View>
        };

        return (
            <View>
                <ScrollView style={{width: width, height: height-125, marginTop: 64}}>
                    <AutoComplete
                        onTyping={this.onTyping}
                        onSelect={(e) => this.selectDrug(e)}
                        onBlur={() => {}}
                        onFocus={() => {}}
                        onSubmitEditing={(e) => {}}
                        onEndEditing={(e) => {}}

                        suggestions={this.state.drugsAutocomplete}

                        placeholder='Drug title'
                        style={styles.autocomplete}
                        clearButtonMode='always'
                        returnKeyType='go'
                        textAlign='center'
                        clearTextOnFocus={true}

                        maximumNumberOfAutoCompleteRows={6}
                        applyBoldEffectToAutoCompleteSuggestions={true}
                        reverseAutoCompleteSuggestionsBoldEffect={true}
                        showTextFieldDropShadowWhenAutoCompleteTableIsOpen={false}
                        autoCompleteTableViewHidden={false}

                        autoCompleteTableBorderColor='lightblue'
                        autoCompleteTableBackgroundColor='azure'
                        autoCompleteTableCornerRadius={10}
                        autoCompleteTableBorderWidth={1}

                        autoCompleteRowHeight={35}

                        autoCompleteFontSize={15}
                        autoCompleteRegularFontName='Helvetica Neue'
                        autoCompleteBoldFontName='Helvetica Bold'
                        autoCompleteTableCellTextColor={'black'}
                    />

                    <View style={{flexDirection: 'row', borderWidth: 1, borderColor: 'gray', backgroundColor: 'white'}}>
                        <View style={{flex: 0.6, marginTop: 10, marginBottom: 10}}>
                            <Text
                                style={{height: 20, marginLeft: 20, fontSize: 18}}
                            >
                                Recent Searches
                            </Text>
                        </View>
                        <TouchableHighlight
                            onPress={this.gotoRecentSearches}
                            style={{
                                flex: 0.4,
                                borderRadius: 10,
                                marginTop: 5,
                                marginHorizontal: 5,
                                alignItems: 'center',
                                backgroundColor: '#357bb5',
                                padding: 5
                                }}
                        >
                            <Text style={{color: 'white'}}>
                                Manage
                            </Text>
                        </TouchableHighlight>

                    </View>

                    <ListView
                        dataSource={this.state.searchResults}
                        renderRow={(rowData) =>

                                <TouchableHighlight
                                    selectDrugFromHistory={this.selectDrugFromHistory}
                                    productData = {rowData}
                                    onPress={
                                        function(){
                                            this.selectDrugFromHistory(rowData.product_name)
                                        }
                                    }
                                    style={{borderWidth: 1, borderRadius: 5, borderColor: 'black'}}
                                >
                                    <View style={{borderWidth: 1, borderColor: 'lightgray', backgroundColor: 'white', margin: 2, borderRadius:5}}>
                                        <Text style={{margin: 8, fontSize: 16}}>{rowData.product_name}</Text>
                                    </View>

                                </TouchableHighlight>
                                }
                    />

                </ScrollView>
            </View>
        );
    }
});

var styles = StyleSheet.create({
    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        marginLeft: 1,
        marginRight: 1,
        marginTop: 1,
        borderRadius: 5,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 50,

    },

    innerContainer: {
        borderRadius: 10,
        alignItems: 'center',
    },
    row: {
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingTop: 5,
    },
    cellBorder: {
        backgroundColor: '#d1d1d1',
        height: 1,
        marginLeft: 4,
    },
    profpic: {
        width: 50,
        height: 50,
    },
    title: {
        fontSize: 16,
        marginBottom: 5,
    },
    subtitle: {
        fontSize: 16,
        marginBottom: 8,
    },
    textcontainer: {
        flex: 0.88,
        paddingLeft: 19,
    },
    blanktext: {
        padding: 10,
        fontSize: 20,
    },
    showContent: {height: 0},
    showContentHidden: {padding: 10},
});

module.exports = SearchPage;