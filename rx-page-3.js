'use strict';

var React = require('react-native');

var {
  SegmentedControlIOS,
  ScrollView,
  StyleSheet,
  TabBarIOS,
  NavigatorIOS,
  Text,
  View,
  TouchableHighlight,
  TextInput
} = React;

var BasicSegmentedControlExample = React.createClass({
  render() {
    return (
      <View>
        <View style={{margin: 10, padding: 10, width: 250}}>
          <SegmentedControlIOS values={['-', '+']}  momentary={true} />
        </View>
      </View>
    );
  }
});

var GLOBALS  = require('./Globals');

var Page = React.createClass({

  render: function(){
    return (
      <ScrollView>
        <View style={[styles.tabContent]}>
          <Text style={styles.tabText}>RX PAGE</Text>
          <Text style={styles.tabText}></Text>
          <View style={{
            width: 250,
            height: 35,
            flexDirection: 'row',
            backgroundColor: '#fff',
            padding: 5}}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              onChangeText={this.goToSearchResults}
              placeholder="Search..."
              style={{
                flex: 1,
                height:25,
                flexDirection: 'row',
                backgroundColor: 'white',
                borderColor: '#cccccc',
                borderRadius: 3,
                borderWidth: 1,
                paddingLeft: 8,
              }}
              testID="explorer_search"
              value={this.state}
            />
        </View>

        <BasicSegmentedControlExample />

        <View style={{backgroundColor:'brown', padding:10, borderRadius: 10}}>
          
            <Text style={{}}>I'm not a button!!! {GLOBALS.menuMap.myrx.count}</Text>
          
        </View>
      </View>
    </ScrollView>
    );
  }

});


var styles = StyleSheet.create({
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'black',
    marginTop: 70,
  },
});


module.exports = Page;