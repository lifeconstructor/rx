'use strict';


var {

    Dimensions

    } = React;

var GLOBALS = require('./Globals');


var window = Dimensions.get('window');
var TimerMixin = require('react-timer-mixin');
var invariant = require('invariant');



var {height, width} = Dimensions.get('window');


// Main Component
var SearchPage = React.createClass({

    mixins: [TimerMixin],

    timeoutID: (null:any),


getInitialState: function () {

    var URL = CONFIG.API_URL + 'api/get-from-history?access-token=' + GLOBALS.API_KEY;

    fetch(URL)
        .then((response) => response.json())
        .catch((error) => {

        })
        .then((responseData) => {

            this.setState({
                searchResults: this.getDataSource1(responseData)
            });

        })
        .done();

    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    return {
        isLoading: false,
        isLoadingTail: false,
        searchResults: ds.cloneWithRows([]),
        filter: '',
        queryNumber: 0,
        www: 0,
        searchBarWidth: 0,
        showMore: 0,
        showMore2: 1,
        test: 30,
        data: [],
        drugsAutocomplete: [],
        selectedDrug: ''

    };
},

componentWillReceiveProps: function () {

    var URL = CONFIG.API_URL + 'api/get-from-history?access-token=' + GLOBALS.API_KEY;

    fetch(URL)
        .then((response) => response.json())
        .catch((error) => {

        })
        .then((responseData) => {

            this.setState({
                searchResults: this.getDataSource1(responseData)
            });

        })
        .done();
},

removeAll: function() {


    var URL = CONFIG.API_URL + CONFIG.history_remove;

    fetch(URL, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + GLOBALS.API_KEY
        },
        body: JSON.stringify({
            removeAll : true,
            items     : []
        })
    }).then((response) => response.json())
        .then((responseText) => {

            this.setState({
                searchResults: this.getDataSource1([])
            });

        })
        .catch((error) => {

            console.log(error);

        });


},

removeHistoryItem: function(id){

    var URL = CONFIG.API_URL + CONFIG.history_remove;

    fetch(URL, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + GLOBALS.API_KEY
        },
        body: JSON.stringify({
            removeAll : false,
            items     : [id]
        })
    }).then((response) => response.json())
        .then((responseText) => {

            var URL = CONFIG.API_URL + 'api/get-from-history?access-token=' + GLOBALS.API_KEY;

            fetch(URL)
                .then((response) => response.json())
                .catch((error) => {

                })
                .then((responseData) => {

                    this.setState({
                        searchResults: this.getDataSource1(responseData)
                    });

                })
                .done();

        })
        .catch((error) => {

            console.log(error);

        });

},


getDataSource1: function (test:Array<any>):ListView.DataSource {
    return this.state.searchResults.cloneWithRows(test);
},




measureMainComponent: function () {
    this.refs.MainComponent.measure((ox, oy, width, height) => {
        this.setState({
            www: width,
            hhh: height
        });
    });
},


render: function () {

    var test = (this.state.searchResults._cachedRowCount);
    var listOfDrugs1 = (test) ?
        <ListView
            dataSource={this.state.searchResults}
            renderRow={(rowData) =>


                                <View style={{
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    backgroundColor: 'white',
                                    margin: 2,
                                    borderRadius:5}}
                                >

                                    <Text style={{
                                        flex: 0.7,
                                        margin: 8,
                                        fontSize: 16}}
                                    >
                                        {rowData.product_name}
                                    </Text>

                                    <TouchableHighlight
                                        removeHistoryItem={this.removeHistoryItem}
                                        productData = {rowData}
                                        onPress={
                                            function(){
                                                this.removeHistoryItem(rowData.id)
                                            }
                                        }
                                        style={{
                                            flex: 0.3,
                                            borderWidth: 1,
                                            borderRadius: 5,
                                            borderColor: 'black',
                                            backgroundColor: '#d83b34',
                                            alignItems: 'center',
                                            marginTop: 10,
                                            marginHorizontal: 10}}
                                    >
                                        <Text style={{color: 'white'}}>Remove</Text>
                                    </TouchableHighlight>

                                </View>


                            }
        />
        :
        <View style={{borderWidth: 1, borderColor: 'gray', backgroundColor: 'white'}}>
            <View style={{flex: 1, marginTop: 10, marginBottom: 10}}>
                <Text
                    style={{height: 20, marginLeft: 20, fontSize: 18}}
                >
                    No Recent Searches
                </Text>
            </View>
        </View>

    return (

        <View onLayout={this.measureMainComponent.bind(null,this)} ref="MainComponent">

            <ScrollView style={{width: width, height: height-125, marginTop: 64}}>


                <View style={{flexDirection: 'row', borderWidth: 1, borderColor: 'gray', backgroundColor: 'white'}}>
                    <View style={{flex: 0.6, marginTop: 10, marginBottom: 10}}>
                        <Text
                            style={{height: 20, marginLeft: 20, fontSize: 18}}
                        >
                            Recent Searches
                        </Text>
                    </View>
                    <TouchableHighlight
                        onPress={this.removeAll}
                        style={{
                                flex: 0.4,
                                borderRadius: 10,
                                marginTop: 5,
                                marginHorizontal: 5,
                                alignItems: 'center',
                                backgroundColor: '#357bb5',
                                padding: 5
                                }}
                    >
                        <Text style={{color: 'white'}}>
                            Remove All
                        </Text>
                    </TouchableHighlight>

                </View>

                {listOfDrugs1}

            </ScrollView>
        </View>
    );
}
});

var styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 50,

    },

    innerContainer: {
        borderRadius: 10,
        alignItems: 'center',
    },
    row: {
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingTop: 5,
    },
    cellBorder: {
        backgroundColor: '#d1d1d1',
        height: 1,
        marginLeft: 4,
    },
    profpic: {
        width: 50,
        height: 50,
    },
    title: {
        fontSize: 16,
        marginBottom: 5,
    },
    subtitle: {
        fontSize: 16,
        marginBottom: 8,
    },
    textcontainer: {
        flex: 0.88,
        paddingLeft: 19,
    },
    blanktext: {
        padding: 10,
        fontSize: 20,
    },
    showContent: {height: 0},
    showContentHidden: {padding: 10},
});

module.exports = SearchPage;