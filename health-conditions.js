'use strict';

var React = require('react-native');
var {
    AppRegistry,
    Image,
    ListView,
    StyleSheet,
    Text,
    View,
    } = React;

var GLOBALS = require('./Globals');
var TimerMixin = require('react-timer-mixin');
var invariant = require('invariant');
var dismissKeyboard = require('dismissKeyboard');

// URL
var API_KEY = '';
var API_URL = 'http://stage-rxcout.pilgrimconsulting.com/backend/web/api/health-conditions/?query=&page_limit=100&page_number=0&apikey=7MNqWkQAss4Pn8Dw9WTYaYNbnVREEFj5QJV27SmALUwGENhvr9';
var PAGE_SIZE = 25;
var PARAMS = '';
var REQUEST_URL = API_URL + PARAMS;

var HealthConditions = React.createClass({

    mixins: [TimerMixin],
    timeoutID: (null: any),
    totalRows: 0,

    getInitialState: function() {
        return {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            loaded: false,
            showMore2: this.props.showMore2,
        };
    },

    fetchData: function() {
        fetch(REQUEST_URL)
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.result),
                    loaded: true,
                });
                this.totalRows = responseData.result.length;
            })
            .done();
    },

    componentDidMount: function() {
        this.clearTimeout(this.timeoutID);
        this.timeoutID = this.setTimeout(() => this.fetchData(), 0);
    },

    renderLoadingView: function() {
        return (
            <View style={styles.container}>
                <Text>Loading . . .</Text>
            </View>
        );
    },

    keepId: function(id){
        GLOBALS.menuMap.search.currentHealthId = 2;
    },

    renderHealthCondition: function(result) {
        return (
            <TouchableHighlight onPress={this.props.showInnerHealthCondition.bind(null, result)}>
                <View style={[styles.row, {paddingLeft: 19, backgroundColor: '#F5FCFF'}]}>
                    <Text style={{fontSize: 16,marginBottom: 7, color: '#0000A0'}}>{result.title}</Text>
                </View>
            </TouchableHighlight>
        );
    },

    render: function() {
        if (!this.state.loaded) {
            // Loading Bar (call: renderLoadingView)
        }

        var heightOfOneRow = 21;
        var totalRows = this.totalRows * heightOfOneRow;

        return (
            <View>
            <ScrollView style={[this.props.showMore2==1 ?{height: totalRows}:{height: 0}, {backgroundColor: '#F5FCFF'}]}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderHealthCondition}
                    style={[styles.listView]}
                />
            </ScrollView>
                </View>

        );
    },
});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
    row: {
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingTop: 5,
    },
    cellBorder: {
        backgroundColor: '#d1d1d1',
        height: 1,
        marginLeft: 4,
    },
    rightContainer: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
    },
    year: {
        textAlign: 'center',
    },
    thumbnail: {
        width: 53,
        height: 81,
    },
    listView: {
        backgroundColor: '#F5FCFF',
    },
});



module.exports = HealthConditions;