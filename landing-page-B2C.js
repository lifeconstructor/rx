'use strict';

var CONFIG = require('./config/config');

var ga = this.ga = null;

var AutoComplete = require('react-native-autocomplete');


var LandingPage = React.createClass({

    getInitialState: function() {

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return {
            isLoading: false,
            isLoadingTail: false,
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            searchResults: ds.cloneWithRows([]),
            filter: '',
            queryNumber: 0,
            www: 0,
            searchBarWidth: 0,
            showMore: 0,
            showMore2: 1,
            test: 30,
            data: [],
            drugsAutocomplete: [],
            selectedDrug: '',
            keyBoardHeight: 0
        };
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this));
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this));

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Landing Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);
    },

    keyboardWillShow (e) {
        console.log('show');
        this.setState({keyBoardHeight: e.endCoordinates.height})
    },

    keyboardWillHide (e) {
        console.log('hide');
        this.setState({keyBoardHeight: 0});

        this.refs._scrollView.scrollTo({x:0, y:0, animated:true});
    },

    componentWillReceiveProps (e) {
        /*this.setState({keyBoardHeight: 0})*/

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);

    },

    onTyping: function (text) {

        this.setState({drugName: text});

        var URL = CONFIG.API_URL + 'api/autocomplete-product' + '?drug_name=' + this.state.drugName;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {


                var drugs = responseData.filter(function (drug) {

                    return drug.PROPRIETARYNAME.toLowerCase().startsWith(text.toLowerCase())

                }).map(function (drug) {

                    var Tier = '';



                    switch(Math.floor((Math.random() * 3) + 1)) {
                        case 1:
                            Tier = ' (NF)';
                            break;
                        case 2:
                            Tier = ' (Tier 1)';
                            break;
                        default:
                            Tier = ' (Tier 2)';
                    }

                    return drug.PROPRIETARYNAME /*+ Tier*/;

                });

                this.setState({
                    drugsAutocomplete: drugs,
                    data: responseData
                });
            })
            .done();

    },



    measureMainComponent: function() {
        this.refs.MainComponent.measure((ox, oy, width, height) => {
            this.setState({
                www: width,
                hhh: height
            });
        });
    },

    login: function(){


        this.props.route.login();
    },


    endEditing: function(){

        console.log('123');

    },

    render: function() {
        return (
            <View style={{flex: 1}}
                  onLayout={this.measureMainComponent.bind(null,this)}  ref="MainComponent">
                <ScrollView
                    style={{width: this.state.www, height: this.state.hhh, marginBottom: this.state.keyBoardHeight}}
                    ref='_scrollView'
                >
                    <Image
                        source={require('./img/home-background.png')}
                        style={{width: this.state.www, height: this.state.hhh}}
                    >
                        <View style={styles.container}>
                            <View style={{flexDirection: 'row', width: 249, height: 101, marginBottom:30}}>
                                <Image
                                    source={require('./img/logo.png')}
                                    style={{
                                            flex: 1,
                                            alignSelf: 'stretch',
                                            width: 249,
                                            height: 101
                                            }}
                                />
                            </View>


                            <AutoComplete
                                ref={component => this._textInput = component}
                                onTyping={this.onTyping}
                                onSelect={(e) => {this.props.route.selectDrug(e);}}
                                onBlur={() => {}}
                                onFocus={() => {}}
                                onSubmitEditing={(e) => {}}
                                onEndEditing={(e) => {}}

                                suggestions={this.state.drugsAutocomplete}

                                placeholder='Search Drug'
                                style={styles.autocomplete}
                                clearButtonMode='always'
                                returnKeyType='search'
                                textAlign='center'
                                clearTextOnFocus={true}

                                maximumNumberOfAutoCompleteRows={6}
                                applyBoldEffectToAutoCompleteSuggestions={true}
                                reverseAutoCompleteSuggestionsBoldEffect={true}
                                showTextFieldDropShadowWhenAutoCompleteTableIsOpen={false}
                                autoCompleteTableViewHidden={false}

                                autoCompleteTableBorderColor='lightblue'
                                autoCompleteTableBackgroundColor='azure'
                                autoCompleteTableCornerRadius={10}
                                autoCompleteTableBorderWidth={1}

                                autoCompleteRowHeight={35}

                                autoCompleteFontSize={15}
                                autoCompleteRegularFontName='Helvetica Neue'
                                autoCompleteBoldFontName='Helvetica Bold'
                                autoCompleteTableCellTextColor={'black'}
                            />

                            <View style={styles.buttonWrapper}>
                                <TouchableHighlight
                                    onPress={this.login}
                                    style={styles.buttons}
                                >
                                    <Text
                                        style={[styles.buttonText, {marginLeft: 41, marginRight: 41}]}
                                    >
                                        Member Login
                                    </Text>
                                </TouchableHighlight>
                            </View>
                            {/*<View style={styles.buttonWrapper}>
                                <TouchableHighlight
                                    onPress={this.props.route.findPharmacy}
                                    style={styles.buttons}

                                >
                                    <Text
                                        style={styles.buttonText}
                                    >
                                        Find a Pharmacy
                                    </Text>
                                </TouchableHighlight>
                            </View>*/}
                            <View style={styles.buttonWrapper}>
                                <TouchableHighlight
                                    onPress={this.props.route.signUp}
                                    style={[styles.buttons, {backgroundColor: null}]}
                                >
                                    <Text
                                        style={{backgroundColor: null}}
                                    >
                                        * Sign up *
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </Image>
                </ScrollView>
            </View>


        );
    }

});

var styles = StyleSheet.create({
    autocomplete: {
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginLeft: 40,
        marginRight: 40,
        marginBottom: 20,
        borderRadius: 5
    },
    backgroundImage: {
        flex: 1,
        alignSelf: 'stretch'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',

    },
    buttonWrapper: {
        flexDirection: 'column'
    },
    buttons: {
        flex: 1,
        backgroundColor: '#357bb5',
        borderRadius: 5,
        margin: 5
    },
    buttonText :{
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }
});

module.exports = LandingPage;
