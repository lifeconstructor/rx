'use strict';

var React = require('react-native');
var {

    Image,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ListView,
    Platform

    } = React;

var GLOBALS = require('./Globals');

var SePage4 = require('./map-screen');

var pharmaciesList = [];
var pharmaciesListWithPrices = [];

var genericName = '';


var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


var MovieScreen = React.createClass({

    getInitialState: function() {

        pharmaciesList = GLOBALS.pharmaciesData;
        pharmaciesListWithPrices = [];
        var ndcId = '';
        var daysSupply = this.props.route.daysSupply == 'Less than 3 months' ? 0:90;
        var quantity = this.props.route.quantity;

        var strength = this.props.route.strength;
        var dosageForm = this.props.route.dosageForm;

        this.props.route.DRUGS.forEach(function logArrayElements(element, index, array) {

            if (element.strength.description == strength && element.dosageForm == dosageForm){
                ndcId = element.generic.id;
                genericName = element.generic.name;
            }
        });

        var Link = 'https://dplu-preview.data-rx.com/api/v1/drug-prices?generic:id=' + ndcId +
            '&days-supply=' + daysSupply +
            '&quantity=' + quantity;

        if (Array.isArray(pharmaciesList)){
            pharmaciesList.forEach(function logArrayElements(element, index, array) {

                Link = Link + '&pharmacy:npi[' + index + ']=' + element.npi;

            });

            fetch(Link, {
                method: 'GET',
                headers: {
                    'Host': 'dplu-preview.data-rx.com',
                    'Connection': 'keep-alive',
                    'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                    'Referer': 'https://dplu-preview.data-rx.com/',
                    'Accept-Encoding': 'gzip, deflate, sdch',
                    'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
                }
            }).then((response) => response.json())
                .catch((error) => {

                    console.log(error);

                })
                .then((responseData) => {

                    /*console.log(responseData);*/

                    this.setState({
                        reData: responseData
                    });



                    pharmaciesList.forEach(function(element, index, array) {

                        pharmaciesListWithPrices.push(element);

                        responseData.forEach(function(element, index, array) {

                            if (element.pharmacy.npi == pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].npi){

                                pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].genericPrice = element.genericPrice;
                                pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].brandPrice = element.brandPrice;

                            }

                        });

                    });

                    this.setState({
                        dataSource: ds.cloneWithRows(pharmaciesListWithPrices)
                    });

                })
                .done();



            var URL_for_coords_by_zip = 'http://maps.googleapis.com/maps/api/geocode/json?address=' + GLOBALS.currentZip + '&language=en';


            fetch(URL_for_coords_by_zip)
                .then((response) => response.json())
                .catch((error) => {


                })
                .then((responseData) => {
                    if (responseData.results){
                        this.setState({

                            zipLat: responseData.results[0].geometry.location.lat,
                            zipLng: responseData.results[0].geometry.location.lng
                        });
                    }
                    console.log(responseData);
                })
                .done();



        }

        return {
            dataSource: ds.cloneWithRows(pharmaciesList),
            drugTitle: GLOBALS.QUERY,
            reData: ''
        };
    },


    componentDidMount(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});

            },
            (error) => alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
        this.watchID = navigator.geolocation.watchPosition((position) => {
            var lastPosition = JSON.stringify(position);
            this.setState({lastPosition});
        });
    },

    componentWillReceiveProps: function(){

        pharmaciesList = GLOBALS.pharmaciesData;
        pharmaciesListWithPrices = [];

        var ndcId = '';
        var daysSupply = this.props.route.daysSupply == 'Less than 3 months' ? 0:90;
        var quantity = this.props.route.quantity;

        var strength = this.props.route.strength;
        var dosageForm = this.props.route.dosageForm;

        this.props.route.DRUGS.forEach(function logArrayElements(element, index, array) {

            if (element.strength.description == strength && element.dosageForm == dosageForm){
                ndcId = element.generic.id;
                genericName = element.generic.name;
            }
        });

        var Link = 'https://dplu-preview.data-rx.com/api/v1/drug-prices?generic:id=' + ndcId +
            '&days-supply=' + daysSupply +
            '&quantity=' + quantity;

        if (Array.isArray(pharmaciesList)){
            pharmaciesList.forEach(function logArrayElements(element, index, array) {

                Link = Link + '&pharmacy:npi[' + index + ']=' + element.npi;

            });

            fetch(Link, {
                method: 'GET',
                headers: {
                    'Host': 'dplu-preview.data-rx.com',
                    'Connection': 'keep-alive',
                    'Accept': 'application/json; charset=utf-8, application/transit+json; charset=utf-8, application/transit+transit; charset=utf-8, text/plain; charset=utf-8, text/html; charset=utf-8, */*; charset=utf-8',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
                    'Referer': 'https://dplu-preview.data-rx.com/',
                    'Accept-Encoding': 'gzip, deflate, sdch',
                    'Accept-Language': 'uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2,de;q=0.2'
                }
            }).then((response) => response.json())
                .catch((error) => {

                    console.log(error);

                })
                .then((responseData) => {

                    /*console.log(responseData);*/

                    this.setState({
                        reData: responseData
                    });



                    pharmaciesList.forEach(function(element, index, array) {

                        pharmaciesListWithPrices.push(element);

                        responseData.forEach(function(element, index, array) {

                            if (element.pharmacy.npi == pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].npi){

                                pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].genericPrice = element.genericPrice;
                                pharmaciesListWithPrices[pharmaciesListWithPrices.length - 1].brandPrice = element.brandPrice;

                            }

                        });

                    });

                    this.setState({
                        dataSource: ds.cloneWithRows(pharmaciesListWithPrices)
                    });

                })
                .done();



            var URL_for_coords_by_zip = 'http://maps.googleapis.com/maps/api/geocode/json?address=' + GLOBALS.currentZip + '&language=en';


            fetch(URL_for_coords_by_zip)
                .then((response) => response.json())
                .catch((error) => {


                })
                .then((responseData) => {



                    if (responseData.results){



                        this.setState({

                            zipLat: responseData.results[0].geometry.location.lat,
                            zipLng: responseData.results[0].geometry.location.lng
                        });



                    }

                    console.log(responseData);

                })
                .done();



        }



        this.setState({
            dataSource: ds.cloneWithRows(pharmaciesList),
            pharmaciesList: pharmaciesList
        });

    },

    renderRow: function(rowData){
        return(
            <TouchableHighlight
                selectPharmacy={this.selectPharmacy}
                pharmacyData = {rowData}
                onPress={
                    function(){
                        this.selectPharmacy(this.pharmacyData)
                    }
                }
                style={styles.packageButton}
            >
                <View style={{backgroundColor: CONFIG.backgroundCl}}>
                <View style={{ flexDirection: 'row', backgroundColor: 'white', margin: 6, marginHorizontal: 7, borderRadius: 10, padding: 5}}>
                    <Image
                        style={{height:40, width: 40, marginTop: 20}}
                        source={require('./img/bottle.png')}
                    />
                    <View style={{flex:0.7, marginLeft: 10}}>
                        <Text
                            style={{fontSize: 20, marginTop:10,alignItems: 'center', backgroundColor: 'white'}}>
                            {rowData.name}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {rowData.address.line1 + ' ' + rowData.address.state + ' ' + rowData.address.zip}
                        </Text>
                        <Text
                            style={{fontSize: 14, marginTop:5,alignItems: 'center'}}>
                            {Math.round(parseFloat(rowData.distance.miles) * 1000) / 1000} mi
                        </Text>
                    </View>
                    <View style={{flex:0.3, marginLeft: 10}}>
                        <Text
                            style={{fontSize: 18, marginTop:5,alignItems: 'center'}}>

                            {
                                rowData.genericPrice ? 'Generic:':''
                            }

                        </Text>
                        <Text
                            style={{fontSize: 18, marginTop:5,alignItems: 'center', fontWeight: 'bold'}}>
                            {
                                rowData.genericPrice ? '$':''
                            }
                            {rowData.genericPrice}
                        </Text>
                        <Text
                            style={{fontSize: 18, marginTop:5,alignItems: 'center'}}>

                            {
                                rowData.brandPrice ? 'Brand:':''
                            }

                        </Text>
                        <Text
                            style={{fontSize: 18, marginTop:5,alignItems: 'center', fontWeight: 'bold'}}>
                            {
                                rowData.brandPrice ? '$':''
                            }

                            {rowData.brandPrice}
                        </Text>
                    </View>
                </View>
                </View>

            </TouchableHighlight>
        );
    },

    selectPharmacy: function(pharmacyObject){


        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({initialPosition});

                var lat = (this.props.route.userCurentLocation)?position.coords.latitude:this.state.zipLat;

                var lng = (this.props.route.userCurentLocation)?position.coords.longitude:this.state.zipLng;

                var URL_for_polyline = 'https://maps.googleapis.com/maps/api/directions/json?&origin='
                    + lat + ','
                    + lng + '&destination='
                    + pharmacyObject.address.line1 + ' '
                    + pharmacyObject.address.state + ' '
                    + pharmacyObject.address.zip + '&mode=driving&language=en';


                fetch(URL_for_polyline)
                    .then((response) => response.json())
                    .catch((error) => {


                    })
                    .then((responseData) => {

                        if (responseData.status == 'OK'){
                            this.props.navigator.push({
                                title: 'Map',
                                component: SePage4,

                                locationObject: responseData.routes[0].legs[0].end_location,
                                polyline: responseData.routes[0].overview_polyline.points,
                                userCurentLocation: this.props.route.userCurentLocation,
                                zipLat: this.state.zipLat,
                                zipLng: this.state.zipLng,
                            });
                        } else {
                            AlertIOS.alert('Way cannot be calculated.');
                        }
                    })
                    .done();
            });

        /*this.props.navigator.push({
            title: GLOBALS.titleCut(pharmacyObject.name),
            component: SePage4,
            pharmacyObject: pharmacyObject
        });*/

    },

    render: function() {

        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    <View style={[styles.innerView, {margin: 5, backgroundColor: 'white', borderRadius: 5, padding: 5}]}>
                        <Image
                            style={styles.image}
                            source={require('./img/pill.png')}
                        />
                        <View style={styles.productWrapper}>
                            <Text
                                style={styles.titleText}>
                                {this.props.route.drugTitle}
                            </Text>
                            <Text
                                style={{fontSize: 18, marginTop:5,alignItems: 'center'}}>
                                Generic: {genericName}
                            </Text>
                            <Text
                                style={{fontSize: 18, marginTop:5,alignItems: 'center'}}>
                                {this.props.route.quantity + ' ' + this.props.route.dosageForm} {this.props.route.strength}
                            </Text>

                        </View>
                    </View>
                    <View style={[styles.innerView, {margin: 5, backgroundColor: 'white', borderRadius: 5, padding: 5}]}>
                        <Text style={{flex:1, margin: 5, fontSize: 18,}}>
                            NOTICE - THESE ARE THE CASH PRICES.  YOUR OUT OF POCKET COST MAY VARY, DEPENDING UPON COORDINATION OF BENEFITS AND THE DETAILS OF YOUR PLAN
                        </Text>
                    </View>
                    {pharmaciesList.length ?
                        <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow}
                    />
                    :
                        <Text style={{alignSelf: 'center', fontSize: 24, color: 'red', marginTop: 20}}>No Pharmacies Found</Text>
                    }
                </ScrollView>
            </View>
        );
    },
});

var styles = StyleSheet.create({
    container: {
        marginTop: ((Platform.OS === 'ios')?64:55),
        flex:1
    },
    scrollView: {
        flex: 1
    },
    innerView: {
        flexDirection: 'row',
        backgroundColor: GLOBALS.backgroundCl
    },
    image:{
        height:40,
        width: 40,
        margin: 10
    },
    productWrapper: {
        flex:1,
        marginLeft: 10
    },
    titleText: {
        fontSize: 20,
        marginTop:10,
        alignItems: 'center',
        fontWeight: 'bold'
    },
    packageButton: {

    }

});

module.exports = MovieScreen;
