var ga = this.ga = null;

var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

import Spinner from 'react-native-loading-spinner-overlay';

var LoginScreen = React.createClass({

    getInitialState: function() {
        return({
            username        : '',
            password        : '',
            usernameError   : '',
            passwordError   : '',
            keyBoardHeight  : 0,
            isLoading       : false
        });
    },

    onLogin: function(){

        this.setState({
            usernameError   : '',
            passwordError   : '',
            isLoading       : true
        });

        var URL = CONFIG.API_URL + CONFIG.login_url;

        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        }).then((response) => response.json())
            .then((responseText) => {

                this.setState({
                    isLoading: false
                });

                if (responseText.errors){
                    if (responseText.errors.username){
                        this.setState({
                            usernameError: responseText.errors.username[0]
                        });
                    }
                    if (responseText.errors.password){
                        this.setState({
                            passwordError: responseText.errors.password[0]
                        });
                    }
                }

                if (responseText.auth_key){

                    /*this.props.route.hideNavBar();*/
                    if (responseText.is_member || CONFIG.PRODUCTION_MODE == 'B2C'){
                        GLOBALS.API_KEY = responseText.auth_key;

                        this.props.topNavigator.push({
                            title: '',
                            component: Tabs,
                            loginObject: responseText,
                            selectedTab: 'account'
                        });
                    } else {
                        AlertIOS.alert('Login for members only');
                    }


                }

            })
            .catch((error) => {
                this.setState({
                    error: 'Error connecting to server'
                });
            });

    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this));
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this));

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);
    },

    componentWillReceiveProps() {

        let clientId = DeviceInfo.getUniqueID();
        ga = new Analytics(CONFIG.google_analytics_key, clientId, 1, DeviceInfo.getUserAgent());
        var screenView = new GAHits.ScreenView('RxScout', 'Login Screen', CONFIG.ios_app_version, 'com.snb.rxscout');
        ga.send(screenView);

    },

    keyboardWillShow (e) {

        this.setState({keyBoardHeight: e.endCoordinates.height})
    },

    keyboardWillHide (e) {
        this.setState({keyBoardHeight: 0});

        //this.refs._scrollView.scrollTo(200);
    },

    render: function() {

        var content = this.state.isLoading
            ?
                <View style={{ flex: 1 }}>
                    <Spinner visible={true} />
                </View>
            :
                <View style={[styles.container, {marginBottom: this.state.keyBoardHeight, backgroundColor: CONFIG.backgroundCl}]} pointerEvents="auto">



                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Group Number
                    </Text>

                    <TextInput
                        ref='SecondInput'
                        style={styles.input}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        returnKeyType = {"send"}
                        onSubmitEditing={(event) => {this.onLogin()}}
                    />

                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.passwordError}
                    </Text>


                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Member ID
                    </Text>

                    <TextInput
                        style={styles.input}
                        onChangeText={(username) => this.setState({username})}
                        value={this.state.username}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        returnKeyType = {"next"}
                        onSubmitEditing={(event) => {
                                        this.refs.SecondInput.focus();
                                      }}
                    />

                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.usernameError}
                    </Text>

                    <View style={{flexDirection: 'row'}}>
                        <TouchableHighlight
                            onPress={this.onLogin}
                            style={[styles.loginButton, {marginRight: 10}]}
                            ref='test'
                        >
                            <Text style={styles.loginText}>
                                Submit
                            </Text>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={()=>this.props.topNavigator.pop()}
                            style={[styles.loginButton, {marginLeft: 10}]}
                            ref='test'
                        >
                            <Text style={styles.loginText}>
                                Cancel
                            </Text>
                        </TouchableHighlight>
                    </View>



                </View>
           ;

        return (
            <ScrollView ref='_scrollView' keyboardShouldPersistTaps={true} >
                {content}
            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 84
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        fontSize: 20
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white',
        fontSize: 22
    }

});

module.exports = LoginScreen;