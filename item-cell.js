'use strict';

var React = require('react-native');
var {
  Image,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  View
} = React;

var GLOBALS1 = require('./Globals');

var MovieCell = React.createClass({
  
  render: function() {
    
    var TouchableElement = TouchableHighlight;



    return (
      <View>
        <TouchableElement
          onPress={this.props.onSelect}
          onShowUnderlay={this.props.onHighlight}
          onHideUnderlay={this.props.onUnhighlight}>
          <View style={styles.row}>
            <View>
              <Text style={styles.movieTitle} numberOfLines={150}>
                {this.props.drug.PROPRIETARYNAME}

              </Text>
            </View>

          </View>

        </TouchableElement>
      </View>

    );
  }
});

var styles = StyleSheet.create({
  textContainer: {
    flex: 1
  },
  movieTitle: {
    flex: 1,
    fontSize: 16,
    fontWeight: '500',
    marginBottom: 2
  },
  movieYear: {
    color: '#999999',
    fontSize: 12
  },
  row: {
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 5
  },
  cellImage: {
    backgroundColor: '#dddddd',
    height: 30,
    marginRight: 10,
    width: 60
  },
});

module.exports = MovieCell;
