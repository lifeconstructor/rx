'use strict';

var React = require('react-native');
var {
    Image,
    PixelRatio,
    ScrollView,
    StyleSheet,
    Text,
    View,
    Modal,
    Platform
    } = React;

var GLOBALS = require('./Globals');
var SePage4 = require('./search-page-5');


var Button = React.createClass({

    getInitialState() {
        return {
            active: false,
        };
    },

    _onHighlight() {
        this.setState({active: true});
    },

    _onUnhighlight() {
        this.setState({active: false});
    },

    render() {
        var colorStyle = {
            color: this.state.active ? '#fff' : '#000',
        };
        return (
            <View style={this.props.style}>
                <TouchableHighlight
                    onHideUnderlay={this._onUnhighlight}
                    onPress={this.props.onPress}
                    onShowUnderlay={this._onHighlight}
                    underlayColor="#a9d9d4">
                    <Text style={{color:'white', margin:10}}>{this.props.children}</Text>
                </TouchableHighlight>
            </View>
        );
    }
});

var MovieScreen = React.createClass({
    getInitialState: function() {

        return {

            modalVisibleCoupon: false,
            animated: true,
            transparent: true,
        };
    },
    _setModalVisibleCoupon(visible) {
        this.setState({
            modalVisibleCoupon: visible
        });
    },

    selectLocation: function(locationId: Number, locationTitle: String) {
        if (Platform.OS === 'ios') {
            GLOBALS.menuMap.search.count++;
            GLOBALS.menuMap.search.currentLocationId = locationId;
            GLOBALS.menuMap.search.titles[GLOBALS.menuMap.search.count] = locationTitle;
            this.props.navigator.push({
                title: GLOBALS.titleCut(locationTitle),
                component: SePage4
            });
        }
    },

    _setModalVisibleRotate() {
        if (this.state.modalVisibleCoupon) {
            this.setState({
                modalVisibleCoupon: false
            });
            this.setState({
                modalVisibleCoupon: true
            });
        }
    },

    render: function() {



        return (
            <ScrollView onLayout={()=>this._setModalVisibleRotate.call(this)}>

                <Modal
                    animated={this.state.animated}
                    transparent={this.state.transparent}
                    visible={this.state.modalVisibleCoupon}
                >

                    <View style={{
                        flex:1,
                        flexDirection:'column'}}>
                        <View style={{
                            backgroundColor: 'gray',
                            flex:0.7,
                            alignItems: 'center'}}>
                            <Image
                                style={{height:200, width:200, margin: 30}}
                                source={require('./img/qr-code.png')}
                            />
                        </View>
                        <View style={{
                            backgroundColor: 'gray',
                            flex:0.1,
                            alignItems: 'center'}}>
                            <Text>68OYH-JBLJ-YJU5-3KI9</Text>
                        </View>
                        <View style={{
                            flex:0.2,
                            backgroundColor: 'gray',
                            alignItems: 'center'}}>
                            <Button
                                onPress={this._setModalVisibleCoupon.bind(this, false)}
                                style={{margin:10, backgroundColor: 'green', borderRadius: 10}}>
                                <Text style={{margin: 8, fontSize: 24}}>Save Voucher</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>

                <View style={{marginTop: 64, flexDirection: 'row', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <View style={{marginLeft: 10}}>
                        <Text
                            style={{fontSize: 24, marginTop: 10}}
                        >
                            {GLOBALS.QUERY}
                        </Text>
                        <Text>{this.props.route.quantity} {this.props.route.dose}</Text>
                        <Text style={{padding:10, fontSize: 20, color:'gray'}}>BEST PRICE</Text>
                    </View>
                </View>



                <View
                    style={{marginLeft: 20}}
                >
                    <Text style={{fontSize: 20, marginTop: 20}}>
                        PRICE WITH COUPON
                    </Text>
                    <View style={{margin: 20}}>
                        <View style={{padding: 8}}>
                            <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                                Prescription for 30 tablets to be taken over
                            </Text>
                        </View>
                        <View style={{borderTopWidth: 1, borderTopColor: 'lightgray', padding:8, flexDirection: 'row'}}>
                            <Text style={{fontSize: 15, flex: 0.7}}>
                                84+ days
                            </Text>
                            <Text style={{fontSize: 15, flex: 0.3}}>
                                $11.35
                            </Text>
                        </View>
                        <View style={{borderTopWidth: 1, borderTopColor: 'lightgray', padding:8, flexDirection: 'row'}}>
                            <Text style={{fontSize: 15, flex: 0.7}}>
                                30 days
                            </Text>
                            <Text style={{fontSize: 15, flex: 0.3}}>
                                $12.60
                            </Text>
                        </View>

                    </View>

                    <TouchableHighlight
                        onPress={()=>this._setModalVisibleCoupon(true)}
                        style={{
                            backgroundColor: 'lightgreen',
                            flex:1,
                            margin: 30,
                            marginTop: 10,
                            padding: 10,
                            borderRadius: 5,
                            alignItems: 'center'
                            }}>
                        <Text style={{color:'white', fontSize: 20}}>View Free Coupon</Text>
                    </TouchableHighlight>
                </View>

                <View style={{flexDirection: 'column', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <View style={{marginLeft: 10}}>
                        <Text style={{padding:10, fontSize: 20, color:'gray'}}>ADDITIONAL PRICE</Text>
                    </View>
                    <View style={{backgroundColor: 'white'}}>
                        <Text style={{padding:10, fontSize: 15, marginLeft: 30}}>CASH $107.00</Text>
                    </View>
                </View>

                <View style={{flexDirection: 'column', backgroundColor: 'lightgray', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <View style={{marginLeft: 10}}>
                        <Text style={{padding:10, fontSize: 20, color:'gray'}}>2 NEARBY LOCATIONS</Text>
                    </View>
                    <TouchableHighlight
                        onPress={()=>this.selectLocation(1, '2020 Malony Ln')}>
                        <View style={{backgroundColor: 'white'}}>
                            <Text style={{padding:10, fontSize: 15, marginLeft: 30, color: '#3366FF'}}>Walmart Supercenter</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={()=>this.selectLocation(2, '1203 Murfreesboro Rd')}>
                        <View style={{backgroundColor: 'white', marginBottom:43}}>
                            <Text style={{padding:10, fontSize: 15, marginLeft: 30, color: '#3366FF'}}>Walmart Neighborhood</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        );
    },
});



var styles = StyleSheet.create({
    contentContainer: {
        padding: 10,
    },
    rightPane: {
        justifyContent: 'space-between',
        flex: 1,
    },
    movieTitle: {
        flex: 1,
        fontSize: 16,
        fontWeight: '500',
    },
    rating: {
        marginTop: 10,
    },
    ratingTitle: {
        fontSize: 14,
    },
    ratingValue: {
        fontSize: 28,
        fontWeight: '500',
    },
    mpaaWrapper: {
        alignSelf: 'flex-start',
        borderColor: 'black',
        borderWidth: 1,
        paddingHorizontal: 3,
        marginVertical: 5,
    },
    mpaaText: {
        fontFamily: 'Palatino',
        fontSize: 13,
        fontWeight: '500',
    },
    mainSection: {
        flexDirection: 'row',
    },
    detailsImage: {
        width: 134,
        height: 200,
        backgroundColor: '#eaeaea',
        marginRight: 10,
    },
    separator: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        height: 1 / PixelRatio.get(),
        marginVertical: 10,
    },
    castTitle: {
        fontWeight: '500',
        marginBottom: 3,
    },
    castActor: {
        marginLeft: 2,
    },
});

module.exports = MovieScreen;