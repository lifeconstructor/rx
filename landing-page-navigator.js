'use strict';

var GLOBALS     = require('./Globals');



var LandingPage = CONFIG.PRODUCTION_MODE=="PBM"?require('./landing-page-PBM'):require('./landing-page-B2C');
var TabsNL       = require('./TabsNL');
var Search      = require('./not-logedin-search');
var Map         = require('./map-screen');


var NavigationBarRouteMapperLanding = {

    LeftButton: function(route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }
        var previousRoute = navState.routeStack[index - 1];

        return (
            <TouchableOpacity
                onPress={function(){
                    navigator.pop();
                    if (index === 1){
                        route.hideNavBar();
                    }
                }}
                style={styles.navBarLeftButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                    {previousRoute.title}
                </Text>
            </TouchableOpacity>
        );
    },

    RightButton: function(route, navigator, index, navState) {
        return (
            <TouchableOpacity
                onPress={() => navigator.push(newRandomRoute())}
                style={styles.navBarRightButton}>
                <Text style={[styles.navBarText, styles.navBarButtonText]}>
                </Text>
            </TouchableOpacity>
        );
    },

    Title: function(route, navigator, index, navState) {
        return (
            <Text style={[styles.navBarText, styles.navBarTitleText]}>
                {route.title}
            </Text>
        );
    },

};


var landRoute = React.createClass({

    getInitialState(){
        return({
            hideNavBar: true,
            username        : '',
            password        : ''
        });
    },

    hideNavBar: function(){
        this.setState({
            hideNavBar: true
        });
    },

    showNavBar: function(){
        this.setState({
            hideNavBar: false
        });
    },

    pop: function() {
        this.navigator.pop();
    },

    login: function(){
        this.navigator.push({
            title: '',
            component: TabsNL,
            hideNavBar: this.hideNavBar,
            showNavBar: this.showNavBar,
            selectedTab: 'myrx'
        });
    },

    signUp: function(){
        this.navigator.push({
            title: '',
            component: TabsNL,
            hideNavBar: this.hideNavBar,
            showNavBar: this.showNavBar,
            selectedTab: 'reminders'
        });
    },

    searchDrugs: function(){
        this.showNavBar();
        this.navigator.push({
            title: 'Search',
            component: Search,
            hideNavBar: this.hideNavBar,
            showNavBar: this.showNavBar
        });
    },

    renderScene: function(route, navigator) {
        var Component = route.component;
        return (
            <View style={styles.container}>
                <Component
                    route={route}
                    navigator={navigator}
                    topNavigator={navigator}
                />
            </View>
        )
    },

    selectDrug: function(drugTitle) {

        /*this.showNavBar();*/

        drugTitle = drugTitle.replace(" (NF)", "");
        drugTitle = drugTitle.replace(" (Tier 1)", "");
        drugTitle = drugTitle.replace(" (Tier 2)", "");

        this.setState({selectedDrug: drugTitle});


        for (var i in this.state.data) {
            if (this.state.data[i].PROPRIETARYNAME == this.state.selectedDrug){
                this.setState({
                    productId: this.state.data[i].id
                });
            }
        }

        var URL = CONFIG.API_URL + 'api/products' + '?query=' + this.state.selectedDrug + '&page_limit=100&page_number=0&apikey=' + GLOBALS.API_KEY;


        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData.result)
                    GLOBALS.DATA = responseData.result;


                if (drugTitle != "No Recent Searches"){

                    GLOBALS.QUERY = drugTitle;



                    this.navigator.push({
                        title: GLOBALS.titleCut(drugTitle),
                        component: TabsNL,
                        drug: responseData.result[0],
                        hideNavBar: this.hideNavBar,
                        showNavBar: this.showNavBar,
                        selectedTab: 'search',
                        DATA: responseData.result
                    });
                }
            })
            .done();
    },



    render: function() {
        if ( this.state.hideNavBar === true ) {
            return (
                <Navigator
                    ref={(navigator) => { this.navigator = navigator; }}
                    renderScene={this.renderScene}
                    tintColor='#D6573D'
                    barTintColor='#FFFFFD'
                    titleTextColor='#D6573D'
                    navigationBarHidden={true}
                    initialRoute={{
                      title: 'Home',
                      component: LandingPage,
                      login: this.login,
                      signUp: this.signUp,
                      searchDrugs: this.searchDrugs,
                      hideNavBar: this.hideNavBar,
                      showNavBar: this.showNavBar,
                      findPharmacy: this.findPharmacy,
                      selectDrug: this.selectDrug,
                      pop: this.pop
                }}
                />
            );
        }
        return (
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='#D6573D'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBarHidden={true}
                navigationBar={
                        <Navigator.NavigationBar
                          routeMapper={NavigationBarRouteMapperLanding}
                          style={styles.navBar}
                        />
                    }
                initialRoute={{
                  title: 'Home',
                  component: LandingPage,
                  login: this.login,
                  hideNavBar: this.hideNavBar,
                  showNavBar: this.showNavBar,
                  pop: this.pop
                }}
            />
        );
    }

});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    messageText: {
        fontSize: 17,
        fontWeight: '500',
        padding: 15,
        marginTop: 50,
        marginLeft: 15,
    },

    button: {
        backgroundColor: 'white',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#CDCDCD',
    },

    buttonText: {
        fontSize: 17,
        fontWeight: '500',
    },

    navBar: {
        backgroundColor: GLOBALS.COLORS.orange,
    },

    navBarText: {
        fontSize: 16,
        marginVertical: 10,
    },

    navBarTitleText: {
        color: 'black',
        fontWeight: '500',
        marginVertical: 9,
    },

    navBarLeftButton: {
        paddingLeft: 10,
    },

    navBarRightButton: {
        paddingRight: 10,
    },

    navBarButtonText: {
        color: 'gray',
    },

    scene: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#EAEAEA',
    },
});

module.exports = landRoute;
