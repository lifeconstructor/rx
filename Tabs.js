'use strict';

var Globals = require('./app/globals').load();

var {
    View,
    Navigator
    } = React;

var Main = require('./Main');

var Tabs = React.createClass({

    renderScene: function(route, navigator) {
        var Component = route.component;
        return (
            <View style={styles.container}>
                <Component
                    route={route}
                    navigator={navigator}
                    topNavigator={this.props.navigator}
                    loginObject={this.props.route.loginObject}
                />
            </View>
        )
    },

    render: function() {
        return (
            <Navigator
                ref={(navigator) => { this.navigator = navigator; }}
                renderScene={this.renderScene}
                tintColor='#D6573D'
                barTintColor='#FFFFFD'
                titleTextColor='#D6573D'
                navigationBarHidden={true}
                initialRoute={{
                  title: '',
                  component: Main
                }}
            />
        );
    }

});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
});



module.exports = Tabs;
