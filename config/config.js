var DeviceInfo = require('react-native-device-info');
var IOS_v = DeviceInfo.getReadableVersion();

exports.CONFIG = {

    mode                    : 'dev',// 'dev', 'prod'

    PRODUCTION_MODE         : 'PBM', //'B2C' or 'PBM'
    OWNER_NAME              : 'RxPreferred',// 'RxScout'

    ios_app_version         : IOS_v,
    android_app_version     : '0.0.0',

    URL_v1                  : '',
    URL_v2                  : 'http://v2-0-rxscout.pilgrimconsulting.com/backend/web/',
    URL_v3                  : 'http://v3-0-rxscout.pilgrimconsulting.com/backend/',


    API_URL                 : 'http://v2-0-rxscout.pilgrimconsulting.com/backend/',


    login_url               : 'api-auth/login',
    sign_up_url             : 'api-auth/sign-up',
    logout_url              : 'api-auth/logout?access-token=ACCESS_TOKEN',
    drugs_url               : '',
    autocomplete_url        : '',
    recent_searches_add     : 'backend/web/api/add-to-history',
    recent_searches_get     : 'backend/web/api/get-from-history',
    history_remove          : 'api/history-remove',
    user_update_get         : 'api-auth/user-update?access-token=',
    user_update_post        : 'api-auth/user-update',
    info                    : 'api/info?content=',
    update_password         : 'api-auth/update-password',
    update_phone            : 'api-auth/update-phone-number',



    google_analytics_key    : 'UA-75829701-1',

    backgroundCl : '#21a8de'
};