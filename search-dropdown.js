var SearchPage = React.createClass({
    getInitialState: function(){
        return {
            query: ""
        }
    },

    componentWillMount: function(){
        DB.search_query.get({actor: "tweet"}, function(data){
            var query_data = data[0]
            me.setState({query: query_data.query})
        })
    },

    render: function(){
        var me = this;
        DB.search_query.get({actor: "tweet"}, function(data){
            var query_data = data[0]
            me.setState({query: query_data.query})
        })
        return (
            <Text>
                {me.state.query}
            </Text>
        )
    }
});

module.exports = SearchPage

