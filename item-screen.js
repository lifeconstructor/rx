'use strict';

var React = require('react-native');
var {
  Image,
  PixelRatio,
  ScrollView,
  StyleSheet,
  Text,
  View,
  ListView,
  Platform,
  TouchableHightlight
} = React;

var GLOBALS = require('./Globals');
var LocationScreen = require('./location-screen');

var MovieScreen = React.createClass({
  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      dataSource: ds.cloneWithRows(
        GLOBALS.medicines.drugs.filter(
          function(obj){
            if (obj.id == GLOBALS.menuMap.myrx.currentDrugId) {
              return true;
            } else {
              return false;
            }
          })[0].locations
      ),
    };
  },


  selectLocation: function() {
    if (Platform.OS === 'ios') {
      GLOBALS.menuMap.myrx.count++;
      GLOBALS.menuMap.myrx.titles[GLOBALS.menuMap.myrx.count] = 'Drug Locations';
      this.props.navigator.push({
        title: GLOBALS.titleCut('Drug Locations'),
        component: LocationScreen,
        passProps: {},
      });
    } else {
      dismissKeyboard();
      this.props.navigator.push({
        title: GLOBALS.titleCut(drug.title),
        name: 'drug',
        drug: drug
      });
    }
  },

  render: function() {
    var Drug = GLOBALS.medicines.drugs.filter(
        function(obj){
          if (obj.id == GLOBALS.menuMap.myrx.currentDrugId) {
            return true;
          } else {
            return false;
          }
        })[0];



    return (
        <View style={{marginTop: 45}}>

          <Text style={{padding:10}}>Pharmacies near 'Current Location' ...</Text>

          <ScrollView>

            <TouchableHighlight
                onPress={()=>this.selectLocation()}
            >
            <View style={{flexDirection: 'row', backgroundColor: 'white'}}>
              <Image
                  style={{height:40, width: 40, margin: 10}}
                  source={require('./pill.png')}
              />
              <View style={{marginLeft: 10}}>
                <Text>{Drug.title && Drug.title[0].toUpperCase() + Drug.title.slice(1)}</Text>
                <Text>{Drug.quantity}</Text>
                <Text>{Drug.dose}</Text>
              </View>
            </View>
            </TouchableHighlight>


            <View style={{padding: 10}}>
              <Text style={{fontWeight: "bold"}}>Quantity: {Drug.quantity}</Text>
              <Text style={{fontWeight: "bold"}}>Dose: {Drug.dose}</Text>
              <Text style={{fontWeight: "bold"}}>Description:</Text>
              <Text>{Drug.descriptionLong}</Text>
            </View>

          </ScrollView>

          </View>
    );
  },
});



var styles = StyleSheet.create({
  contentContainer: {
    padding: 10,
  },
  rightPane: {
    justifyContent: 'space-between',
    flex: 1,
  },
  movieTitle: {
    flex: 1,
    fontSize: 16,
    fontWeight: '500',
  },
  rating: {
    marginTop: 10,
  },
  ratingTitle: {
    fontSize: 14,
  },
  ratingValue: {
    fontSize: 28,
    fontWeight: '500',
  },
  mpaaWrapper: {
    alignSelf: 'flex-start',
    borderColor: 'black',
    borderWidth: 1,
    paddingHorizontal: 3,
    marginVertical: 5,
  },
  mpaaText: {
    fontFamily: 'Palatino',
    fontSize: 13,
    fontWeight: '500',
  },
  mainSection: {
    flexDirection: 'row',
  },
  detailsImage: {
    width: 134,
    height: 200,
    backgroundColor: '#eaeaea',
    marginRight: 10,
  },
  separator: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: 1 / PixelRatio.get(),
    marginVertical: 10,
  },
  castTitle: {
    fontWeight: '500',
    marginBottom: 3,
  },
  castActor: {
    marginLeft: 2,
  },
});

module.exports = MovieScreen;
