'use strict';

import Autocomplete from './auto';
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView
} from 'react-native';


class AutocompleteExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: [{title: 'test'}],
            query: '',
        };
    }

    _findFilm(query) {
        if (query === '') {
            return [];
        }

        const { films } = this.state;
        const regex = new RegExp(`${query.trim()}`, 'i');
        return films.filter(film => film.title.search(regex) >= 0);
    }




    render() {
        const { query } = this.state;
        const films = this._findFilm(query);

        return (
            <View style={styles.container}>
                <ScrollView>

                <Autocomplete
                    autoCapitalize="none"
                    autoCorrect={false}
                    containerStyle={styles.autocompleteContainer}
                    data={films}
                    defaultValue={query}
                    onChangeText={text => this.setState({query: text})}
                    placeholder="Enter Star Wars film title"
                    renderItem={({title, release_date}) => (
                        <TouchableOpacity onPress={() => this.setState({query: title})}>
                          <Text style={styles.itemText}>
                            {title}
                          </Text>
                        </TouchableOpacity>
                      )}
                />
                    </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        paddingTop: 20
    },
    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 20
    },
    itemText: {
        fontSize: 15,
        margin: 2
    },
    info: {
        paddingTop: 60,
        flex:4,
    },
    infoText: {
        textAlign: 'center'
    },
    titleText: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 10,
        marginTop: 10,
        textAlign: 'center'
    },
    directorText: {
        color: 'grey',
        fontSize: 12,
        marginBottom: 10,
        textAlign: 'center',
    },
    openingText: {
        textAlign: 'center'
    }
});


module.exports = AutocompleteExample;