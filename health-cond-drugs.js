'use strict';

var React = require('react-native');
var {
    AppRegistry,
    Image,
    ListView,
    StyleSheet,
    Text,
    View,

    } = React;
var GLOBALS = require('./Globals');
var TimerMixin = require('react-timer-mixin');
var invariant = require('invariant');
var dismissKeyboard = require('dismissKeyboard');


// URL
var API_KEY = '';
var API_URL = '';

var URL1 = 'http://stage-rxcout.pilgrimconsulting.com/backend/web/api/drugs-health-condition/?query=dfgdf&page_limit=20&page_number=0&health_c_id=';
var URL2 = '&apikey=7MNqWkQAss4Pn8Dw9WTYaYNbnVREEFj5QJV27SmALUwGENhvr9';
var PAGE_SIZE = 25;
var PARAMS = '&apikey=7MNqWkQAss4Pn8Dw9WTYaYNbnVREEFj5QJV27SmALUwGENhvr9';
var REQUEST_URL = '';

var MainContainer = require('./main-container');

function getImageSource(movie: Object, kind: ?string): {uri: ?string} {
    var uri = movie && movie.image ? movie.image : null;
    if (uri && kind) {
        uri = uri.replace('tmb', kind);
    }
    return { uri };
}


var HealthConditions = React.createClass({

    mixins: [TimerMixin],
    timeoutID: (null: any),
totalRows: 0,

    getInitialState: function() {
    return {
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        loaded: false,
        showMore2: this.props.showMore2,
    };
},

fetchData: function() {
    fetch(URL1 + this.props.route.ncId + URL2)
        .then((response) => response.json())
        .then((responseData) => {
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(responseData.result),
                loaded: true,
            });
            this.totalRows = responseData.result.length;
        })
        .done();
},

componentDidMount: function() {
    this.clearTimeout(this.timeoutID);
    this.timeoutID = this.setTimeout(() => this.fetchData(), 0);
},

renderLoadingView: function() {
    return (
        <View style={styles.container}>
            <Text>Loading . . .</Text>
        </View>
    );
},

renderHealthCondition: function(result) {
    if(result.length < 1){
        return (
            <View style={styles.container}>
                <Text>Sorry, no results</Text>
            </View>
        );
    } else {

        return (
            <View style={styles.container} key={result.id}>

                <Image
                    key={getImageSource(result, 'det')}
                    source={getImageSource(result, 'det')}
                    style={[styles.thumbnail,{width: 90, marginTop: 0}]}
                />

                <View style={styles.rightContainer} key={result.title}>
                    <Text style={styles.title}>{result.title}</Text>
                    <Text style={styles.year}>{result.description}</Text>
                </View>

            </View>
        );

    }
},

render: function() {
    if (!this.state.loaded) {
        return this.renderLoadingView();
    }

    var heightOfOneRow = 21;
    var totalRows = this.totalRows * heightOfOneRow;

    return (
        <MainContainer>
            <ScrollView style={[this.props.showMore2==1 ?{height: totalRows}:{height: 0}, {backgroundColor: '#F5FCFF'}]}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderHealthCondition}
                    style={[styles.listView]}
                />
            </ScrollView>
        </MainContainer>
    );
},
});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    rightContainer: {
        flex: 1,
        backgroundColor: 'white',

    },
    title: {
        fontSize: 18,
        textAlign: 'center',
    },
    year: {
        padding: 5,
    },
    thumbnail: {
        width: 53,
        height: 81,
    },
    listView: {
        backgroundColor: '#F5FCFF',
    },
    separator: {
        height: 1,
        backgroundColor: 'gray',
    },
});



module.exports = HealthConditions;