// TODO: get rid of most of the information in here, move necessary items to config
exports.oPillIdCriterea = {
  imprint :'',
  color   :'',
  shape   :''
};


// Tab Icons
exports.tabIcons = {
    myrx:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAHMJ3jJAAAAM1BMVEX///+AgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoXobw3jAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAVdJREFUOMutktGOqzAMRCfEpAYCOf//tfchoYQC3V3pjlRqjU5sM0SqApCy6tORsoTqT4AkeEnCJWlu5zS71SLXPpKUh6MsMmtNDkAqYW8Qt2ZlqFu8IZNEXa2WsXOjeytLMGvlcgCSNJveWijorJFj5B7DtDTToZ0maytv4jDn5cYEbQCxN7sBJxKA1w/kSUNHJBZdxHJiao4YZh9c1XhxqvJ+Em/OMu1BWZ9jPsxIuppAqiF3ZijxSird9NwgAJTenMhydx9Px59WMjMLHyuN15XycLPSTc+5T6z+D+dvpBtZn+o3ZRYZxO9U6ue97hF393zeLLu7pxMW+aL48QZJ0pQ7TW0XO4P56d1uwBUo0rzPXMMDuABbN3966vjr0QmYpMGahiewACiWdzDj0+jkr/Cb0ZKFP8QT1h/jmYFVSv8pnvUeXC8grPmilY9LoVSeLllpF/IfLzEiXz1h+4EAAAAASUVORK5CYII=',
    reminders:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAHMJ3jJAAAAM1BMVEX///+AgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoXobw3jAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAXtJREFUOMuNlNGWgyAMREelGjXA/f+v3QdQqdhu86CcYZIMSUCSkCRZkPAoCRkSx4YkaZdEBbLeDKg/mmX1Lh71a9rOkFNqA/ixLNSpxNolmaxGlQEoSJJdUU2SNF7KChwZRrZOadLdSO6cslZmsjdnkWQIc0S4eAcYb0wiJIBNjXuTrf4DQxuq2sZeV5npcnpVPWMn3nl12AY8EFudJ+gOyx2UGmKOG2vN71cdGztAd4RBOzYn6M15ThBgvsUM0nDFhEiRriZRnSBn6cDi9RV8k3fW7GzHcinlQKfGaYAUpFd+r2Yq8jd9sTEDPLTsQ7cwM7PwAxGgnZgnokla6q36lzhlyEM/WB/MWp4BliEZEDcg6GkmZeAloYPpV+J05fW5I+aQII4JdmA12zzT3XzrjhDXV/OatcQYJmkKoxTCnI6xfSD6XfcQBv1E1FPqGeITL3Q1unfgmsm1d+36P+Un6ZEy6Cdg0D0WZWe/1zLPH+crrPXJzL68v2d/fxUhIEvSCUoAAAAASUVORK5CYII=',
    search:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAHMJ3jJAAAAM1BMVEX///+AgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoXobw3jAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAVBJREFUOMvNU9luxCAMtIEACWTx/39tHa4YSKr2oVJHK2LGA/haACDIoPxTl6UEheXrAc5CbNWpNUAiOinvy5p9WIiYVZ7YIA0dmjIb6pbd5ShQ4sVQfbet0nSCSBDItyuoPt/2sx2oIN2O0HaOut6Lk6nZVBMZrg5Cedt8xtwhiUMVfL8oxF2Q39G2fLlsyQ0ubO+48RaK8VpPGZBpxlY5liDMwYvcsUczphaeyPgtaQVpl3rFbqreMC8COVi7I1gaany03JPsMn4yZ2BgZY//iO2miSRwKHiEPmnBgYsMqyyWpqH9VOmkM5k9hwt0ytXBecT6yN7wF/uRTE5iW+Pe81TDOOrxIUGcHW/CxZHDXktR+i8Td0vUbYgYQ93LrOiHpOdauFLfvb+vSjuTemvNgrT+h7c0SqKCNyn3yIfIOKwSbXyWzif/h9QC/ETKsi8p4h2sg3TBtgAAAABJRU5ErkJggg==',
    pillid:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAHMJ3jJAAAAM1BMVEX///+AgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoXobw3jAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAATNJREFUOMuNlNu2gyAMREcUPGI18/9fex4gEm61rD7Y7W4IAxQAwgGAJECQCACAA3kQ+lZIgkgfPu9J8nkgAUqp91SAajqkqrCUWnUxMH25bSdasbSn8+Q51nY2U55rj6xOdpCUrYGkVLZBdrXCYJk++BaVnzeNL8yrH4+TvFyNjtSQWBa1TRkw0g0YrwHT3mpGZX4v7MxsBUIlxrwDSg/DlEYA3uzUnpmJDtgzg0ksKDOwsAINA7l1DDHRP8vSFdD+2iQ9Xoe/Sz5xmWpNupT1J61vbKrl2H/Q2ns01yrxm2YOYXyWF0Zi6DQMVT/SejWaC9JlGroM70n0WxP1SfIz2J/Q7kj75zPRkujeNeDqpg7jY+BIUtybBmBJZ/wTNrft91QDgFW+nqi6geMiSTnDzPgHPEEqrN4bsI0AAAAASUVORK5CYII=',
    account:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAHMJ3jJAAAAM1BMVEX///+AgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoWAgoXobw3jAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAVFJREFUOMu1lNuShCAMRBsvDCLI+f+v3QdQiKP7tNs1NaZiV5JuA1IFgISEWiDJqb+VyILzp8bxQKdEKAKWlggQ2WvtSc8AdklShMqUq/2uP6nVPRvVMLaQzA6o+Fbxmluz3jFzIbfUhsXpTNOoBMDUeMvgXZvk1HIly1DJXzEQJfnCLmkFjtPQjpj1N9hrWz+kctc4y0zcEB9ysElqkmJ/7aQJgGSkN6LpaO3U0qo+JLHJ3STtdOfTD7lSgxJhz3AAEHTUcbVTfJMiOazzmSIp3xaWJ2u9/hhL2FJKaQvLO8cft8/C8TDHhxd87JbxC+JFW207Jzk7xvp9guJzl+3OM1IXyxz7BiswmO7praAtmfRuhbXM7r5Zhol34njM/O02SXd7J0ma7p8g9aupBCetl9HHKi1pPLqfr6M9wuUuM/S76e02bAbP6dcVXcus/8AP2NsuDZMnxFQAAAAASUVORK5CYII='
};

exports.titleCut = function (str){
    if (str.lenth < 10){
        return str;
    }
    return str.substr(0, 10)+'...';
};

exports.print_r = function print_r (arr,level) {
 

    var dumped_text = "";

    if(!level) level = 0;

    //The padding given at the beginning of the line.

    var level_padding = "";

    for(var j=0;j<level+1;j++) level_padding += "    ";

    if(typeof(arr) == 'object') { //Array/Hashes/Objects 

    for(var item in arr) {

     var value = arr[item];
     
     if(typeof(value) == 'object') { //If it is an array,
      dumped_text += level_padding + "'" + item + "' ...\n";
      dumped_text += print_r(value,level+1);
     } else {
      dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
     }
    }
    } else { //Stings/Chars/Numbers etc.
    dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
    }
    return dumped_text;
};

exports.getDateTime = function getDateTime() {
    var now     = new Date();
    var year    = now.getFullYear();
    var month   = now.getMonth()+1;
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds();
    if(month.toString().length == 1) {
        var month = '0'+month;
    }
    if(day.toString().length == 1) {
        var day = '0'+day;
    }
    if(hour.toString().length == 1) {
        var hour = '0'+hour;
    }
    if(minute.toString().length == 1) {
        var minute = '0'+minute;
    }
    if(second.toString().length == 1) {
        var second = '0'+second;
    }

    //var dateTime = parseInt(year+month+day+hour+minute);//
    //var dateTime = parseInt(day+hour+minute);// debug, for Minutes diff
    var dateTime = parseInt(year+month+day);// debug, for Day diff

    return dateTime;
};


exports.COLORS = {
    red: '#d83b34',
    orange: '#e0e0e0',
    black: '#452f21'
};

exports.menuMap = {
  activeTab: 'search',
  myrx: {
    currentDrugID: 0,
    count: 1,
    titles: {
      0: ' ',
      1: 'My Rx',
      2: 'My Rx',
      3: 'My Rx',
      4: 'My Rx'
    }    
  },
  reminders: {
    count: 1,
    titles: {
      0: ' ',
      1: 'Reminders',
      2: '2-rem',
      3: '3-rem'
    }
  },
  search: {
    healthCondition: 0,
      currentHealthId: 0,
    count: 1,
    titles: {
      0: ' ',
      1: 'Search',
      2: '2-Ce-ce',
      3: '3-De-de'
    }
  },
  pills: {
    count: 1,
    titles: {
      0: ' ',
      1: 'Pill ID',
      2: '2-Ce-ce',
      3: '3-De-de'
    }
  },
  account: {
    count: 1,
    titles: {
      0: ' ',
      1: 'Account',
      2: '2-Ce-ce',
      3: '3-De-de'
    }
  }
};

exports.appIcons = {
    'arrow':"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAACVBMVEXS0ND////S0NBtD2kCAAAAAnRSTlMAAHaTzTgAAAWmSURBVHja7d1bbttIFEVRsuD5D7mSH8PIw5ZIU4Gi2muPQJdnQRb7o7Pvm8oNjwAAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAAADAIwBAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAPSf9fain/vHbdXzyR9v9w3w3Jt8sYUBDH/a0gCGHzdpAMPP2zSA4QUnDWB4xU0DGOu/4gBw5hgCUgBG4i0XgFOnEJABMCLvuQCcPISABICRedMF4PQZBCz/cEboXReAbxxBwNKPZkScA3DhBAKWfTAjIx2ASwcQsORjGSHrAFz8+AQs91BGSjsAlz88AUs9khHzDsADPjoByzyQkRMPwEM+OAFLPI4RNA+Amz0MR3sW27ZtF/8HAAS8/qMgoP5tSED9zyEB9d9DBNR/EBNQfyMioP5KTEAcAAF1AATUARBQB0BA/n4C6ucTUL+egPrxBNRvJ6B+OgH1ywmoH05A/W4C6mcTUL+agPrRBNRvJqB+MgH1iwmoH0xA/V4C6ucSUL+WgPqxBNRvJaB+KgH1SwmoH0pA/U4C6mcSUL+SgPqRBNRvJKB+IgH1Cwmof8kRUP+dQ0D9VYeAOAAC6gAIqAMgoA6AgDoAAuoACKgDIKAOgIA6AALqAAioAyCgDoCAOgAC6gAIqAMgoA6AgDoAAuoACKgDIKAOgIA6AALqAAioAyCgDoCAOgAC8v9wKgH1fzs5LyD/z6fXBeQB1AUAEBcAQFwAAHEBAMQFABAXAEBcAABxAQDEBQAQFwBAXAAAcQEAxAUAEBcAQFwAAHEBAMQFABAXAEBcAABxAQDEBQAQFwBAXAAA/+K5TADsD4D9AbA/APYHwP4A2B8A+wNgfwDsD4D9AbA/APYHwP4A2B8A+wNgfwDsD4D9AbA/APYHwP4A2B8A+wNgfwDsD4D9AbA/APYHwP4A2B8A+wNgfwDsD4D9AbA/APYHwP4A2B8A+wNgfwDsD4D9AbA/APYHwP4A2B8A+wNg/zwA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZg/zYA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZg/zYA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZg/zYA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZg/zYA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZg/zYA+7cB2L8NwP5tAPZvA7B/G4D92wDs3wZgf78B7F8GMO0f/waY9o//CZj2j/8GmPaP/wic9o+/BUz7x18Dp/3j/x1g2r8N4NSm9l8QwIlV7b8kgMO72n9RAAeXtf+yAA5ta/+FARxY1/5LA7i7r/0XB3BnYfsvD+DmxvYPALixsv0TAL7c2f4RAF8sbf8MgE+3tn8IwCdr2z8F4K+97R8D8Mfi9s8B+G1z+wcB/LK6/ZMAPna3fxTA+/L2zwLYpv3bALZp/8Ptu2fgG0AACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQADomf0EaBYFEPdfmSIAAAAASUVORK5CYII=",
};

exports.locations = [
        {
            "id" :  "1",
            "title": "HealthWarehouse",
            "distance":"0.7 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "COUPON",
            "price": "$10.50"
        },
        {
            "id" :  "2",
            "title": "Kroger Pharmacy",
            "distance":"0.9 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "DISCOUNT",
            "price": "$12.50"
        },
        {
            "id" :  "3",
            "title": "Walmart",
            "distance":"4.7 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "COUPON",
            "price": "$13.50"
        },
    ];

exports.medicines= {
  "total": 1,
  "drugs": [
  {
      "id"                  : "1",
      "title"               : "vicodin",
      "image"               : "testImage",
      "descriptionLong"     : "Aspirin, also known as acetylsalicylic acid (ASA), is a salicylate medication, often used to treat pain, fever, and inflammation.[2] Aspirin also has an antiplatelet effect by stopping the binding together of platelets and preventing a patch over damaged walls of blood vessels. Aspirin is also used long-term, at low doses, to help prevent heart attacks, strokes, and blood clot formation in people at high risk of developing blood clots.[3] Low doses of aspirin may be given immediately after a heart attack to reduce the risk of another heart attack or of the death of cardiac tissue. Aspirin may be effective at preventing certain types of cancer, particularly colorectal cancer.",
      "quantity"            : "30 tablets",
      "dose"                : "20mg",
      "defaultPrice"        : "$10.50",
      "color"               : "test",
      "shape"               : "square",
      "descriptionShort"    : "Very good medication",
      "locations"           : [
            {
            "id" :  "1",
            "title": "HealthWarehouse",
            "distance":"0.7 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "CASH",
            "price": "$10.50"
            },
            {
            "id" :  "2",
            "title": "Kroger Pharmacy",
            "distance":"0.9 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "COUPON",
            "price": "$12.50"
            },
            {
            "id" :  "3",
            "title": "Walmart",
            "distance":"4.7 miles",
            "city": "Kyiv",
            "street": "Khreschatyk",
            "num": "23",
            "latitude": "123.1231",
            "lingitude": 124.1234,
            "discountType": "DISCOUNT",
            "price": "$13.50"
            },
      ]

    },
      {
          "id"                  : "2",
          "title"               : "synthroid",
          "image"               : "testImage",
          "descriptionLong"     : "Aspirin, also known as acetylsalicylic acid (ASA), is a salicylate medication, often used to treat pain, fever, and inflammation.[2] Aspirin also has an antiplatelet effect by stopping the binding together of platelets and preventing a patch over damaged walls of blood vessels. Aspirin is also used long-term, at low doses, to help prevent heart attacks, strokes, and blood clot formation in people at high risk of developing blood clots.[3] Low doses of aspirin may be given immediately after a heart attack to reduce the risk of another heart attack or of the death of cardiac tissue. Aspirin may be effective at preventing certain types of cancer, particularly colorectal cancer.",
          "quantity"            : "30 tablets",
          "dose"                : "20mg",
          "defaultPrice"        : "$10.50",
          "color"               : "test",
          "shape"               : "square",
          "descriptionShort"    : "Very good medication",
          "locations"           : [
              {
                  "id" :  "1",
                  "title": "HealthWarehouse",
                  "distance":"0.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "CASH",
                  "price": "$10.50"
              },
              {
                  "id" :  "2",
                  "title": "Kroger Pharmacy",
                  "distance":"0.9 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "COUPON",
                  "price": "$12.50"
              },
              {
                  "id" :  "3",
                  "title": "Walmart",
                  "distance":"4.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "DISCOUNT",
                  "price": "$13.50"
              },
          ]

      },
      {
          "id"                  : "3",
          "title"               : "delasone",
          "image"               : "testImage",
          "descriptionLong"     : "Aspirin, also known as acetylsalicylic acid (ASA), is a salicylate medication, often used to treat pain, fever, and inflammation.[2] Aspirin also has an antiplatelet effect by stopping the binding together of platelets and preventing a patch over damaged walls of blood vessels. Aspirin is also used long-term, at low doses, to help prevent heart attacks, strokes, and blood clot formation in people at high risk of developing blood clots.[3] Low doses of aspirin may be given immediately after a heart attack to reduce the risk of another heart attack or of the death of cardiac tissue. Aspirin may be effective at preventing certain types of cancer, particularly colorectal cancer.",
          "quantity"            : "30 tablets",
          "dose"                : "20mg",
          "defaultPrice"        : "$10.50",
          "color"               : "test",
          "shape"               : "square",
          "descriptionShort"    : "Very good medication",
          "locations"           : [
              {
                  "id" :  "1",
                  "title": "HealthWarehouse",
                  "distance":"0.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "CASH",
                  "price": "$10.50"
              },
              {
                  "id" :  "2",
                  "title": "Kroger Pharmacy",
                  "distance":"0.9 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "COUPON",
                  "price": "$12.50"
              },
              {
                  "id" :  "3",
                  "title": "Walmart",
                  "distance":"4.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "DISCOUNT",
                  "price": "$13.50"
              },
          ]

      },
      {
          "id"                  : "11",
          "title"               : "aspirin",
          "image"               : "testImage",
          "descriptionLong"     : "Aspirin, also known as acetylsalicylic acid (ASA), is a salicylate medication, often used to treat pain, fever, and inflammation.[2] Aspirin also has an antiplatelet effect by stopping the binding together of platelets and preventing a patch over damaged walls of blood vessels. Aspirin is also used long-term, at low doses, to help prevent heart attacks, strokes, and blood clot formation in people at high risk of developing blood clots.[3] Low doses of aspirin may be given immediately after a heart attack to reduce the risk of another heart attack or of the death of cardiac tissue. Aspirin may be effective at preventing certain types of cancer, particularly colorectal cancer.",
          "quantity"            : "30 tablets",
          "dose"                : "20mg",
          "defaultPrice"        : "$10.50",
          "color"               : "test",
          "shape"               : "square",
          "descriptionShort"    : "Very good medication",
          "locations"           : [
              {
                  "id" :  "1",
                  "title": "HealthWarehouse",
                  "distance":"0.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "CASH",
                  "price": "$10.50"
              },
              {
                  "id" :  "2",
                  "title": "Kroger Pharmacy",
                  "distance":"0.9 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "COUPON",
                  "price": "$12.50"
              },
              {
                  "id" :  "3",
                  "title": "Walmart",
                  "distance":"4.7 miles",
                  "city": "Kyiv",
                  "street": "Khreschatyk",
                  "num": "23",
                  "latitude": "123.1231",
                  "lingitude": 124.1234,
                  "discountType": "DISCOUNT",
                  "price": "$13.50"
              },
          ]

      },
  ]

};

// Reverse array || object
exports.oaReverse = function(oa){
    var oaNew = [];
    if(oa instanceof Array && oa!=[]){
        for(var i in oa){
            oaNew.unshift(oa[i]);
        }
    }
    if(oa instanceof Object && oa!={}){
        reverseForIn(oa, function(key){ oaNew[key] = this[key]; });
    }
    return oaNew;
};
function reverseForIn(obj, f) {
    var arr = [];
    for (var key in obj) {
        // add hasOwnPropertyCheck if needed
        arr.push(key);
    }
    for (var i=arr.length-1; i>=0; i--) {
        f.call(obj, arr[i]);
    }
}

exports.__feedRoute = {};