'use strict';


var Tabs = require('./Tabs');
var GLOBALS = require('./Globals');

var LoginScreen = React.createClass({

    getInitialState: function() {

        var URL = CONFIG.API_URL + CONFIG.user_update_get + GLOBALS.API_KEY;

        fetch(URL)
            .then((response) => response.json())
            .catch((error) => {

            })
            .then((responseData) => {

                if (responseData){
                    if (responseData.username){
                        this.setState({
                            username: responseData.username
                        });
                    }
                    if (responseData.email){
                        this.setState({
                            email: responseData.email
                        });
                    }
                    if (responseData.first_name){
                        this.setState({
                            fname: responseData.first_name
                        });
                    } else {
                        this.setState({
                            fname: ''
                        });
                    }
                    if (responseData.last_name){
                        this.setState({
                            lname: responseData.last_name
                        });
                    } else {
                        this.setState({
                            lname: ''
                        });
                    }
                    if (responseData.member_id){
                        this.setState({
                            memberId: responseData.member_id
                        });
                    } else {
                        this.setState({
                            memberId: ''
                        });
                    }
                    if (responseData.group_number){
                        this.setState({
                            groupNumber: responseData.group_number
                        });
                    } else {
                        this.setState({
                            groupNumber: ''
                        });
                    }
                    if (responseData.phone_number){
                        this.setState({
                            phoneNumber: responseData.phone_number
                        });
                    } else {
                        this.setState({
                            phoneNumber: ''
                        });
                    }
                }
            })
            .done();

        return({
            username        : '',
            fname           : '',
            lname           : '',
            email           : '',
            groupNumber     : '',
            memberId        : '',
            phoneNumber     : '',
            usernameError   : '',
            fnameError      : '',
            lnameError      : '',
            emailError      : '',
            groupNumberError: '',
            memberIdError   : '',
            phoneNumberError: '',
            message         : '',
            keyBoardHeight  : 0
        });
    },

    onSignUp: function(){

        this.setState({
            usernameError   : '',
            emailError      : '',
            groupNumberError: '',
            memberIdError   : '',
            phoneNumberError: ''
        });

        var URL = CONFIG.API_URL + CONFIG.user_update_get;


        var test = JSON.stringify({
            first_name: this.state.fname,
            last_name: this.state.lname,
            email: this.state.email,
            group_number: this.state.groupNumber,
            member_id : this.state.memberId,
            phone_number: this.state.phoneNumber
        });

        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + GLOBALS.API_KEY
            },
            body: test
        }).then((response) => response.json())
            .then((responseText) => {

                console.log(responseText);

                if (responseText.errors){
                    if (responseText.errors.username){
                        this.setState({
                            usernameError: responseText.errors.username[0]
                        });
                    }
                    if (responseText.errors.password){
                        this.setState({
                            passwordError: responseText.errors.password[0]
                        });
                    }
                    if (responseText.errors.email){
                        this.setState({
                            emailError: responseText.errors.email[0]
                        });
                    }
                    if (responseText.errors.member_id){
                        this.setState({
                            memberIdError: responseText.errors.member_id[0]
                        });
                    }
                    if (responseText.errors.group_number){
                        this.setState({
                            groupNumberError: responseText.errors.group_number[0]
                        });
                    }
                }

                if (responseText.auth_key){

                    GLOBALS.API_KEY = responseText.auth_key;

                    AlertIOS.alert('Data saved successfully');

                    this.props.navigator.pop({});
                }


            })
            .catch((error) => {

                this.setState({
                    error: 'Error connecting to server'
                });
            });

    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // You can access `this.props` and `this.state` here
        // This function should return a boolean, whether the component should re-render.
        return true;
    },

    componentWillMount () {
        DeviceEventEmitter.addListener('keyboardWillShow', this.keyboardWillShow.bind(this))
        DeviceEventEmitter.addListener('keyboardWillHide', this.keyboardWillHide.bind(this))
    },

    keyboardWillShow (e) {

        this.setState({keyBoardHeight: e.endCoordinates.height})
    },

    keyboardWillHide (e) {
        this.setState({keyBoardHeight: 0});

        //this.refs._scrollView.scrollTo(200);
    },


    render: function() {

        return (
            <ScrollView ref='_scrollView' >

                <View style={[styles.container, {marginBottom: this.state.keyBoardHeight, backgroundColor: GLOBALS.backgroundCl}]}>

                    <Text style={{color: 'red', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.props.route.message}
                    </Text>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            Username
                        </Text>
                        <Text style={{color:'red'}}>*</Text>
                    </View>

                    <TextInput
                        style={styles.input}
                        onChangeText={(username) => this.setState({username})}
                        value={this.state.username}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        editable={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.usernameError}
                    </Text>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            Email
                        </Text>
                        <Text style={{color:'red'}}>*</Text>
                    </View>
                    <TextInput
                        style={styles.input}
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        keyboardType={'email-address'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.emailError}
                    </Text>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            First Name
                        </Text>
                    </View>

                    <TextInput
                        style={styles.input}
                        onChangeText={(fname) => this.setState({fname})}
                        value={this.state.fname}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.fnameError}
                    </Text>


                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                            Last Name
                        </Text>
                    </View>

                    <TextInput
                        style={styles.input}
                        onChangeText={(lname) => this.setState({lname})}
                        value={this.state.lname}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.lnameError}
                    </Text>


                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Member ID
                    </Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(memberId) => this.setState({memberId})}
                        value={this.state.memberId}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.memberIdError}
                    </Text>


                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Group ID
                    </Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(groupNumber) => this.setState({groupNumber})}
                        value={this.state.groupNumber}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.groupNumberError}
                    </Text>

                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        Phone Number
                    </Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                        value={this.state.phoneNumber}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    />
                    <Text style={{color: GLOBALS.COLORS.red, fontSize: 20, textAlign: 'center', marginBottom: 5, marginTop: 5}}>
                        {this.state.phoneNumberError}
                    </Text>


                    <TouchableHighlight
                        onPress={this.onSignUp}
                        style={styles.loginButton}
                    >
                        <Text style={styles.loginText}>
                            Save
                        </Text>
                    </TouchableHighlight>

                </View>

            </ScrollView>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 12
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input:{
        height: 40,
        borderRadius: 5,
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'white'
    },
    loginButton: {
        backgroundColor: '#357bb5',
        borderRadius: 10,
        marginBottom: 30
    },
    loginText :{
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        color: 'white'
    }

});

module.exports = LoginScreen;